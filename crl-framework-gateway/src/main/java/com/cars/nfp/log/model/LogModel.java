package com.cars.nfp.log.model;

import lombok.Data;

import java.util.Date;

/**
 * 请求日志
 *
 * @author 宋长军
 * @date 2020/4/7 14:33
 */
@Data
public class LogModel {

    /**
     * 请求类型
     * 0：appid
     * 1：password
     */
    private Integer authType;

    /**
     * 归属
     */
    private String owner;

    /**
     * 返回码
     */
    private String returnCode;

    /**
     * 请求时间
     */
    private Date requestDate;

    /**
     * 响应时间
     */
    private Date responseDate;

    /**
     * 请求耗时
     */
    private Long timeConsuming;

    /**
     * 请求路径
     */
    private String path;

    /**
     * 请求服务
     */
    private String serviceName;

    /**
     * 请求参数+响应参数
     */
    private byte[] params;

    /**
     * 请求参数
     */
    private String requestParam;

    /**
     * 参数Str
     */
    private String paramsStr;

    /**
     * 请求客户端IP
     */
    private String clientIp;

    /**
     * 错误类型
     * 0：无权限
     * 1：响应异常
     */
    private Integer errorType;

    /**
     * 成功响应
     */
    public String toSuccessString() {
        return "LogModel{" +
                "authType=" + authType +
                ", owner='" + owner + '\'' +
                ", requestDate=" + requestDate +
                ", responseDate=" + responseDate +
                ", timeConsuming=" + timeConsuming +
                ", path='" + path + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", requestParam='" + requestParam + '\'' +
                ", clientIp='" + clientIp + '\'' +
                '}';
    }

    /**
     * 失败响应
     */
    public String toErrorString() {
        return "LogModel{" +
                "authType=" + authType +
                ", owner='" + owner + '\'' +
                ", returnCode='" + returnCode + '\'' +
                ", requestDate=" + requestDate +
                ", responseDate=" + responseDate +
                ", timeConsuming=" + timeConsuming +
                ", path='" + path + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", params='" + paramsStr + '\'' +
                ", clientIp='" + clientIp + '\'' +
                ", errorType=" + errorType +
                '}';
    }
}
