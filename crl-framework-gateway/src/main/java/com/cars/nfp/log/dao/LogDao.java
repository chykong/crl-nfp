package com.cars.nfp.log.dao;

import com.cars.nfp.log.model.LogModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * 日志入库
 *
 * @author 宋长军
 * @date 2020/4/9 15:44
 */
@Repository
public class LogDao {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    /**
     * 请求日志入库
     *
     * @param logModel 日志信息
     * @return 日志入库情况
     */
    public int insertLog(LogModel logModel) {

        String sql = "insert into crl_service_log (id, owner, auth_type, request_date, path, service_name, request_param, client_ip, time_consuming)" +
                " values (seq_crl_service_log.nextval, :owner, :authType, :requestDate, :path, :serviceName, :requestParam, :clientIp, :timeConsuming)";
        return namedParameterJdbcTemplate.update(sql, new BeanPropertySqlParameterSource(logModel));
    }

    /**
     * 响应非200的日志
     *
     * @param logModel 日志信息
     * @return 日志入库情况
     */
    public int insertErrorLog(LogModel logModel) {

        String sql = "insert into crl_service_error_log (id, owner, auth_type, return_code, request_date, path, service_name, params, client_ip, error_type, time_consuming)" +
                " values (seq_crl_server_error_log.nextval, :owner, :authType, :returnCode, :requestDate, :path, :serviceName, :params, :clientIp, :errorType, :timeConsuming)";
        return namedParameterJdbcTemplate.update(sql, new BeanPropertySqlParameterSource(logModel));
    }
}
