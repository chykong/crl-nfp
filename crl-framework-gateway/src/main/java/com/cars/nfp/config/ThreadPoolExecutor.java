package com.cars.nfp.config;

import com.cars.nfp.util.ZuulConstUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * 线程池配置
 *
 * @author 宋长军
 * @date 2020/4/15 11:48
 */
@EnableAsync
@Configuration
public class ThreadPoolExecutor {

    /**
     * 网关日志
     *
     * @return 现成池
     */
    @Bean(ZuulConstUtil.ZUUL_LOG_EXECUTOR)
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {

        final int corePoolSize = 5;
        final int maxPoolSize = 10;
        final int keepLiveSeconds = 10;
        final int queueCapacity = 300;
        final String threadNamePrefix = "Zuul-Log-";

        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setKeepAliveSeconds(keepLiveSeconds);
        executor.setQueueCapacity(queueCapacity);
        executor.setThreadNamePrefix(threadNamePrefix);
        executor.setRejectedExecutionHandler(new java.util.concurrent.ThreadPoolExecutor.AbortPolicy());
        executor.initialize();
        return executor;
    }
}
