package com.cars.nfp.config;

import com.cars.util.global.GlobalReturnCode;
import com.cars.util.json.JsonResult;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * ERROR
 *
 * @author 宋长军
 * @date 2019/4/19 15:36
 */
@RestController
public class ErrorHandle implements ErrorController {

    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping("/error")
    public JsonResult<Void> error(HttpServletResponse response) {

        if (HttpStatus.NOT_FOUND.value() == response.getStatus()) {
            return new JsonResult<>(GlobalReturnCode.URI_NOT_FOUND);
        } else {
            return new JsonResult<>(GlobalReturnCode.SERVER_ERROR);
        }
    }

//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @ExceptionHandler(Exception.class)
//    public JsonResult globalException(HttpServletRequest request, Throwable ex) {
//
//        return new JsonResult(getStatus(request).toString(), ex.getMessage());
//    }
//
//    private HttpStatus getStatus(HttpServletRequest request) {
//
//        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
//        if(statusCode == null) {
//            return HttpStatus.INTERNAL_SERVER_ERROR;
//        }
//        return HttpStatus.valueOf(statusCode);
//    }
}
