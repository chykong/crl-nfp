package com.cars.nfp.config;

import com.cars.nfp.filter.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Filter 配置
 *
 * @author 宋长军
 * @date 2020/4/7 15:46
 */
@Configuration
public class FilterConfig {

    @Bean
    public CheckAuthorizationFilter checkAuthorizationFilter() {
        return new CheckAuthorizationFilter();
    }

    @Bean
    public RequestLogFilter requestLogFilter() {
        return new RequestLogFilter();
    }

    @Bean
    public ResponseLogFilter responseLogFilter() {
        return new ResponseLogFilter();
    }

    @Bean
    public ZuulFallbackProvider zuulFallbackProvider() {
        return new ZuulFallbackProvider();
    }

    @Bean
    public CorsPreFilter corsPreFilter() {
        return new CorsPreFilter();
    }

    @Bean
    public CorsPostFilter corsPostFilter() {
        return new CorsPostFilter();
    }
}
