package com.cars.nfp.util;

/**
 * 常量信息
 *
 * @author 宋长军
 * @date 2020/4/7 15:06
 */
public interface ZuulConstUtil {

    /**
     * 请求地址
     */
    String REQUEST_PATH = "REQUEST_PATH";

    /**
     * 请求时间
     */
    String REQUEST_DATE = "REQUEST_DATE";

    /**
     * appid 请求方式
     */
    String APPID_AUTH = "APPID";

    /**
     * password 请求方式
     */
    String PASSWORD_AUTH = "Authorization";

    /**
     * appid 请求方式
     */
    Integer APPID_AUTH_TYPE = 0;

    /**
     * password 请求方式
     */
    Integer PASSWORD_AUTH_TYPE = 1;

    /**
     * 无需鉴权的请求
     */
    Integer WHITHOU_AUTH_TYPE = 2;

    /**
     * 无需鉴权的请求
     */
    String WHITHOUT_AUTH_TYPE_NAME = "NO_NEED_AUTH";

    /**
     * 日志线程池
     */
    String ZUUL_LOG_EXECUTOR = "zuulLogExecutor";
}
