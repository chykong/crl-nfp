package com.cars.nfp.util;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 数据列表
 *
 * @author 宋长军
 * @date 2020/6/12 10:35
 */
@Data
@AllArgsConstructor
public class Params {

    /**
     * 请求参数
     */
    private String request;
    /**
     * 响应数据
     */
    private String response;

    public Params(String request) {
        this.request = request;
    }
}
