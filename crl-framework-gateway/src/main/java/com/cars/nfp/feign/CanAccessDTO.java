package com.cars.nfp.feign;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 方法请求参数
 *
 * @author 宋长军
 * @date 2019/9/4 16:07
 */
@AllArgsConstructor
@Data
public class CanAccessDTO {

    /**
     * 请求 url
     */
    @NotBlank
    private String apiUrl;

    /**
     * token 信息
     */
    @NotBlank
    private String token;
}
