package com.cars.nfp.feign;

import com.cars.util.global.GlobalReturnCode;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 调用鉴权服务
 *
 * @author 宋长军
 * @date 2019/8/15 16:27
 */
@FeignClient(value = "crl-framework-auth", fallback = AuthorizationClient.AuthorizationClientFallback.class)
public interface AuthorizationClient {

    /**
     * 获取请求权限
     *
     * @param canAccessDTO 请求URL 请求token
     * @return 是否有请求权限
     */
    @PostMapping("/canAccess")
    JsonResult canAccess(@RequestBody CanAccessDTO canAccessDTO);

    /**
     * 鉴权服务异常
     */
    @Component
    class AuthorizationClientFallback implements AuthorizationClient {

        @Override
        public JsonResult canAccess(CanAccessDTO canAccessDTO) {
            return JsonResultUtil.error("调用鉴权服务异常!");
        }
    }
}
