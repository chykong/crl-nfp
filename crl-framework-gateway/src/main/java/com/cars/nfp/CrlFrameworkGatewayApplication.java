package com.cars.nfp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Spring Zuul
 *
 * @author 宋长军
 * @date 2020/6/12 9:53
 */
@RefreshScope
@EnableZuulProxy
@EnableFeignClients
@SpringBootApplication
public class CrlFrameworkGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrlFrameworkGatewayApplication.class, args);
    }
}
