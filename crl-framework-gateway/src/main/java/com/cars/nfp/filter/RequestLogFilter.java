package com.cars.nfp.filter;

import com.cars.nfp.util.ZuulConstUtil;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import java.util.Date;

/**
 * 请求日志处理器
 *
 * @author 宋长军
 * @date 2020/4/7 10:07
 */
public class RequestLogFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return FilterConstants.SERVLET_DETECTION_FILTER_ORDER - 1;
    }

    @Override
    public boolean shouldFilter() {
        RequestContext requestContext = RequestContext.getCurrentContext();
        return requestContext.sendZuulResponse();
    }

    @Override
    public Object run() {
        RequestContext requestContext = RequestContext.getCurrentContext();
        requestContext.set(ZuulConstUtil.REQUEST_DATE, new Date());
        requestContext.set(ZuulConstUtil.REQUEST_PATH, requestContext.getRequest().getRequestURI());
        return null;
    }
}
