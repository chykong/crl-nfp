package com.crl.dic.controller;

import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.crl.portal.model.DicCz;
import com.crl.portal.service.DicCzService;
import com.crl.portal.vo.DicCzVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @description: 站名字典
 * @author: 孔垂云
 * @version: 1.0
 * @date: 2020/6/9 23:25
 */
@RestController
@RequestMapping("/dic/cz")
public class DicCzController {

    @Autowired
    private DicCzService dicCzService;

    /**
     * 获取车站字典
     * @return
     */
    @PostMapping("/list")
    public JsonResult list() {
        List<DicCz> dicCzList = dicCzService.list();
        return JsonResultUtil.ok(dicCzList);
    }

    /**
     * 测试Exception日志记录
     */
    @PostMapping("/testException1")
    public JsonResult testException1(){
        String one = dicCzService.testException1();
        return JsonResultUtil.ok();
    }

    /**
     * 测试Exception日志记录
     */
    @PostMapping("/testException2")
    public JsonResult testException2(@RequestBody @Valid DicCzVO dicCzVO){
        String one = dicCzService.testException1();
        return JsonResultUtil.ok();
    }

}
