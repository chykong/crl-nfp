package com.crl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @description:
 * @author: 孔垂云
 * @version: 1.0
 * @date: 2020/6/1 13:13
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.crl", lazyInit = true)
public class NfpApiPortalApplication {
    public static void main(String[] args) {
        SpringApplication.run(NfpApiPortalApplication.class, args);
    }
}
