package com.crl;

import com.crl.portal.service.DicCzService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author linan
 * @date 2020-06-12 15:58
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class CZTest {

    @Autowired
    private DicCzService dicCzService;


    @Test
    public void testCzList(){
        List list = dicCzService.list();
        System.out.println(list.size());
    }

}
