## crl Logistics
## nfp 网络平台network freight platform
### crl-framework 公共包
### crl-nfp 网络平台涉及包

### crl-framework-eureka eurekaserver
### crl-framework-controller 公共的controller依赖
### crl-framework-dao 公共的dao依赖
### crl-framework-redis 公共的redis依赖

### crl-nfp-biz 平台核心业务代码，含model、dao、service
### crl-nfp-api-portal 平台门户无授权的api
### crl-nfp-api-user 平台用户信息
### crl-nfp-api-plat-manage 平台管理接口
### crl-nfp-api-wx  微信接口
### crl-nfp-api-app app接口