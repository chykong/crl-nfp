-- 数据库建表脚本

--- 网关日志表
-- Create table 网关请求日志记录
create table CRL_SERVICE_LOG
(
    id             NUMBER,
    owner          VARCHAR2(20),
    auth_type      NUMBER,
    request_date   DATE,
    path           VARCHAR2(100),
    service_name   VARCHAR2(50),
    client_ip      VARCHAR2(15),
    request_param  VARCHAR2(300),
    time_consuming NUMBER
);
-- Add comments to the table
comment on table CRL_SERVICE_LOG
    is '网关请求日志记录';
-- Add comments to the columns
comment on column CRL_SERVICE_LOG.id
    is '编号';
comment on column CRL_SERVICE_LOG.owner
    is '所属，appid/usernmae';
comment on column CRL_SERVICE_LOG.auth_type
    is '0：appid，1：password，2：无需权限的请求';
comment on column CRL_SERVICE_LOG.request_date
    is '请求时间';
comment on column CRL_SERVICE_LOG.path
    is '请求地址';
comment on column CRL_SERVICE_LOG.service_name
    is '服务名';
comment on column CRL_SERVICE_LOG.client_ip
    is '客户端IP';
comment on column CRL_SERVICE_LOG.request_param
    is '请求参数';
comment on column CRL_SERVICE_LOG.time_consuming
    is '请求耗时';
-- Create/Recreate indexes
create index CRL_SERVICE_LOG_INDEX1 on CRL_SERVICE_LOG (OWNER)
    pctfree 10
    initrans 2
    maxtrans 255
    storage
    (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
    );
create index CRL_SERVICE_LOG_INDEX2 on CRL_SERVICE_LOG (REQUEST_DATE)
    pctfree 10
    initrans 2
    maxtrans 255
    storage
    (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
    );
create index CRL_SERVICE_LOG_INDEX3 on CRL_SERVICE_LOG (SERVICE_NAME)
    pctfree 10
    initrans 2
    maxtrans 255
    storage
    (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
    );
create sequence seq_crl_service_log
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

-- Create table 网关请求响应非200日志记录
create table CRL_SERVICE_ERROR_LOG
(
    id             NUMBER,
    owner          VARCHAR2(13),
    auth_type      NUMBER,
    return_code    VARCHAR2(5),
    request_date   DATE,
    path           VARCHAR2(100),
    service_name   VARCHAR2(50),
    params         BLOB,
    client_ip      VARCHAR2(15),
    error_type     NUMBER,
    time_consuming NUMBER
);
-- Add comments to the table
comment on table CRL_SERVICE_ERROR_LOG
    is '网关请求响应非200日志记录';
-- Add comments to the columns
comment on column CRL_SERVICE_ERROR_LOG.id
    is '编号';
comment on column CRL_SERVICE_ERROR_LOG.owner
    is '拥有者，appid/usernmae';
comment on column CRL_SERVICE_ERROR_LOG.auth_type
    is '0：appid，1：password，2：无需权限的请求';
comment on column CRL_SERVICE_ERROR_LOG.return_code
    is '返回码';
comment on column CRL_SERVICE_ERROR_LOG.request_date
    is '请求时间';
comment on column CRL_SERVICE_ERROR_LOG.path
    is '请求地址';
comment on column CRL_SERVICE_ERROR_LOG.service_name
    is '服务名';
comment on column CRL_SERVICE_ERROR_LOG.params
    is 'request、response';
comment on column CRL_SERVICE_ERROR_LOG.client_ip
    is '客户端IP';
comment on column CRL_SERVICE_ERROR_LOG.error_type
    is '0：无权限，1：响应异常';
comment on column CRL_SERVICE_ERROR_LOG.time_consuming
    is '请求耗时';
-- Create/Recreate indexes
create index CRL_SERVICE_ERROR_LOG_INDEX1 on CRL_SERVICE_ERROR_LOG (OWNER)
    pctfree 10
    initrans 2
    maxtrans 255
    storage
    (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
    );
create index CRL_SERVICE_ERROR_LOG_INDEX2 on CRL_SERVICE_ERROR_LOG (REQUEST_DATE)
    pctfree 10
    initrans 2
    maxtrans 255
    storage
    (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
    );
create index CRL_SERVICE_ERROR_LOG_INDEX3 on CRL_SERVICE_ERROR_LOG (SERVICE_NAME)
    pctfree 10
    initrans 2
    maxtrans 255
    storage
    (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
    );
create sequence seq_crl_server_error_log
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

--- 用户服务表
-- Create table 用户登录表
create table CRL_BASE_USERLOGIN
(
    user_id              NUMBER,
    username             VARCHAR2(20),
    mobile               VARCHAR2(11),
    password             VARCHAR2(100),
    status               NUMBER,
    error_times          NUMBER(2) default 0,
    last_login_time      DATE,
    change_password_time DATE,
    audit_status         NUMBER
);
-- Add comments to the table
comment on table CRL_BASE_USERLOGIN
    is '统一登录用户登录表';
-- Add comments to the columns
comment on column CRL_BASE_USERLOGIN.user_id
    is '关联HGP_BASE_USER';
comment on column CRL_BASE_USERLOGIN.username
    is '用户名';
comment on column CRL_BASE_USERLOGIN.mobile
    is '手机号';
comment on column CRL_BASE_USERLOGIN.password
    is '登录密码';
comment on column CRL_BASE_USERLOGIN.status
    is '状态0正常，1禁用';
comment on column CRL_BASE_USERLOGIN.error_times
    is '密码错误次数';
comment on column CRL_BASE_USERLOGIN.last_login_time
    is '上次登录时间';
comment on column CRL_BASE_USERLOGIN.change_password_time
    is '密码修改时间';
comment on column CRL_BASE_USERLOGIN.audit_status
    is '审核状态0未审核，1审核通过，2审核未通过';
-- Create/Recreate indexes
create index IND_CRL_BASE_USERLOGIN_1 on CRL_BASE_USERLOGIN (USERNAME)
    pctfree 10
    initrans 2
    maxtrans 255
    storage
    (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
    );
create index IND_CRL_BASE_USERLOGIN_2 on CRL_BASE_USERLOGIN (MOBILE)
    pctfree 10
    initrans 2
    maxtrans 255
    storage
    (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
    );
create index IND_CRL_BASE_USERLOGIN_3 on CRL_BASE_USERLOGIN (PASSWORD)
    pctfree 10
    initrans 2
    maxtrans 255
    storage
    (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
    );

-- Create table 用户表
create table CRL_BASE_USER
(
    id             NUMBER not null,
    username       VARCHAR2(30),
    real_name      VARCHAR2(30),
    telephone      VARCHAR2(10),
    mobile         VARCHAR2(11),
    idcard         VARCHAR2(20),
    department_id  NUMBER,
    workcard       VARCHAR2(30),
    position       VARCHAR2(50),
    remark         VARCHAR2(200),
    created_by     VARCHAR2(20),
    created_at     DATE,
    modified_by    VARCHAR2(20),
    modified_at    DATE,
    bureau_code    VARCHAR2(2),
    freight_code   VARCHAR2(6),
    station_code   VARCHAR2(3),
    auth_system_id NUMBER
);
-- Add comments to the table
comment on table CRL_BASE_USER
    is '统一登录用户表';
-- Add comments to the columns
comment on column CRL_BASE_USER.id
    is '主键，SEQ_CRL_BASE_USER';
comment on column CRL_BASE_USER.username
    is '用户名';
comment on column CRL_BASE_USER.real_name
    is '真实姓名';
comment on column CRL_BASE_USER.telephone
    is '座机号';
comment on column CRL_BASE_USER.mobile
    is '手机号';
comment on column CRL_BASE_USER.idcard
    is '身份证号';
comment on column CRL_BASE_USER.department_id
    is '关联部门表id';
comment on column CRL_BASE_USER.workcard
    is '工作证号';
comment on column CRL_BASE_USER.position
    is '所在岗位';
comment on column CRL_BASE_USER.remark
    is '备注';
comment on column CRL_BASE_USER.created_by
    is '创建人';
comment on column CRL_BASE_USER.created_at
    is '创建时间';
comment on column CRL_BASE_USER.modified_by
    is '修改人';
comment on column CRL_BASE_USER.modified_at
    is '修改时间';
comment on column CRL_BASE_USER.bureau_code
    is '路局';
comment on column CRL_BASE_USER.freight_code
    is ' huo yun中心';
comment on column CRL_BASE_USER.station_code
    is '车站';
comment on column CRL_BASE_USER.auth_system_id
    is '所属授权系统id';
-- Create/Recreate indexes
create index IDX_CRL_BASE_USER_1 on CRL_BASE_USER (USERNAME)
    pctfree 10
    initrans 2
    maxtrans 255
    storage
    (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
    );
create index IDX_CRL_BASE_USER_2 on CRL_BASE_USER (MOBILE)
    pctfree 10
    initrans 2
    maxtrans 255
    storage
    (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
    );
create index IDX_CRL_BASE_USER_3 on CRL_BASE_USER (REAL_NAME)
    pctfree 10
    initrans 2
    maxtrans 255
    storage
    (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
    );
-- Create/Recreate primary, unique and foreign key constraints
alter table CRL_BASE_USER
    add primary key (ID)
        using index
            pctfree 10
            initrans 2
            maxtrans 255
            storage
            (
            initial 64K
            next 1M
            minextents 1
            maxextents unlimited
            );
-- 资源表 CRL_BASE_RESOURCE
create table CRL_BASE_RESOURCE
(
  ID             NUMBER not null,
  PARENT_ID      NUMBER,
  RESOURCE_NAME  VARCHAR2(20),
  RESOURCE_TYPE  NUMBER,
  RESOURCE_CODE  VARCHAR2(128),
  DISPLAY_ORDER  NUMBER,
  URI            VARCHAR2(100),
  TARGET         VARCHAR2(30),
  ICON           VARCHAR2(128),
  IS_SHOW        NUMBER,
  REDIRECT_PATH  VARCHAR2(200),
  KEEP_ALIVE     NUMBER,
  STATUS         NUMBER,
  RESOURCE_DESC  VARCHAR2(200),
  COMPONENT      VARCHAR2(200),
  PATH           VARCHAR2(100),
  AUTH_SYSTEM_ID NUMBER
);

comment on table CRL_BASE_RESOURCE
  is '统一登录资源表';
comment on column CRL_BASE_RESOURCE.ID
  is '主键，SEQ_HGP_BASE_RESOURCE';
comment on column CRL_BASE_RESOURCE.PARENT_ID
  is '自关联上级资源';
comment on column CRL_BASE_RESOURCE.RESOURCE_NAME
  is '资源名称';
comment on column CRL_BASE_RESOURCE.RESOURCE_TYPE
  is '资源类别0模块，1功能';
comment on column CRL_BASE_RESOURCE.RESOURCE_CODE
  is '资源代码，唯一，校验权限';
comment on column CRL_BASE_RESOURCE.DISPLAY_ORDER
  is '资源排序';
comment on column CRL_BASE_RESOURCE.URI
  is '资源路径';
comment on column CRL_BASE_RESOURCE.TARGET
  is '目标';
comment on column CRL_BASE_RESOURCE.ICON
  is '图标';
comment on column CRL_BASE_RESOURCE.IS_SHOW
  is '是否显示0显示，1不显示';
comment on column CRL_BASE_RESOURCE.REDIRECT_PATH
  is '重定向地址，父级菜单默认地址（VUE）';
comment on column CRL_BASE_RESOURCE.KEEP_ALIVE
  is '是否缓存0缓存，1不缓存';
comment on column CRL_BASE_RESOURCE.STATUS
  is '状态0正常，1锁定';
comment on column CRL_BASE_RESOURCE.RESOURCE_DESC
  is '资源描述';
comment on column CRL_BASE_RESOURCE.COMPONENT
  is '组件';
comment on column CRL_BASE_RESOURCE.PATH
  is '路由地址';
comment on column CRL_BASE_RESOURCE.AUTH_SYSTEM_ID
  is '所属授权系统id';
alter table CRL_BASE_RESOURCE
  add primary key (ID)
  using index 
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IND_CRL_BASE_RESOURCE_1 on CRL_BASE_RESOURCE (RESOURCE_NAME)
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IND_CRL_BASE_RESOURCE_2 on CRL_BASE_RESOURCE (RESOURCE_TYPE)
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IND_CRL_BASE_RESOURCE_3 on CRL_BASE_RESOURCE (RESOURCE_CODE)
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- 序列
create sequence SEQ_CRL_BASE_RESOURCE
    minvalue 1
    maxvalue 9999999999999999999999999
    start with 3541
    increment by 1
    cache 20;

-- 角色表 CRL_BASE_ROLE
create table CRL_BASE_ROLE
(
  ID             NUMBER not null,
  ROLE_CODE      VARCHAR2(20),
  ROLE_NAME      VARCHAR2(20),
  ROLE_DESC      VARCHAR2(200),
  CREATED_BY     VARCHAR2(20),
  CREATED_AT     DATE,
  MODIFIED_BY    VARCHAR2(20),
  MODIFIED_AT    DATE,
  ROLE_TYPE      VARCHAR2(1) default 1,
  AUTH_SYSTEM_ID NUMBER
);

comment on table CRL_BASE_ROLE
  is '统一登录角色表';
comment on column CRL_BASE_ROLE.ID
  is '主键，SEQ_HGP_BASE_ROLE';
comment on column CRL_BASE_ROLE.ROLE_CODE
  is '角色代码';
comment on column CRL_BASE_ROLE.ROLE_NAME
  is '角色名称';
comment on column CRL_BASE_ROLE.ROLE_DESC
  is '角色描述';
comment on column CRL_BASE_ROLE.CREATED_BY
  is '创建人';
comment on column CRL_BASE_ROLE.CREATED_AT
  is '创建时间';
comment on column CRL_BASE_ROLE.MODIFIED_BY
  is '修改人';
comment on column CRL_BASE_ROLE.MODIFIED_AT
  is '修改时间';
comment on column CRL_BASE_ROLE.ROLE_TYPE
  is '0管理员角色 1普通角色';
comment on column CRL_BASE_ROLE.AUTH_SYSTEM_ID
  is '所属授权系统id';
alter table CRL_BASE_ROLE
  add primary key (ID)
  using index
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IND_CRL_BASE_ROLE_1 on CRL_BASE_ROLE (ROLE_CODE)
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index IND_CRL_BASE_ROLE_2 on CRL_BASE_ROLE (ROLE_NAME)
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
    );
-- 序列
create sequence SEQ_CRL_BASE_ROLE
    minvalue 1
    maxvalue 9999999999999999999999999
    start with 721
    increment by 1
    cache 20;

-- 角色资源关系表
create table CRL_BASE_ROLE_RESOURCE
(
  ROLE_ID     NUMBER,
  RESOURCE_ID NUMBER
);

comment on table CRL_BASE_ROLE_RESOURCE
  is '统一登录角色资源表';
comment on column CRL_BASE_ROLE_RESOURCE.ROLE_ID
  is '关联HGP_BASE_ROLE';
comment on column CRL_BASE_ROLE_RESOURCE.RESOURCE_ID
  is '关联HGP_BASE_RESOURCE';
create index IND_CRL_BASE_ROLE_RESOURCE_1 on CRL_BASE_ROLE_RESOURCE (ROLE_ID)
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-- 用户角色表
create table CRL_BASE_USER_ROLE
(
  USER_ID NUMBER,
  ROLE_ID NUMBER
);

comment on table CRL_BASE_USER_ROLE
  is '统一登录用户角色表';
comment on column CRL_BASE_USER_ROLE.USER_ID
  is '用户id，关联HGP_BASE_USER';
comment on column CRL_BASE_USER_ROLE.ROLE_ID
  is '角色id，关联HGP_BASE_ROLE';
create index IND_CRL_BASE_USER_ROLE_1 on CRL_BASE_USER_ROLE (USER_ID)
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-- 多系统系统信息表 CRL_BASE_AUTH_SYSTEM
create table CRL_BASE_AUTH_SYSTEM
(
    ID          NUMBER,
    SYSTEM_NAME VARCHAR2(50),
    PRINCIPAL   VARCHAR2(20),
    CREATED_BY  VARCHAR2(20),
    SYSTEM_DESC VARCHAR2(200),
    CREATED_AT  DATE,
    MODIFIED_BY VARCHAR2(20),
    MODIFIED_AT DATE
);

comment on column CRL_BASE_AUTH_SYSTEM.SYSTEM_NAME
    is '系统名称';
comment on column CRL_BASE_AUTH_SYSTEM.PRINCIPAL
    is '系统负责人';
comment on column CRL_BASE_AUTH_SYSTEM.CREATED_BY
    is '创建人';
comment on column CRL_BASE_AUTH_SYSTEM.SYSTEM_DESC
    is '系统描述';
comment on column CRL_BASE_AUTH_SYSTEM.CREATED_AT
    is '创建时间';
comment on column CRL_BASE_AUTH_SYSTEM.MODIFIED_BY
    is '修改人';
comment on column CRL_BASE_AUTH_SYSTEM.MODIFIED_AT
    is '修改时间';
-- 序列
create sequence SEQ_CRL_BASE_AUTH_SYSTEM
    minvalue 1
    maxvalue 9999999999999999999999999999
    start with 90
    increment by 1
    cache 20;

