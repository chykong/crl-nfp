package com.cars.nfp.feign;

import com.cars.nfp.dto.LoginInfoDTO;
import com.cars.util.global.GlobalReturnCode;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.json.JsonUtil;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 用户服务调用
 *
 * @author 宋长军
 * @date 2019/8/14 9:09
 */
@FeignClient(value = "crl-nfp-api-user", fallback = UserAppClient.UserAppClientFallBack.class)
public interface UserAppClient {

    /**
     * 登录操作
     *
     * @param loginInfoDTO 登录信息
     * @return 登录结果
     */
    @PostMapping("/login")
    JsonResult login(@RequestBody LoginInfoDTO loginInfoDTO);

    /**
     * 根据用户名获取角色列表，URI权限路径列表
     *
     * @param username 用户名
     * @return 权限列表
     */
    @PostMapping("/auth")
    JsonResult auth(String username);

    /**
     * 调用用户接口异常
     */
    @Component
    class UserAppClientFallBack implements UserAppClient {
        @Override
        public JsonResult login(LoginInfoDTO loginInfoDTO) {
            return JsonResultUtil.error("调用用户服务异常！");
        }

        @Override
        public JsonResult auth(String username) {
            return JsonResultUtil.error("调用用户服务异常！");
        }
    }
}
