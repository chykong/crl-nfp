package com.cars.nfp.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 退出登录
 *
 * @author 宋长军
 * @date 2019/8/20 17:41
 */
@Data
public class LogoutDTO {

    /**
     * token信息
     */
    @NotBlank
    private String token;
}
