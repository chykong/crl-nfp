package com.cars.nfp.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 刷新token接受参数
 *
 * @author 宋长军
 * @date 2019/8/20 10:04
 */
@Data
public class RefreshTokenDTO {

    /**
     * refreshOTken
     */
    @NotBlank
    private String refreshToken;
}
