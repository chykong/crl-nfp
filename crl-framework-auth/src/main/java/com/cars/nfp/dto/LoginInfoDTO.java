package com.cars.nfp.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 接受登录信息
 *
 * @author 宋长军
 * @date 2019/8/16 15:11
 */
@Data
public class LoginInfoDTO {

    /**
     * 登录用户名
     */
    @NotNull
    private String username;

    /**
     * 登录用户密码
     */
    @NotNull
    private String password;

    /**
     * 验证码
     */
    private String code;

    /**
     * 标识位
     */
    private Integer flag;
}
