package com.cars.nfp.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * 权限校验参数
 *
 * @author 宋长军
 * @date 2019/8/19 14:38
 */
@Data
@NoArgsConstructor
public class CanAccessDTO {

    /**
     * 请求 url
     */
    @NotBlank
    private String apiUrl;

    /**
     * token 信息
     */
    @NotBlank
    private String token;

    public CanAccessDTO(@NotBlank String apiUrl, @NotBlank String token) {
        this.apiUrl = apiUrl;
        this.token = token;
    }
}
