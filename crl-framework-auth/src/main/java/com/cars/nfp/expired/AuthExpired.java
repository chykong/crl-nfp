package com.cars.nfp.expired;

/**
 * 存储权限过期时间
 *
 * @author 宋长军
 * @date 2019/8/19 8:55
 */
public interface AuthExpired {

    /**
     * TOKEN 过期时间 2小时
     */
    long TOKEN_EXPIRED = 2 * 60 * 60 * 1000L;

    /**
     * REFRESH_TOKEN 过期时间 30天
     */
    long REFRESH_TOKEN_EXPIRED = 30 * 24 * 60 * 60 * 1000L;

    /**
     * 用户权限列表过期时间
     */
    long AUTH_LIST_EXPIRED = 24 * 60 * 60 * 1000L;
}
