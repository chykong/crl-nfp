package com.cars.nfp.controller;

import com.cars.nfp.dto.CanAccessDTO;
import com.cars.nfp.dto.LoginInfoDTO;
import com.cars.nfp.dto.LogoutDTO;
import com.cars.nfp.dto.RefreshTokenDTO;
import com.cars.nfp.service.AuthorizedService;
import com.cars.nfp.service.LoginService;
import com.cars.nfp.service.TokenService;
import com.cars.util.json.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 鉴权服务
 *
 * @author 宋长军
 * @date 2019/8/16 15:07
 */
@RestController
public class AuthController {

    @Autowired
    private AuthorizedService authorizedService;
    @Autowired
    private LoginService loginService;
    @Autowired
    private TokenService tokenService;

    /**
     * 用户登录
     *
     * @param loginInfoDTO 登录参数
     * @return 登录信息
     */
    @PostMapping("/login")
    public JsonResult login(@Valid @RequestBody LoginInfoDTO loginInfoDTO) {

        return loginService.login(loginInfoDTO);
    }

    /**
     * 用户登出操作
     *
     * @param logoutDTO 用户名
     * @return 退出结果
     */
    @PostMapping("/logout")
    public JsonResult logout(@Valid @RequestBody LogoutDTO logoutDTO) {

        return loginService.logout(logoutDTO);
    }

    /**
     * 根据refreshToken获取token信息
     *
     * @param refreshToken refreshtoken
     * @return token信息
     */
    @PostMapping("/refreshToken")
    public JsonResult getTokenByRefresh(@Valid @RequestBody RefreshTokenDTO refreshToken) {

        return tokenService.getTokenByRefresh(refreshToken.getRefreshToken());
    }

    /**
     * 判断请求是否有权限
     *
     * @param canAccessDTO 请求信息
     * @return 判断权限结果
     */
    @PostMapping("/canAccess")
    public JsonResult canAccess(@Valid @RequestBody CanAccessDTO canAccessDTO) {

        return authorizedService.canAccess(canAccessDTO);
    }
}
