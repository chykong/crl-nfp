package com.cars.nfp.service;

import com.cars.nfp.expired.AuthExpired;
import com.crl.redis.RedisKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 鉴权服务Redis操作
 *
 * @author 宋长军
 * @date 2019/8/21 9:29
 */
@Service
public class AuthRedisService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 存储<username, resourceList>
     *
     * @param username  username
     * @param resources 权限列表
     */
    public void saveUserAuth(String username, List<String> resources) {

        String userAuthKey = RedisKey.HGP_AUTHORIZED_URL + username;
        deleteKey(RedisKey.HGP_AUTHORIZED_URL, username);
        // <username, resourceList>
        ListOperations<String, String> listOperations = stringRedisTemplate.opsForList();
        if (CollectionUtils.isEmpty(resources)) {

            resources = Collections.singletonList("Unauthorization");
        }
        listOperations.rightPushAll(userAuthKey, resources);
        stringRedisTemplate.expire(userAuthKey, AuthExpired.AUTH_LIST_EXPIRED, TimeUnit.MILLISECONDS);
    }

    /**
     * 获取<username, resourceList>
     *
     * @param username 用户名
     * @return 权限列表
     */
    public List<String> getUserAuth(String username) {

        String userAuthKey = RedisKey.HGP_AUTHORIZED_URL + username;
        ListOperations<String, String> listOperations = stringRedisTemplate.opsForList();
        return listOperations.range(userAuthKey, 0, -1);
    }

    /**
     * 存储<username, accessToken>
     *
     * @param username    用户名
     * @param accessToken 用户对应的accessToken
     */
    public void saveUserAccessToken(String username, String accessToken) {

        String userAccessTokenKey = RedisKey.HGP_USER_ACCESSTOKEN + username;
        stringRedisTemplate.opsForList().rightPush(userAccessTokenKey, accessToken);
        stringRedisTemplate.expire(userAccessTokenKey, AuthExpired.TOKEN_EXPIRED, TimeUnit.MILLISECONDS);
    }

    /**
     * 删除 redisKey
     *
     * @param redisKey key 前缀
     * @param username key 后缀
     */
    public void deleteKey(String redisKey, String username) {

        String key = redisKey + username;
        stringRedisTemplate.delete(key);
    }

    /**
     * 获取用户对用的accessToken
     *
     * @param username 用户名
     * @return accessToken
     */
    public List<String> getUserAccessToken(String username) {

        String userAccessTokenKey = RedisKey.HGP_USER_ACCESSTOKEN + username;
        return stringRedisTemplate.opsForList().range(userAccessTokenKey, 0, -1);
    }

    /**
     * 判断redis中是否有对应的键
     *
     * @param redisKey 键 前缀
     * @param username 键 后缀
     * @return true：存在
     */
    public boolean isPresence(String redisKey, String username) {

        String key = redisKey + username;
        Boolean boo = stringRedisTemplate.hasKey(key);
        return boo == null ? false : boo;
    }
}
