package com.cars.nfp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 鉴权服务
 *
 * @author 宋长军
 * @date 2020/6/16 16:09
 */
@EnableFeignClients
@SpringBootApplication
@EnableSwagger2
@EnableEurekaClient
@EnableDiscoveryClient
public class CrlFrameworkAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrlFrameworkAuthApplication.class, args);
    }
}
