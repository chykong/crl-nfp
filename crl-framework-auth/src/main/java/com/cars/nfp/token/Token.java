package com.cars.nfp.token;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * TOKEN 信息
 *
 * @author 宋长军
 * @date 2019/8/15 17:37
 */
@Data
@AllArgsConstructor
public class Token implements Serializable {

    private static final long serialVersionUID = 8620625771460837795L;
    /**
     * 获取到的token
     */
    private String accessToken;

    /**
     * 刷新所用token
     */
    private String refreshToken;
}
