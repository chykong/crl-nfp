package com.cars.util.sign;

/**
 * @description:
 * @author: 孔垂云
 * @version: 1.0
 * @date: 2020/4/16 14:30
 */
public class SignCreateUtil {
    /**
     * 创建密钥对
     *
     * @param privateKeyPath 私钥地址 G:\key\hy_private.key
     * @param publicKeyPath  公钥地址 G:\key\hy_private.key
     */
    public static void createKeyPair(String privateKeyPath, String publicKeyPath) {
        RsaEncrypt rsaEncrypt = new RsaEncrypt();
        //创建密钥对
        rsaEncrypt.genKeyPair();
        SignUtil.writeObject(privateKeyPath, rsaEncrypt.getPrivateKey());
        SignUtil.writeObject(publicKeyPath, rsaEncrypt.getPublicKey());
        System.out.println("创建完毕");
    }
}
