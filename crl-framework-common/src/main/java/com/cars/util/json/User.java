package com.cars.util.json;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * @description:
 * @author: 孔垂云
 * @version: 1.0
 * @date: 2020/6/1 11:56
 */
@Data
@ToString
public class User {
    private int id;
    private String name;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date date;
}
