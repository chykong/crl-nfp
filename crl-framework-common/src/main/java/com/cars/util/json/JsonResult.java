package com.cars.util.json;

import com.cars.util.global.GlobalReturnCode;
import lombok.Data;

import java.io.Serializable;

/**
 * 所有Rest接口返回公共对象
 *
 * @param <T> Data 参数类型
 */
@Data
public class JsonResult<T> implements Serializable {
    /**
     * 消息
     */
    private String msg;
    /**
     * 返回码
     */
    private String returnCode;

    /**
     * 数据
     */
    private T data;

    public JsonResult() {
        this(GlobalReturnCode.OK);
    }

    public JsonResult(String returnCode, String msg, T data) {
        this.msg = msg;
        this.returnCode = returnCode;
        this.data = data;
    }

    public JsonResult(String returnCode, String msg) {
        this.msg = msg;
        this.returnCode = returnCode;
    }

    public JsonResult(GlobalReturnCode globalReturnCode) {
        this.returnCode = globalReturnCode.getReturnCode();
        this.msg = globalReturnCode.getMsg();
    }

    public JsonResult(GlobalReturnCode globalReturnCode, T data) {
        this.returnCode = globalReturnCode.getReturnCode();
        this.msg = globalReturnCode.getMsg();
        this.data = data;
    }

    public JsonResult(GlobalReturnCode globalReturnCode, String msg, T data) {
        this.returnCode = globalReturnCode.getReturnCode();
        this.msg = msg;
        this.data = data;
    }

    public boolean success() {
        return GlobalReturnCode.OK.getReturnCode().equals(this.returnCode);
    }

    @Override
    public String toString() {
        return "JsonResult{" +
                "msg='" + msg + '\'' +
                ", returnCode='" + returnCode + '\'' +
                ", data=" + data +
                '}';
    }
}
