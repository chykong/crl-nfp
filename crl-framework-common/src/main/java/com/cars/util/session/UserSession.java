package com.cars.util.session;

import java.io.Serializable;

/**
 * 系统用户Session信息 该类可以根据实际信息进行修改
 *
 * @author 孔垂云
 * @date 2017-05-23
 */

public class UserSession implements Serializable {

	private static final long serialVersionUID = 1L;
	private int userId;//用户id
    private String userIp;//用户IP

    private String username;//用户名  即登录账号
    private String realname;//真实姓名
    private int roleId;//角色id
    private String roleName;//角色名称
    private int corpId = 0;//公司id
    private String avatar;//头像地址
    
    private Integer userLevel;//用户级别1总公司2路局3站段4车站
    private String ljdm;//路局代码
    private String cwddm;//车务段代码
    private String czdm;//车站代码
    private Integer gw;//岗位1、 huo yun、2车务、3车辆
    private String createBy;//创建人
    private String contacts;//联系人
    private String telephone;//联系电话
    private Integer firstUpdate;//是否修改过密码，0表示未修改过，1表示修改过

    public Integer getGw() {
        return gw;
    }

    public void setGw(Integer gw) {
        this.gw = gw;
    }

    public Integer getFirstUpdate() {
		return firstUpdate;
	}

	public void setFirstUpdate(Integer firstUpdate) {
		this.firstUpdate = firstUpdate;
	}

	public String getContacts() {
		return contacts;
	}

	public void setContacts(String contacts) {
		this.contacts = contacts;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getCzdm() {
        return czdm;
    }

    public void setCzdm(String czdm) {
        this.czdm = czdm;
    }

    public String getLjdm() {
        return ljdm;
    }

    public void setLjdm(String ljdm) {
        this.ljdm = ljdm;
    }

    public String getCwddm() {
        return cwddm;
    }

    public void setCwddm(String cwddm) {
        this.cwddm = cwddm;
    }
    public Integer getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(Integer userLevel) {
		this.userLevel = userLevel;
	}

	public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getCorpId() {
        return corpId;
    }

    public void setCorpId(int corpId) {
        this.corpId = corpId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("UserSession{");
        sb.append("userId=").append(userId);
        sb.append(", userIp='").append(userIp).append('\'');
        sb.append(", username='").append(username).append('\'');
        sb.append(", realname='").append(realname).append('\'');
        sb.append(", roleId=").append(roleId);
        sb.append(", roleName='").append(roleName).append('\'');
        sb.append(", corpId=").append(corpId);
        sb.append(", avatar='").append(avatar).append('\'');
        sb.append(", userLevel=").append(userLevel);
        sb.append(", ljdm='").append(ljdm).append('\'');
        sb.append(", cwddm='").append(cwddm).append('\'');
        sb.append(", czdm='").append(czdm).append('\'');
        sb.append(", gw='").append(gw).append('\'');
        sb.append(", createBy='").append(createBy).append('\'');
        sb.append(", contacts='").append(contacts).append('\'');
        sb.append(", telephone='").append(telephone).append('\'');
        sb.append(", firstUpdate=").append(firstUpdate);
        sb.append('}');
        return sb.toString();
    }
}
