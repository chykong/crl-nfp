package com.cars.util.global;

/**
 * 操作类型
 */
public enum OperateTypeEnum {
    //操作类型
    ADD("新增", "I"),
    UPDATE("修改", "U"),
    DELETE("删除", "D");

    /**
     * name
     */
    private String name;

    /**
     * 值
     */
    private String value;

    OperateTypeEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
