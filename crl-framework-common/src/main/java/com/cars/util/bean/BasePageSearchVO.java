package com.cars.util.bean;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class BasePageSearchVO {
    @NotNull
    private Integer pageIndex;
    @NotNull
    private Integer pageSize;

    public BasePageSearchVO() {
    }

    public BasePageSearchVO(@NotNull Integer pageIndex, @NotNull Integer pageSize) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
    }
}
