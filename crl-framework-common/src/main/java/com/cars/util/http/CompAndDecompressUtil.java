package com.cars.util.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * 压缩解压缩工具类
 * @author zhaoyp
 * @date 2013-11-08
 */
public class CompAndDecompressUtil {

	/**  
     * 对应C++ Zlib压缩  
     * @param data    待压缩数据  
     * @return byte[] 压缩后的数据  
     */  
    public static byte[] compress(byte[] data) {   
        byte[] output = new byte[0];   
  
        Deflater compresser = new Deflater();   
  
        compresser.reset();   
        compresser.setInput(data);   
        compresser.finish();   
        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length);   
        try {   
            byte[] buf = new byte[1024];   
            while (!compresser.finished()) {   
                int i = compresser.deflate(buf);   
                bos.write(buf, 0, i);   
            }   
            output = bos.toByteArray();   
        } catch (Exception e) {   
            output = data;   
            e.printStackTrace();   
        } finally {   
            try {   
                bos.close();   
            } catch (IOException e) {   
                e.printStackTrace();   
            }   
        }   
        compresser.end();   
        return output;   
    } 
    
    
    /**  
     * 对应C++ Zlib解压缩    
     * @param data    待压缩的数据  
     * @return byte[] 解压缩后的数据   
     */  
    public static byte[] decompress(byte[] data) {   
        byte[] output = new byte[0];   
  
        Inflater decompresser = new Inflater();   
        decompresser.reset();   
        decompresser.setInput(data);   
  
        ByteArrayOutputStream o = new ByteArrayOutputStream(data.length);   
        try {   
            byte[] buf = new byte[1024];   
            while (!decompresser.finished()) {   
                int i = decompresser.inflate(buf);   
                o.write(buf, 0, i);   
            }   
            output = o.toByteArray();   
        } catch (Exception e) {   
            output = data;   
            e.printStackTrace();   
        } finally {   
            try {   
                o.close();   
            } catch (IOException e) {   
                e.printStackTrace();   
            }   
        }   
  
        decompresser.end();   
        return output;   
    } 
	public static void main(String[] args) {
		

	}

}
