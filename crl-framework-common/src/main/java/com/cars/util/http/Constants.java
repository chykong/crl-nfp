package com.cars.util.http;


public class Constants {

    /**
     * 业务数据加密算法KEY
     */
    public static final String CRC_HP_BUSINESS_KEY = "12345678";

    /**
     * CHANNEL_ENCODE,CHANNEL_PASSWORD 在传输通道中用到 MsgChannel.java中
     *
     * @author zhaoyp
     * @date 2013-11-12 添加
     */
    //编码解码格式
    public static final String CHANNEL_ENCODE = "GBK";
    //加密解密秘钥
    public static final String CHANNEL_PASSWORD = "12345678";

    /**
     * 图片文件后缀
     */
    public static final String IMAGE_EXP = "jpg@jpeg@png@dwg@gif@JPG@JPEG@PNG@DWG@GIF";

    //接收状态 0未接受 1已接收
    public static final String JSZT_STATUS_NO = "0";
    public static final String JSZT_STATUS_YES = "1";
}

