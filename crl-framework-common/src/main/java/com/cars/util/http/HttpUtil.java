package com.cars.util.http;

import com.cars.util.log.LogUtil;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLException;
import java.io.*;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * what: http工具类
 *
 * @author LiHong created on 2018年2月24日
 */
public class HttpUtil {
    public static HttpRequestRetryHandler myRetryHandler = new HttpRequestRetryHandler() {
        @Override
        public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
            if (executionCount >= 5) {
                // Do not retry if over max retry count
                return false;
            }
            if (exception instanceof InterruptedIOException) {
                // Timeout
                return false;
            }
            if (exception instanceof UnknownHostException) {
                // Unknown host
                return false;
            }
            if (exception instanceof ConnectTimeoutException) {
                // Connection refused
                return false;
            }
            if (exception instanceof SSLException) {
                // SSL handshake exception
                return false;
            }
            HttpClientContext clientContext = HttpClientContext.adapt(context);
            HttpRequest request = clientContext.getRequest();
            boolean idempotent = !(request instanceof HttpEntityEnclosingRequest);
            if (idempotent) {
                // Retry if the request is considered idempotent
                return true;
            }
            return false;
        }
    };

    /**
     * what: 发送http的get请求
     *
     * @param url
     * @return
     * @author LiHong created on 2018年2月24日
     */
    public static String httpGet(String url) {
        InputStream is = null;
        BufferedReader br = null;
        String ret = null;
        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpGet httpGet = new HttpGet(url);
            // 设置超时时间
            // setConnectTimeout：设置连接超时时间，单位毫秒。
            // setConnectionRequestTimeout：设置从connect
            // Manager获取Connection超时时间，单位毫秒。
            // setSocketTimeout：请求获取数据的超时时间，单位毫秒。
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(10000)
                    .setConnectionRequestTimeout(10000).setSocketTimeout(10000).build();
            httpGet.setConfig(requestConfig);
            httpGet.setHeader("connection", "Keep-Alive");
            HttpResponse httpResponse = httpClient.execute(httpGet);
            // 连接成功
            ret = getResponse(httpResponse, is, br);
            httpGet.releaseConnection();
        } catch (Exception e) {
//            LogUtil.infoHttp(e.getMessage());
        } finally {
            try {
                if (br != null)
                    br.close();
                if (is != null)
                    is.close();
            } catch (IOException e) {
//                LogUtil.infoHttp(e.getMessage());
            }
        }
        return ret;
    }

    /**
     * what: 发送http的post请求
     *
     * @param url
     * @param hashMap
     * @return
     * @author LiHong created on 2018年2月27日
     */
    public static String httpPost(String url, Map<String, String> hashMap) {
        InputStream is = null;
        BufferedReader br = null;
        String ret = null;
        CloseableHttpClient httpClient = null;
        HttpPost httpPost = null;
        try {
            // HttpClient httpClient = HttpClients.createDefault();
            httpClient = HttpClients.custom().setRetryHandler(HttpUtil.myRetryHandler).build();
            httpPost = new HttpPost(url);
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000)
                    .setConnectionRequestTimeout(30000).setSocketTimeout(30000).build();
            httpPost.setConfig(requestConfig);
            httpPost.setHeader("connection", "Keep-Alive");
            /*
             * 如果需要发送请求参数，可调用HttpGet、HttpPost共同的setParams(HetpParams params)方法
             * 来添加请求参数；对于HttpPost对象而言，也可调用setEntity(HttpEntity entity)方法来设置请求参数。
             */

            // 传递多个参数时
            List<NameValuePair> parameters = new ArrayList<>();
            Iterator<Map.Entry<String, String>> iter = hashMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String, String> entry = iter.next();
                parameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }

            httpPost.setEntity(new UrlEncodedFormEntity(parameters, Consts.UTF_8));
            CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
            // 连接成功
            ret = getResponse(httpResponse, is, br);
            httpPost.releaseConnection();
            httpResponse.close();
        } catch (Exception e) {
//            LogUtil.infoHttp(e.getMessage());
        } finally {
            try {
                if (br != null)
                    br.close();
                if (is != null)
                    is.close();
                httpClient.close();

            } catch (IOException e) {
//                LogUtil.infoHttp(e.getMessage());
            }
        }
        return ret;
    }

    /**
     * what: 获取返回值
     *
     * @param httpResponse
     * @param is
     * @param br
     * @return
     * @throws Exception
     * @author LiHong created on 2018年2月24日
     */
    private static String getResponse(HttpResponse httpResponse, InputStream is, BufferedReader br) throws Exception {

        if (200 == httpResponse.getStatusLine().getStatusCode()) {
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null)
                return EntityUtils.toString(httpEntity, "UTF-8");
        }
        return null;
    }

    public static String postJson(String url, String jsonParam) {
//        LogUtil.infoHttp("请求url：" + url + ";请求参数：" + jsonParam);
        String getJson = "";
        CloseableHttpClient httpclient = null;
        HttpPost httpPost = null;
        try {

            httpclient = HttpClients.custom().setRetryHandler(HttpUtil.myRetryHandler).build();
            httpPost = new HttpPost(url);

            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000)
                    .setConnectionRequestTimeout(30000).setSocketTimeout(30000).build();
            httpPost.setConfig(requestConfig);

            StringEntity entity = new StringEntity(jsonParam.toString(), "utf-8");// 解决中文乱码问题
            entity.setContentEncoding("UTF-8");
            entity.setContentType("application/json");
            httpPost.setEntity(entity);

            CloseableHttpResponse response = httpclient.execute(httpPost);
            response.getStatusLine().getStatusCode();
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String str = "";
            while ((str = rd.readLine()) != null) {
                getJson += str;
            }
//            LogUtil.infoHttp("返回值：" + getJson);

            response.close();
        } catch (Exception e) {
//            LogUtil.infoHttp(e.getMessage());
        } finally {
            httpPost.abort();
            httpPost.releaseConnection();
            try {
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return getJson;
    }

    public static String postJson(String url, String jsonParam, String contentType) {
//        LogUtil.infoHttp("请求url：" + url + ";请求参数：" + jsonParam);
        String getJson = "";
        CloseableHttpClient httpclient = null;
        HttpPost httpPost = null;
        try {

            httpclient = HttpClients.custom().setRetryHandler(HttpUtil.myRetryHandler).build();
            httpPost = new HttpPost(url);

            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000)
                    .setConnectionRequestTimeout(30000).setSocketTimeout(30000).build();
            httpPost.setConfig(requestConfig);

            StringEntity entity = new StringEntity(jsonParam.toString(), "utf-8");// 解决中文乱码问题
            entity.setContentEncoding("UTF-8");
            entity.setContentType(contentType);
            httpPost.setEntity(entity);

            CloseableHttpResponse response = httpclient.execute(httpPost);
            response.getStatusLine().getStatusCode();
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String str = "";
            while ((str = rd.readLine()) != null) {
                getJson += str;
            }
//            LogUtil.infoHttp("返回值：" + getJson);

            response.close();
        } catch (Exception e) {
//            LogUtil.infoHttp(e.getMessage());
        } finally {
            httpPost.abort();
            httpPost.releaseConnection();
            try {
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return getJson;
    }

    /**
     * 发post请求 请求头添加appid
     *
     * @param url       url
     * @param jsonParam json
     * @param appid     appid
     * @return 结果
     */
    public static String post(String url, String jsonParam, String appid) {

//        LogUtil.infoHttp("请求url：" + url + ";请求参数：" + jsonParam);
        StringBuilder getJson = new StringBuilder();
        CloseableHttpClient httpclient = null;
        HttpPost httpPost = null;
        try {

            httpclient = HttpClients.custom().setRetryHandler(HttpUtil.myRetryHandler).build();
            httpPost = new HttpPost(url);

            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000)
                    .setConnectionRequestTimeout(30000).setSocketTimeout(30000).build();
            httpPost.setConfig(requestConfig);

            StringEntity entity = new StringEntity(jsonParam, "utf-8");
            entity.setContentEncoding("UTF-8");
            entity.setContentType("application/json");
            httpPost.setEntity(entity);
            httpPost.setHeader("appid", appid);

            CloseableHttpResponse response = httpclient.execute(httpPost);
            response.getStatusLine().getStatusCode();
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String str;
            while ((str = rd.readLine()) != null) {
                getJson.append(str);
            }
//            LogUtil.infoHttp("返回值：" + getJson);

            response.close();
        } catch (Exception e) {
//            LogUtil.infoHttp(e.getMessage());
        } finally {
            if (httpPost != null) {
                httpPost.abort();
                httpPost.releaseConnection();
                try {
                    httpclient.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return getJson.toString();
    }

    /**
     * post请求 一个参数
     *
     * @param url
     * @param jsonParam
     * @return
     */
    public static String postPara(String url, String jsonParam) {
        LogUtil.info(LogUtil.LOG_HTTP, "请求url：" + url + ";请求参数：" + jsonParam);
        String getJson = "";
        CloseableHttpClient httpclient = null;
        HttpPost httpPost = null;
        try {

            httpclient = HttpClients.custom().setRetryHandler(HttpUtil.myRetryHandler).build();
            httpPost = new HttpPost(url);

            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000)
                    .setConnectionRequestTimeout(30000).setSocketTimeout(30000).build();
            httpPost.setConfig(requestConfig);

            StringEntity entity = new StringEntity(jsonParam.toString(), "utf-8");// 解决中文乱码问题
            entity.setContentEncoding("UTF-8");
            entity.setContentType("application/json");
            httpPost.setEntity(entity);

            CloseableHttpResponse response = httpclient.execute(httpPost);
            response.getStatusLine().getStatusCode();
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String str = "";
            while ((str = rd.readLine()) != null) {
                getJson += str;
            }
            LogUtil.info(LogUtil.LOG_HTTP, "返回值："+ getJson);

            response.close();
        } catch (Exception e) {
            LogUtil.error(LogUtil.LOG_HTTP, "参数错误信息：{}"+ e.getMessage());
        } finally {
            httpPost.abort();
            httpPost.releaseConnection();
            try {
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return getJson;
    }
}
