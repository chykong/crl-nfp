package com.cars.util.dom4j;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * what:   dom4j 解析xml
 *
 * @author 刘艳超
 * @date 2018年2月28日
 */
public class Dom4jUtil {
    /**
     * what:    获取根节点
     *
     * @param filePath
     * @return
     * @throws DocumentException
     * @author 刘艳超
     * @date 2018年2月28日
     */
   /* public static Element getRootEle(String xml) throws DocumentException {
        // 创建saxReader对象
        SAXReader reader = new SAXReader();
        // 通过read方法读取一个文件 转换成Document对象
        Document document = DocumentHelper.parseText(xml);
        //获取根节点元素对象
        Element rootElement = document.getRootElement();
        return rootElement;
    }*/

    /**
     * what:   获取code标签内值.
     *
     * @author 刘艳超
     * @date 2018年2月28日
     */
    /*public static String getval(String filePath, String label) throws DocumentException {
        Element rootEle = getRootEle(filePath);
        String name = rootEle.getQName(label).getName();
        return name;
    }*/

    /**
     * <Info>
     *    <mode>D00001</mode>
     *    <code>-9999</code>
     *    <msg>业务执行失败！
     *    错误详细信息：D00001业务不存在，请检查业务配置信息。</msg>
     *    <content/>
     * </Info>
     * 获取报文中某个节点信息
     * @param strTest
     * @return
     */
    public static String getvalue(String strTest,String root,String child) {
        try {
            //1.创建一个工厂
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

            //2.得到解析器
            DocumentBuilder db = dbf.newDocumentBuilder();

            //在编程中，字符串从网络传递
            InputStream is = new ByteArrayInputStream(strTest.getBytes());
            Document dm = db.parse(is);
            NodeList nl = dm.getElementsByTagName(root);

            //5.改进将 Node 换成 Element (Element提供了更加丰富的方法，解决不能指定得到某个子节点的问题)

            //得到第一个人的信息
            Element el = (Element) nl.item(0);

            //指定得到某个子节点
            NodeList nll = el.getElementsByTagName(child);

            //打印name值,这是固定的取法
            String name = ((Element) nll.item(0)).getFirstChild().getNodeValue();
            return name;
        } catch (Exception e) {
            e.printStackTrace();
            return "fail";
        }
    }
}
