package com.cars.util.list;

import com.cars.util.json.JsonUtil;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @description: List操作工具类
 * @author: 孔垂云
 * @version: 1.0
 * @date: 2019/8/28 11:32
 */
public class ListUtils {
    /**
     * 复制list
     *
     * @param list
     * @param <T>
     * @return
     */
    public static <T> List copyList(List<T> list) {
        try {
            if (CollectionUtils.isEmpty(list)) {
                return new ArrayList();
            }
            return JsonUtil.toArray(JsonUtil.toStr(list), list.get(0).getClass());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 复制list
     *
     * @param list
     * @param <T>
     * @return
     */
    public static <T, E> List<E> copyList(List<T> list, Class<E> e) {
        try {
            if (CollectionUtils.isEmpty(list)) {
                return new ArrayList();
            }
            return JsonUtil.toArray(JsonUtil.toStr(list), e);
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * 复制map
     *
     * @param map
     * @return
     */
    public static Map<String, Object> copyMap(Map map) {
        return JsonUtil.toMap(JsonUtil.toStr(map));
    }
}
