package com.cars.util.code;

/**
 * @Description: 公共常量类
 *
 * @author: 王泽
 * @date:  2019/12/12 19:37
 */
public class Constants {

    /**
     * 验证码显示的密码错误次数
     */
    public static final Integer ERROR_VALIDATE_CODE_COUNTS = 2;
    /**
     * 锁定账号操作的密码错误次数
     */
    public static final Integer ERROR_LOGIN_COUNTS = 50;
    /**
     * 账号锁定时长(分钟)
     */
    public static final Integer LOCK_TIME = 30;
}
