package com.cars.util.code;

/**
 * @Description:全局参数配置
 * @Date:2019/12/13 9:21
 * @Author:苗凯旋
 */
public class GlobalConst {
    /**
     * 总管理员admin用户Id
     */
    public static final int ADMIN_USER_ID = 23;
    /**
     * 管理用户角色类型
     */
    public static final String MANAGER_ROLE_TYPE = "0";
    /**
     * 普通用户角色类型
     */
    public static final String COMMON_ROLE_TYPE = "1";
    /**
     * 资源类别-模块
     */
    public static final int RESOURCE_MODULE = 0;
    /**
     * 资源类别-功能
     */
    public static final int RESOURCE_FUNCTION = 1;
    /**
     * 资源最顶层父Id
     */
    public static final int RESOURCE_PARENT = 0;
}
