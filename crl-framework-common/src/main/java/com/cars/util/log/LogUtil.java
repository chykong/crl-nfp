package com.cars.util.log;

import org.slf4j.LoggerFactory;

/**
 * @description:输出日志工具类
 * @author: 孔垂云
 * @version: 1.0
 * @date: 2019/8/28 17:00
 */
public class LogUtil {
  /**
   * 日志类型
   */
  public static final String LOG_SYS = "sysLog";
  public static final String LOG_DAO = "daoLog";
  public static final String LOG_SERVICE = "serviceLog";
  public static final String LOG_CONTROLLER = "controllerLog";
  public static final String LOG_OPERATION = "operationLog";
  public static final String LOG_API = "apiLog";
  public static final String LOG_PERFORMANCE = "performanceLog";
  public static final String LOG_JOB = "jobLog";
  public static final String LOG_HTTP = "httpLog";

  /**
   * 根据日志类型输出info记录
   *
   * @param logType 日志类型
   * @param format  消息格式
   * @param params  消息参数
   */
  public static void info(String logType, String format, Object... params) {
    LoggerFactory.getLogger(logType).info(format, params);
  }

  /**
   * 根据日志类型输出warn记录
   *
   * @param logType 日志类型
   * @param format  消息格式
   * @param params  消息参数
   */
  public static void warn(String logType, String format, Object... params) {
    LoggerFactory.getLogger(logType).warn(format, params);
  }

  /**
   * 根据日志类型输出error记录
   *
   * @param logType 日志类型
   * @param format  消息格式
   * @param params  消息参数
   */
  public static void error(String logType, String format, Object... params) {
    LoggerFactory.getLogger(logType).error(format, params);
  }

  /**
   * 根据日志类型输出debug记录
   *
   * @param logType 日志类型
   * @param format  消息格式
   * @param params  消息参数
   */
  public static void debug(String logType, String format, Object... params) {
    LoggerFactory.getLogger(logType).debug(format, params);
  }

  /**
   * 根据日志类型输出info记录
   *
   * @param c 日志类
   * @param format  消息格式
   * @param params  消息参数
   */
  public static void info(Class<? extends Object> c, String format, Object... params) {
    LoggerFactory.getLogger(c).info(format, params);
  }

  /**
   * 根据日志类型输出warn记录
   *
   * @param c 日志类型
   * @param format  消息格式
   * @param params  消息参数
   */
  public static void warn(Class<? extends Object> c, String format, Object... params) {
    LoggerFactory.getLogger(c).warn(format, params);
  }

  /**
   * 根据日志类型输出error记录
   *
   * @param c 日志类
   * @param format  消息格式
   * @param params  消息参数
   */
  public static void error(Class<? extends Object> c, String format, Object... params) {
    LoggerFactory.getLogger(c).error(format, params);
  }

  /**
   * 根据日志类型输出debug记录
   *
   * @param c 日志类
   * @param format  消息格式
   * @param params  消息参数
   */
  public static void debug(Class<? extends Object> c, String format, Object... params) {
    LoggerFactory.getLogger(c).debug(format, params);
  }


}
