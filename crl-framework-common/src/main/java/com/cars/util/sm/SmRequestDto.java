package com.cars.util.sm;

/**
 * Created by 孔垂云 on 2018/3/30.
 */
public class SmRequestDto {
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 短信内容
     */
    private String text;
    /**
     * 发送类型
     */
    private Integer sendType;

    @Override
    public String toString() {
        return "SmsRequestDto{" + "mobile='" + mobile + '\'' + ", text='" + text + '\'' + ", sendType=" + sendType + '}';
    }

    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
