package com.crl.redis;

/**
 * @autho 孔垂云
 * @date 2017/7/27.
 */
public class RedisKey {

    /**
     * 在线人数
     */
    public static final String ONLINE_USER = "CARS:ONLINE";
    /**
     * 短信验证码key
     */
    public static final String USER_SMSCODE = "USER:SMSCODE";
    /**
     * 忘记密码短信验证码key
     */
    public static final String USER_SMSCODEFORGET = "USER:SMSCODEFORGET";
    /**
     * CRL redis键
     */
    public static final String CRL_BASE = "CRL:GATEWAY:";
    /**
     * 网关_appid_对应的 sfjy/yxrq/ip/uri
     */
    public static final String HGP_AUTHORIZED_URL = CRL_BASE + "APPID:";
    /**
     * 用户对应TOKEN
     */
    public static final String HGP_USER_ACCESSTOKEN = CRL_BASE + "XM:";
}
