package com.cars.nfp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 用户服务
 *
 * @author 宋长军
 * @date 2020/6/15 10:03
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.cars", "com.crl"})
public class CrlNfpApiUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrlNfpApiUserApplication.class, args);
    }
}