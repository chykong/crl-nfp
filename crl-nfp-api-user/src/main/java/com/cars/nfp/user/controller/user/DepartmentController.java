package com.cars.nfp.user.controller.user;

import com.cars.util.json.JsonResult;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import com.crl.user.user.dto.DepUserDto;
import com.crl.user.user.dto.DepartmentDto;
import com.crl.user.user.service.DepartmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 部门相关操作
 */
@RestController
@RequestMapping("/department")
@Api(description = "部门：增删改查。", tags = "06.Department", basePath = "/department")
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;

    /**
     * 新增
     *
     * @param departmentDto
     * @return
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增", notes = "新增部门信息，createBy字段需要传当前用户id，createAt字段不需要传，moditify不需要传", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult saveDepartment(@Validated({AddValidGroup.class}) @RequestBody DepartmentDto departmentDto) {
        return departmentService.save(departmentDto);
    }

    /**
     * 验证部门代码是否存在
     */
    @PostMapping("/check")
    @ApiOperation(value = "验证部门代码", notes = "验证部门代码", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    @ApiImplicitParam(name = "requestMap", dataType = "{\"depCode\":\"资源代码\"}")
    public JsonResult check(@RequestBody Map<String, String> requestMap) {
        return departmentService.checkCode(requestMap);
    }

    /**
     * 验证同一级部门名称是否存在
     */
    @PostMapping("/checkName")
    @ApiOperation(value = "验证部门名称", notes = "验证部门名称", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult checkName(@RequestBody DepartmentDto departmentDto) {
        return departmentService.checkName(departmentDto);
    }

    /**
     * 删除部门
     */
    @PostMapping("/del")
    @ApiOperation(value = "删除", notes = "删除部门信息，只传一个id就可以")
    public JsonResult deleteDepartment(@Validated({DeleteValidGroup.class}) @RequestBody DepartmentDto departmentDto) {
        return departmentService.delete(departmentDto);
    }

    /**
     * 修改部门信息
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改", notes = "修改部门信息，created相关字段不填写，modity_by填写当前用户id，at字段不需要填写", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult updateDepartment(@Validated({UpdateValidGroup.class}) @RequestBody DepartmentDto departmentDto) {
        return departmentService.update(departmentDto);
    }

    /**
     * 修改部门负责人
     */
    @PostMapping("/updateUserid")
    @ApiOperation(value = "修改", notes = "修改部门信息，created相关字段不填写，modity_by填写当前用户id，at字段不需要填写", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult updateUserid(@Validated({UpdateValidGroup.class}) @RequestBody DepUserDto depUserDto) {
        return departmentService.updateUserid(depUserDto);
    }

    /**
     * 获取所有部门信息
     */
    @PostMapping("/tree")
    @ApiOperation(value = "获取所有", notes = "获取所有部门信息树形结构", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<List<DepartmentDto>> tree(@RequestBody Map<String, String> requestMap) {
        return departmentService.tree(requestMap);
    }

    /**
     * 获取所有负责部门父级菜单 --用于异步加载
     */
    @PostMapping("/listParents")
    @ApiOperation(value = "获取所有", notes = "获取所有部门信息树形结构", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    @ApiImplicitParam(name = "requestMap", dataType = "{\"userId\":\"用户Id\"}")
    public JsonResult<List<DepartmentDto>> listParents(@RequestBody Map<String, String> requestMap) {
        return departmentService.listParents(requestMap);
    }

    /**
     * 获取直系子菜单
     */
    @PostMapping("/listChildren")
    @ApiOperation(value = "获取直系子菜单", notes = "获取所有部门信息树形结构", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    @ApiImplicitParam(name = "requestMap", dataType = "{\"userId\":\"用户Id\"}")
    public JsonResult<List<DepartmentDto>> listChildren(@RequestBody Map<String, String> requestMap) {
        return departmentService.listChildren(Integer.valueOf(requestMap.get("parentId")));
    }


    /**
     * 获取所有部门信息
     */
    @PostMapping("/listAll")
    @ApiOperation(value = "获取所有", notes = "获取所有部门信息树形结构", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    @ApiImplicitParam(name = "requestMap", dataType = "{\"userId\":\"用户Id\"}")
    public JsonResult<List<DepartmentDto>> listAll(@RequestBody Map<String, String> requestMap) {
        return departmentService.listAll(requestMap);
    }

    /**
     * 根据id获取角色对象，用于修改
     */
    @PostMapping("/get")
    @ApiOperation(value = "根据id获取部门对象", notes = "根据id获取部门对象，用于修改部门，只传一个id就可以")
    public JsonResult<DepartmentDto> getById(@Validated({DeleteValidGroup.class}) @RequestBody DepartmentDto departmentDto) {
        return departmentService.getById(departmentDto);
    }

    /**
     * 根据parentiId获取部门编码
     */
    @PostMapping("/getDepCode")
    @ApiOperation(value = "根据parentiId获取部门编码", notes = "部门编码按所在层级的个数生成")
    public JsonResult getDepCode(@RequestBody DepartmentDto departmentDto) {
        return departmentService.getDepCode(departmentDto);
    }
}
