package com.cars.nfp.user.controller.user;

import com.cars.util.bean.BasePage;
import com.cars.util.global.GlobalReturnCode;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.jwt.JwtUtil;
import com.cars.util.string.StringUtil;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import com.crl.user.user.dto.RoleDto;
import com.crl.user.user.dto.UserDto;
import com.crl.user.user.service.RoleService;
import com.crl.user.user.service.UserService;
import com.crl.user.user.vo.RoleSearchVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 角色相关操作
 */
@RestController
@RequestMapping("/role")
@Api(description = "角色：增删改查。", tags = "02.Role", basePath = "/role")
public class RoleController {
    @Autowired
    private RoleService roleService;
    @Autowired
    private UserService userService;

    /**
     * 新增角色信息
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增", notes = "新增角色信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult saveRole(@Validated({AddValidGroup.class}) @RequestBody RoleDto roleDto, HttpServletRequest request) {
        Integer systemId = roleDto.getAuthSystemId();
        if(systemId == null){
            String token = request.getHeader("Authorization");
            String username = JwtUtil.parseJWT(token).getOwner();
            UserDto dto = userService.getByUsername(username);
            if(dto != null) {
                roleDto.setAuthSystemId(dto.getAuthSystemId());
            }
        }
        return roleService.saveRole(roleDto);
    }

    /**
     * 验证角色代码是否存在
     */
    @PostMapping("/check")
    @ApiOperation(value = "验证角色代码", notes = "验证角色代码", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    @ApiImplicitParam(name = "requestMap", dataType = "{\"roleCode\":\"资源代码\"}")
    public JsonResult check(@RequestBody Map<String, String> requestMap) {
        if (StringUtil.isNullOrEmpty(requestMap.get("roleCode"))) {
            return JsonResultUtil.error(GlobalReturnCode.PARAMS_ERROR.getReturnCode(), "roleCode 不能为空", null);
        }
        return roleService.chekcRoleCode(requestMap.get("roleCode"));
    }

    /**
     * 删除角色
     */
    @PostMapping("/del")
    @ApiOperation(value = "删除", notes = "删除角色信息")
    public JsonResult deleteResource(@Validated({DeleteValidGroup.class}) @RequestBody RoleDto roleDto) {
        return roleService.deleteRole(roleDto);
    }

    /**
     * 修改角色信息
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改", notes = "修改角色信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult updateRole(@Validated({UpdateValidGroup.class}) @RequestBody RoleDto roleDto) {
        return roleService.updateRole(roleDto);
    }

    /**
     * 获取所有角色信息
     */
    @PostMapping("/listAll")
    @ApiOperation(value = "获取所有", notes = "获取所有角色信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<List<RoleDto>> listAll() {
        return roleService.listAll();
    }

    @PostMapping("/listByUserId")
    public JsonResult<Map<String,Object>> listByUserId(@RequestBody Map<String, Object> requestMap){
        return roleService.listByUserId(Integer.parseInt(requestMap.get("userId").toString()));
    }

    @PostMapping("/listByUserAndSystem")
    public JsonResult<Map<String,Object>> listByUserAndSystem(@RequestBody Map<String, Object> requestMap){
        return roleService.listByUserAndSystem(Integer.parseInt(requestMap.get("userId").toString()), Integer.parseInt(requestMap.get("authSystemId").toString()));
    }

    /**
     * 根据id获取角色对象，用于修改
     */
    @PostMapping("/get")
    @ApiOperation(value = "根据id获取角色对象", notes = "根据id获取角色对象，用于修改角色")
    public JsonResult<RoleDto> getById(@Validated({DeleteValidGroup.class}) @RequestBody RoleDto roleDto) {
        return roleService.getById(roleDto);
    }

    /**
     * 根据条件查询角色
     */
    @PostMapping("/search")
    @ApiOperation(value = "根据条件查询角色", notes = "根据条件查询角色信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<BasePage<List<RoleDto>>> search(@Validated @RequestBody RoleSearchVO roleSearchVO, HttpServletRequest request) {
        Integer authId = roleSearchVO.getAuthSystemId();
        if(authId == null) {
            String token = request.getHeader("Authorization");
            String username = JwtUtil.parseJWT(token).getOwner();
            UserDto dto = userService.getByUsername(username);
            if(dto != null) {
                roleSearchVO.setAuthSystemId(dto.getAuthSystemId());
            }
        }
        return roleService.listSearch(roleSearchVO);
    }
}
