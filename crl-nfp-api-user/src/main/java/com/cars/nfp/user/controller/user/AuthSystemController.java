package com.cars.nfp.user.controller.user;

import com.cars.util.bean.BasePage;
import com.cars.util.global.GlobalReturnCode;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.string.StringUtil;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import com.crl.user.user.dto.AuthSystemDto;
import com.crl.user.user.service.AuthSystemService;
import com.crl.user.user.vo.AuthSystemVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Desc:系统信息controller
 * @author: miaokaixuan
 * @Date: 2020/2/20
 */
@RestController
@RequestMapping("/authSystem")
@Api(description = "系统：增删改查。", tags = "01.authSystem", basePath = "/authSystem")
public class AuthSystemController {
    @Autowired
    private AuthSystemService authSystemService;

    /**
     * 新增系统信息
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增", notes = "新增系统信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult saveAuthSystem(@Validated({AddValidGroup.class}) @RequestBody AuthSystemDto authSystemDto) {
        return authSystemService.saveAuthSystem(authSystemDto);
    }

    /**
     * 修改系统信息
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改", notes = "修改系统信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult updateAuthSystem(@Validated({UpdateValidGroup.class}) @RequestBody AuthSystemDto authSystemDto) {
        return authSystemService.updateAuthSystem(authSystemDto);
    }

    /**
     * 删除系统
     */
    @PostMapping("/del")
    @ApiOperation(value = "删除", notes = "删除系统信息")
    public JsonResult deleteAuthSystem(@Validated({DeleteValidGroup.class}) @RequestBody AuthSystemDto authSystemDto) {
        return authSystemService.deleteAuthSystem(authSystemDto);
    }

    /**
     * 根据id获取系统信息对象，用于修改
     */
    @PostMapping("/get")
    @ApiOperation(value = "根据id获取系统信息对象", notes = "根据id获取系统信息对象，用于修改")
    public JsonResult<AuthSystemDto> getById(@Validated({DeleteValidGroup.class}) @RequestBody AuthSystemDto authSystemDto) {
        return authSystemService.getById(authSystemDto);
    }

    /**
     * 验证权限代码是否存在
     */
    @PostMapping("/check")
    @ApiOperation(value = "验证系统名称", notes = "验证系统名称", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    @ApiImplicitParam(name = "requestMap", dataType = "{\"AuthSystemCode\":\"系统名称\"}")
    public JsonResult check(@RequestBody Map<String, String> requestMap) {
        if (StringUtil.isNullOrEmpty(requestMap.get("systemName"))) {
            return JsonResultUtil.error(GlobalReturnCode.PARAMS_ERROR.getReturnCode(), "系统名称 不能为空", null);
        }
        return authSystemService.checkSystemName(requestMap.get("systemName"), null);
    }

    /**
     * 根据条件查询角色
     */
    @PostMapping("/search")
    @ApiOperation(value = "根据条件查询角色", notes = "根据条件查询角色信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<BasePage<List<AuthSystemDto>>> search(@Validated @RequestBody AuthSystemVO authSystemVO) {
        return authSystemService.listSearch(authSystemVO);
    }

    /**
     * 查询所有的系统信息
     * @return
     */
    @PostMapping("/listAllSystem")
    public JsonResult listAllSystem(@RequestBody Map<String, String> requestMap){
        return authSystemService.listAllSystem(requestMap);
    }
}
