package com.cars.nfp.user.controller.system;

import com.cars.util.bean.BasePage;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.string.StringUtil;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import com.crl.user.system.dto.SystemBindDto;
import com.crl.user.system.service.SystemBindService;
import com.crl.user.system.vo.SystemBindSearchVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/systembind")
@Api(description = "绑定业务系统管理操作：绑定，解绑，修改绑定密码，查询。", tags = "09.SystemBind", basePath = "/systembind")
public class SystemBindController {
    @Autowired
    private SystemBindService systemBindService;

    /**
     * 绑定
     */
    @PostMapping("/bind")
    @ApiOperation(value = "绑定", notes = "当前用户与业务系统绑定", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult bind(HttpServletRequest request, @Validated({AddValidGroup.class}) @RequestBody SystemBindDto systemBindDto) {
        return systemBindService.bind(request, systemBindDto);
    }

    /**
     * 解绑
     */
    @PostMapping("/unbind")
    @ApiOperation(value = "解绑", notes = "传入系统id，用户id，绑定的用户名即可，后台校验成功后自动删除该系统id，用户id，绑定用户名对应的数据", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult unbind(HttpServletRequest request,@Validated({UpdateValidGroup.class}) @RequestBody SystemBindDto systemBindDto) {
        return systemBindService.unbind(request,systemBindDto);
    }

    /**
     * 根据用户id获取可用列表
     */
    @PostMapping("/list")
    @ApiOperation(value = "（首页）查询当前用户绑定的所有数据", notes = "根据用户id查询出所有状态是0，即正常状态的列表，按照系统id和创建时间排序,B/S采用http://webCallbackPath?username=xx&code=xx&random=xx的方式弹出浏览器tab页调用", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<List<SystemBindDto>> listByUserId(@Validated({DeleteValidGroup.class}) @RequestBody SystemBindDto systemBindDto) {
        return systemBindService.listByUserId(systemBindDto);
    }

    /**
     * 根据条件查询列表
     */
    @PostMapping("/search")
    @ApiOperation(value = "查询", notes = "将系统id作为查询条件，并默认传入用户id，进行该用户绑定的信息查询功能", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<BasePage<List<SystemBindDto>>> listSearch(@Valid @RequestBody SystemBindSearchVO systemBindSearchVO) {
        return systemBindService.listSearch(systemBindSearchVO);
    }

    /**
     * 根据系统id，用户id，绑定用户名获取对象
     */
    @PostMapping("/get")
    @ApiOperation(value = "根据系统id，用户id，绑定用户名获取对象", notes = "系统id，用户id，绑定用户名作为联合唯一条件获取对象，用于跳转修改密码功能之前获取数据", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<SystemBindDto> getBy(@Validated({UpdateValidGroup.class}) @RequestBody SystemBindDto systemBindDto) {
        return systemBindService.getBy(systemBindDto);
    }

    /**
     * 修改密码
     */
    @PostMapping("/againbind")
    @ApiOperation(value = "修改绑定用户密码", notes = "该接口用于其他系统修改密码时通过该接口进行绑定密码重置", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult againbind(HttpServletRequest request, @Validated({AddValidGroup.class}) @RequestBody SystemBindDto systemBindDto) {
        String clientIp = StringUtil.getIp(request);
        return systemBindService.updatePwd(systemBindDto, null,"", clientIp);
    }

    /**
     * 测试绑定和解绑地址
     */
    @PostMapping("/test")
    @ApiOperation(value = "测试绑定和解绑地址", notes = "模仿B/S结构系统的绑定和解绑地址", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult test(@RequestBody Map<String, String> userMap) {
        if (StringUtil.isNullOrEmpty(userMap.get("username")) || StringUtil.isNullOrEmpty(userMap.get("password"))) {
            return JsonResultUtil.error();
        }
        return JsonResultUtil.ok();
    }

}
