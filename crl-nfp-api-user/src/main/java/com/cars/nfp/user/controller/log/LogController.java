package com.cars.nfp.user.controller.log;

import com.cars.util.json.JsonResult;
import com.crl.user.log.service.LogService;
import com.crl.user.log.vo.LogVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Date:2019/12/13 17:31
 * @Author:苗凯旋
 */
@RestController
@RequestMapping("/log")
public class LogController {
    @Autowired
    private LogService logService;
    /**
     * 查询日志
     */
    @PostMapping("/findLog")
    @ApiOperation(value = "查询列表", notes = "查询日志,根据日志名查询,pageIndex从1开始，表示当前第几页", response = JsonResult.class)
    public JsonResult findLog(@RequestBody LogVo logVo) {
        return logService.findLog(logVo);
    }
}
