package com.cars.nfp.user.controller.system;

import com.cars.util.bean.BasePage;
import com.cars.util.json.JsonResult;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import com.crl.user.system.dto.SystemCategoryDto;
import com.crl.user.system.service.SystemCategoryService;
import com.crl.user.system.vo.SystemCategorySearchVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 业务系统大类操作
 */
@RestController
@RequestMapping("/systemcategory")
@Api(description = "业务系统大类：增删改查。", tags = "07.SystemCategory", basePath = "/systemcategory")
public class SystemCategoryController {
    @Autowired
    private SystemCategoryService systemCategoryService;

    /**
     * 新增
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增", notes = "新增业务系统大类", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult saveSystemCategory(@RequestBody @Validated({AddValidGroup.class}) SystemCategoryDto systemCategoryDto) {
        return systemCategoryService.saveSystemCategory(systemCategoryDto);
    }

    /**
     * 删除
     */
    @PostMapping("/del")
    @ApiOperation(value = "删除", notes = "删除业务系统大类,只需要传id", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult deleteSystemCategory(@RequestBody @Validated({DeleteValidGroup.class}) SystemCategoryDto systemCategoryDto) {
        return systemCategoryService.deleteSystemCategory(systemCategoryDto);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改", notes = "修改业务系统大类", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult updateSystemCategory(@RequestBody @Validated({UpdateValidGroup.class}) SystemCategoryDto systemCategoryDto) {
        return systemCategoryService.updateSystemCategory(systemCategoryDto);
    }

    /**
     * 按照条件查询
     */
    @PostMapping("/search")
    @ApiOperation(value = "按照条件查询", notes = "按照条件查询业务系统大类", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<BasePage<List<SystemCategoryDto>>> listBySearch(@RequestBody SystemCategorySearchVO systemCategorySearchVO) {
        return systemCategoryService.listBySearch(systemCategorySearchVO);
    }

    /**
     * 根据id查询
     */
    @PostMapping("/get")
    @ApiOperation(value = "按照id查询", notes = "按照id查询业务系统大类,只需要传一个id就可以", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<SystemCategoryDto> getById(@RequestBody SystemCategoryDto systemCategoryDto) {
        return systemCategoryService.getById(systemCategoryDto);
    }

    /**
     * 根据id查询
     */
    @PostMapping("/getMaxOrder")
    @ApiOperation(value = "获取最大排序号", notes = "获取最大排序号", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult getMaxOrder() {
        return systemCategoryService.getMaxOrder();
    }

    /**
     * 查询所有
     */
    @PostMapping("/listAll")
    @ApiOperation(value = "查询所有", notes = "查询所有", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<List<SystemCategoryDto>> listAll() {
        return systemCategoryService.listAll();
    }

    /**
     * 根据用户所在路局查询所有本路局可见的分类
     */
    @PostMapping("/listAllByUserId")
    @ApiOperation(value = "查询所有", notes = "查询所有", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<List<SystemCategoryDto>> listAllByUserId(@RequestBody Map<String, String> requestMap) {
        return systemCategoryService.listAllByUserId(requestMap);
    }
}
