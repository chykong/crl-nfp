package com.cars.nfp.user.controller.system;

import com.cars.util.json.JsonResult;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.crl.user.system.dto.SystemUsedlinkDto;
import com.crl.user.system.service.SystemUsedlinkService;
import com.crl.user.system.vo.SystemUsedlinkSearchVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 常用链接操作
 */
@RestController
@RequestMapping("/systemusedlink")
@Api(description = "常用链接：增删改查。", tags = "07.SystemUsedlink", basePath = "/systemcategory")
public class SystemUsedlinkController {
    @Autowired
    private SystemUsedlinkService systemUsedLinkService;

    /**
     * 新增
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增", notes = "新增常用链接", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult saveSystemUsedlink(@RequestBody @Validated({AddValidGroup.class}) SystemUsedlinkDto systemUsedLinkDto) {
        return systemUsedLinkService.saveSystemUsedlink(systemUsedLinkDto);
    }

    /**
     * 删除
     */
    @PostMapping("/del")
    @ApiOperation(value = "删除", notes = "删除常用链接,只需要传id", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult deleteSystemUsedlink(@RequestBody @Validated({DeleteValidGroup.class}) SystemUsedlinkDto systemUsedLinkDto) {
        return systemUsedLinkService.deleteSystemUsedlink(systemUsedLinkDto);
    }

    /**
     * 按照条件查询
     */
    @PostMapping("/search")
    @ApiOperation(value = "按照条件查询", notes = "按照条件查询常用链接", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<List<SystemUsedlinkDto>> listBySearch(@RequestBody SystemUsedlinkSearchVO systemUsedLinkSearchVO) {
        return systemUsedLinkService.listByCreatedBy(systemUsedLinkSearchVO.getCreatedBy());
    }

    /**
     * 根据id查询
     */
    @PostMapping("/get")
    @ApiOperation(value = "按照id查询", notes = "按照id查询常用链接,只需要传一个id就可以", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<SystemUsedlinkDto> getById(@RequestBody SystemUsedlinkDto systemUsedLinkDto) {
        return systemUsedLinkService.getById(systemUsedLinkDto);
    }

    /**
     * 根据id查询
     */
    @PostMapping("/getMaxOrder")
    @ApiOperation(value = "获取最大排序号", notes = "获取最大排序号", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult getMaxOrder(@RequestBody SystemUsedlinkDto systemUsedLinkDto) {
        return systemUsedLinkService.getMaxOrder(systemUsedLinkDto.getCreatedBy());
    }

    /**
     * 查询所有
     */
    @PostMapping("/listAll")
    @ApiOperation(value = "查询所有", notes = "查询所有", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<List<SystemUsedlinkDto>> listAll() {
        return systemUsedLinkService.listAll();
    }
}
