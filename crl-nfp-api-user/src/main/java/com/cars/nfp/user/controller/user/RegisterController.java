package com.cars.nfp.user.controller.user;

import com.cars.util.json.JsonResult;
import com.crl.user.user.dto.RegisterDto;
import com.crl.user.user.service.RegisterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 注册相关controller
 *
 * @author sunchao
 */
@RestController
@RequestMapping("/register")
@Api(description = "注册：注册；验证用户名。",tags = "04.Register",basePath = "/register")
public class RegisterController {
    @Autowired
    private RegisterService registerService;

    /**
     * 注册
     *
     * @param registerDto 请求实体，包含用户名密码和系统标志
     * @return
     */
    @PostMapping("/user")
    @ApiOperation(value = "注册", notes = "用户主动注册或后台人工添加，前端发起请求", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult register(@RequestBody RegisterDto registerDto) {
        return registerService.register(registerDto);
    }

    /**
     * 校验验证码
     * @param registerDto
     * @return
     */
    @PostMapping("/checkMoibleCode")
    @ApiOperation(value = "校验验证码", notes = "校验验证码", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult checkMoibleCode(@RequestBody RegisterDto registerDto){
        return registerService.checkMoibleCode(registerDto);
    }
}
