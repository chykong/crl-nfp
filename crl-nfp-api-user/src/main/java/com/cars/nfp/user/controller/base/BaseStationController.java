package com.cars.nfp.user.controller.base;

import com.cars.util.json.JsonResult;
import com.crl.user.base.dto.BaseStationDto;
import com.crl.user.base.service.BaseStationService;
import com.crl.user.base.vo.BaseStationSearchVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:车站
 * @Date:2019/11/26 16:44
 * @Author:苗凯旋
 */
@RestController
@RequestMapping("/baseStation")
@Api(description = "车站：增删改查。", tags = "03.BaseStation", basePath = "/baseStation")
public class BaseStationController {
    @Autowired
    private BaseStationService baseStationService;
    /**
     * 查询车站
     */
    @PostMapping("/list")
    @ApiOperation(value = "查询列表", notes = "查询车站,pageIndex从1开始，表示当前第几页", response = JsonResult.class)
    public JsonResult list(@RequestBody BaseStationSearchVO baseStationSearchVO) {
        return baseStationService.list(baseStationSearchVO);
    }
    @PostMapping("/listByHyzxdm")
    @ApiOperation(value = "根据 huo yun中心代码查询车站信息",  response = JsonResult.class)
    public JsonResult listByHyzxdm(@RequestBody BaseStationDto baseStationDto){
        return baseStationService.listByHyzxdm(baseStationDto);
    }
}