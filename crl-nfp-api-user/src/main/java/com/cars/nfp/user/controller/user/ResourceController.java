package com.cars.nfp.user.controller.user;

import com.cars.util.global.GlobalReturnCode;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.jwt.JwtUtil;
import com.cars.util.string.StringUtil;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import com.crl.user.user.dto.ResourceDto;
import com.crl.user.user.dto.ResourceTreeDto;
import com.crl.user.user.dto.UserDto;
import com.crl.user.user.service.ResourceService;
import com.crl.user.user.service.UserService;
import com.netflix.ribbon.proxy.annotation.Http;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 资源相关操作
 */
@RestController
@RequestMapping("/resource")
@Api(description = "资源：增删改查。", tags = "01.Resource", basePath = "/resource")
public class ResourceController {
    @Autowired
    private ResourceService resourceService;
    @Autowired
    private UserService userService;

    /**
     * 新增资源信息
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增", notes = "新增资源信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult saveResource(@Validated({AddValidGroup.class}) @RequestBody ResourceDto resourceDto, HttpServletRequest request) {
        Integer systemId = resourceDto.getAuthSystemId();
        if(systemId == null) {
            String token = request.getHeader("Authorization");
            String username = JwtUtil.parseJWT(token).getOwner();
            UserDto dto = userService.getByUsername(username);
            if(dto != null) {
                resourceDto.setAuthSystemId(dto.getAuthSystemId());
            }
        }
        return resourceService.saveResource(resourceDto);
    }

    /**
     * 修改资源信息
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改", notes = "修改资源信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult updateResource(@Validated({UpdateValidGroup.class}) @RequestBody ResourceDto resourceDto) {
        return resourceService.updateResource(resourceDto);
    }

    /**
     * 删除资源
     */
    @PostMapping("/del")
    @ApiOperation(value = "删除", notes = "删除资源信息")
    public JsonResult deleteResource(@Validated({DeleteValidGroup.class}) @RequestBody ResourceDto resourceDto) {
        return resourceService.deleteResource(resourceDto);
    }

    /**
     * 禁用资源
     */
    @PostMapping("/disable")
    @ApiOperation(value = "禁用", notes = "禁用资源")
    public JsonResult updateStatus(@Validated({DeleteValidGroup.class}) @RequestBody ResourceDto resourceDto) {
        return resourceService.updateStatus(resourceDto);
    }

    /**
     * 验证权限代码是否存在
     */
    @PostMapping("/check")
    @ApiOperation(value = "验证资源代码", notes = "验证资源代码", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    @ApiImplicitParam(name = "requestMap", dataType = "{\"resourceCode\":\"资源代码\"}")
    public JsonResult check(@RequestBody Map<String, String> requestMap) {
        if (StringUtil.isNullOrEmpty(requestMap.get("resourceCode"))) {
            return JsonResultUtil.error(GlobalReturnCode.PARAMS_ERROR.getReturnCode(), "resourceCode 不能为空", null);
        }
        return resourceService.checkCode(requestMap.get("resourceCode"));
    }

    /**
     * 获取所有资源列表，用于生成树
     */
    @PostMapping("/tree")
    @ApiOperation(value = "查询资源树", notes = "获取所有资源列表，用于生成树")
    public JsonResult<List<ResourceTreeDto>> listResources(@RequestBody ResourceDto resourceDto, HttpServletRequest request) {
        Integer authId = resourceDto.getAuthSystemId();
        if(authId == null) {
            String token = request.getHeader("Authorization");
            String username = JwtUtil.parseJWT(token).getOwner();
            UserDto dto = userService.getByUsername(username);
            if(dto != null) {
                authId = dto.getAuthSystemId();
            }
        }
        return resourceService.listResources(authId);
    }

    /**
     * 根据id获取资源对象
     */
    @PostMapping("/get")
    @ApiOperation(value = "根据id获取资源对象", notes = "根据id获取资源对象，用于修改资源")
    public JsonResult<ResourceDto> getById(@Validated({DeleteValidGroup.class}) @RequestBody ResourceDto resourceDto) {
        return resourceService.getById(resourceDto);
    }
}
