package com.cars.nfp.user.controller.system;

import com.cars.util.bean.BasePage;
import com.cars.util.global.GlobalReturnCode;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.string.StringUtil;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import com.crl.user.system.dto.SystemDto;
import com.crl.user.system.model.SystemLj;
import com.crl.user.system.service.SystemService;
import com.crl.user.system.vo.SystemSearchVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 业务系统管理操作
 */
@RestController
@RequestMapping("/system")
@Api(description = "业务系统管理操作：增删改查。", tags = "08.System", basePath = "/system")
public class SystemController {
    @Autowired
    private SystemService systemService;

    /**
     * 新增
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增", notes = "新增业务系统，传个createdBy，at不需要传", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult save(@RequestBody @Validated({AddValidGroup.class}) SystemDto systemDto) {
        return systemService.save(systemDto);
    }

    /**
     * 删除
     */
    @PostMapping("/del")
    @ApiOperation(value = "删除", notes = "删除业务系统", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult delete(@RequestBody @Validated({DeleteValidGroup.class}) SystemDto systemDto) {
        return systemService.delete(systemDto);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改", notes = "修改业务系统，传个modifiedBy，at不需要传", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult update(@RequestBody @Validated({UpdateValidGroup.class}) SystemDto systemDto) {
        return systemService.update(systemDto);
    }

    /**
     * 按照条件查询
     */
    @PostMapping("/search")
    @ApiOperation(value = "按照条件查询", notes = "按照条件查询业务系统", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<BasePage<List<SystemDto>>> search(@RequestBody SystemSearchVO systemSearchVO) {
        return systemService.list(systemSearchVO);
    }

    /**
     * 根据id查询
     */
    @PostMapping("/get")
    @ApiOperation(value = "根据id查询", notes = "根据id查询业务系统,只传一个id就可以", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<SystemDto> get(@RequestBody @Validated({DeleteValidGroup.class}) SystemDto systemDto) {
        return systemService.getById(systemDto);
    }

    /**
     * 获取最大排序号
     */
    @PostMapping("/getMaxOrder")
    @ApiOperation(value = "获取最大排序号", notes = "获取最大排序号", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    @ApiImplicitParam(name = "requestMap", dataType = "{\"cateId\":\"分类id\"}")
    public JsonResult getMaxOrder(@RequestBody Map<String, String> requestMap) {
        if (StringUtil.isNullOrEmpty(requestMap.get("cateId"))) {
            return JsonResultUtil.error(GlobalReturnCode.PARAMS_ERROR.getReturnCode(), "分类ID错误", null);
        }
        return systemService.getMaxOrder(Integer.parseInt(requestMap.get("cateId")));
    }

    /**
     * 查询所有
     */
    @PostMapping("/listAll")
    @ApiOperation(value = "根据条件查询所有", notes = "查询所有业务系统", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    @ApiImplicitParam(name = "requestMap", dataType = "{\"cateId\":\"分类Id\"}")
    public JsonResult<Map<String,Object>> listAll(@RequestBody Map<String, Object> requestMap) {
        return systemService.listAll(requestMap);
    }
    /**
     * 根据dicid查询绑定的系统
     * @param requestMap
     * @return
     */
    @PostMapping("/systemListByDicId")
    @ApiOperation(value = "根据dicid查询绑定的系统", notes = "根据dicid查询绑定的系统", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult systemListByDicId(@RequestBody Map<String, String> requestMap){
        if (StringUtil.isNullOrEmpty(requestMap.get("dicId"))) {
            return JsonResultUtil.error(GlobalReturnCode.PARAMS_ERROR.getReturnCode(), "dicId 不能为空", null);
        }
        return systemService.systemListByDicId(requestMap.get("dicId"));
    }

    /**
     * 查询系统路局可见范围
     */
    @PostMapping("/listSystemLj")
    @ApiOperation(value = "查询系统路局可见范围", notes = "查询系统路局可见范围", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    @ApiImplicitParam(name = "requestMap", dataType = "{\"systemId\":\"系统id\"}")
    public JsonResult<List<SystemLj>> listSystemLj(@RequestBody Map<String, Object> requestMap) {
        return systemService.listSystemLj(requestMap);
    }
}
