package com.cars.nfp.user.controller.user;

import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.validation.VerifyCodeUtils;
import com.crl.user.user.dto.CommonLoginDto;
import com.crl.user.user.dto.UserLoginDto;
import com.crl.user.user.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.Map;

/**
 * 登录相关controller
 *
 * @author sunchao
 */
@RestController
@Api(description = "登录：登录；获取权限资源信息。", tags = "05.Login")
public class LoginController {
    @Autowired
    private LoginService loginService;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 用户登录请求
     *
     * @param userLoginDto 请求实体，包含用户名和密码
     * @return
     */
    @PostMapping(value = "/login", consumes = MimeTypeUtils.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "登录", notes = "用户登录请求，用户授权服务服务间调用", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult login(HttpServletRequest request, @RequestBody UserLoginDto userLoginDto) {
        return loginService.login(request, userLoginDto);
    }

    /**
     * 用户登录请求，用户授权服务服务间调用
     *
     * @return
     */
    @PostMapping("/auth")
    @ApiOperation(value = "根据用户名获取用户对应角色权限信息", notes = "根据用户名获取用户对应角色权限信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult auth(@RequestBody Map<String, String> requestMap) {
        return loginService.auth(requestMap);
    }

    /**
     * 用户登录账号密码错误两次以上，显示验证码
     *
     * @return
     */
    @PostMapping(value = "/login/getCode")
    @ApiOperation(value = "获取验证码", notes = "获取验证码", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult getYzm(HttpServletResponse response, HttpServletRequest request) {
        String base64 = null;
        try {
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setContentType("image/jpg");

            //生成随机字串
            String verifyCode = VerifyCodeUtils.generateVerifyCode(4);
            //存入redis缓存
            redisTemplate.opsForValue().set("_code",  verifyCode.toLowerCase());
            //生成图片
            int w = 146, h = 33;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            base64 = VerifyCodeUtils.outputImage(w, h, byteArrayOutputStream, verifyCode);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return JsonResultUtil.ok(base64);
    }


    /**
     * 业务系统登录请求校验
     *
     * @param commonLoginDto 请求实体，包含用户名和密码
     * @return
     */
    @PostMapping(value = "/hgp/commonCheck", consumes = MimeTypeUtils.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "登录", notes = "用户登录请求，用户授权服务服务间调用", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult commonCheck(@RequestBody CommonLoginDto commonLoginDto) {
        return loginService.commonCheck(commonLoginDto);
    }

}
