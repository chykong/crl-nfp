package com.cars.nfp.user.controller.user;

import com.cars.util.json.JsonResult;
import com.cars.util.jwt.JwtUtil;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import com.crl.user.user.dto.UpdatePassDto;
import com.crl.user.user.dto.UserDto;
import com.crl.user.user.service.UserService;
import com.crl.user.user.vo.UserSearchVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 用户相关controller
 *
 * @author sunchao
 */
@RestController
@RequestMapping("/user")
@Api(description = "用户：增删改查。", tags = "03.User", basePath = "/user")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 新增用户信息
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增", notes = "新增或修改用户信息，如果用户基本信息不存在则按照用户名进行添加，如果存在则按照用户名修改,routes为返回字段，不需要填写", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult saveUserInfo(@Validated({AddValidGroup.class}) @RequestBody UserDto userDto, HttpServletRequest request) {
        Integer systemId = userDto.getAuthSystemId();
        if(systemId == null){
            String token = request.getHeader("Authorization");
            String username = JwtUtil.parseJWT(token).getOwner();
            UserDto dto = userService.getByUsername(username);
            if(dto != null) {
                userDto.setAuthSystemId(dto.getAuthSystemId());
            }
        }
        return userService.saveUser(userDto);
    }
    /**
     * 修改用户信息/修改个人信息
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改信息", notes = "修改用户信息/修改个人信息", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult update(@Validated({UpdateValidGroup.class}) @RequestBody UserDto userDto, HttpServletRequest request) {
        Integer systemId = userDto.getAuthSystemId();
        if(systemId == null){
            String token = request.getHeader("Authorization");
            String username = JwtUtil.parseJWT(token).getOwner();
            UserDto dto = userService.getByUsername(username);
            if(dto != null) {
                userDto.setAuthSystemId(dto.getAuthSystemId());
            }
        }
        return userService.update(userDto);
    }
    /**
     * 根据用户id获取用户信息
     */
    @PostMapping("/get")
    @ApiOperation(value = "根据id获取信息", notes = "根据id获取用户信息，只需要传id就可以，其余参数可以不传", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult get(@Validated({DeleteValidGroup.class}) @RequestBody UserDto userDto) {
        return userService.get(userDto);
    }

    /**
     * 根据用户名获取用户信息
     */
    @PostMapping("/getByUsername")
    @ApiOperation(value = "根据id获取信息", notes = "根据username获取用户信息，只需要传username就可以，其余参数可以不传", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult getByUsername(@Validated({AddValidGroup.class}) @RequestBody UserDto userDto) {
        return userService.getByUsername(userDto);
    }

    /**
     * 货价运调根据用户id获取用户信息
     */
    @PostMapping("/getHy")
    @ApiOperation(value = "根据id获取信息", notes = "根据id获取用户信息，只需要传id就可以，其余参数可以不传", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult getHy(@Validated({DeleteValidGroup.class}) @RequestBody UserDto userDto) {
        return userService.getHy(userDto);
    }

    /**
     * 根据用户id获取用户权限信息
     */
    @PostMapping("/getPermission")
    @ApiOperation(value = "根据id获取信息", notes = "根据id获取用户信息，只需要传id就可以，其余参数可以不传", response = JsonResult.class, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult getPermission(@Validated({DeleteValidGroup.class}) @RequestBody UserDto userDto) {
        return userService.getPermission(userDto);
    }

    /**
     * 删除用户
     */
    @PostMapping("/del")
    @ApiOperation(value = "删除", notes = "删除用户，传一个id就可以", response = JsonResult.class)
    public JsonResult deleteUserInfo(@RequestBody @Validated({DeleteValidGroup.class}) UserDto userDto) {
        return userService.deleteUser(userDto);
    }

    /**
     * 禁用用户
     */
    @PostMapping("/disable")
    @ApiOperation(value = "禁用", notes = "禁用用户,传一个id就可以", response = JsonResult.class)
    public JsonResult updateStatus(@RequestBody @Validated({DeleteValidGroup.class}) UserDto userDto) {
        return userService.updateStatus(userDto);
    }

    /**
     * 查询用户
     */
    @PostMapping("/search")
    @ApiOperation(value = "查询列表", notes = "查询用户,根据用户名查询,pageIndex从1开始，表示当前第几页", response = JsonResult.class)
    public JsonResult listUserInfo(@RequestBody UserSearchVO userSearchVO, HttpServletRequest request) {
        Integer authId = userSearchVO.getAuthSystemId();
        if(authId == null) {
            String token = request.getHeader("Authorization");
            String username = JwtUtil.parseJWT(token).getOwner();
            UserDto dto = userService.getByUsername(username);
            if(dto != null) {
                userSearchVO.setAuthSystemId(dto.getAuthSystemId());
            }
        }
        return userService.listUser(userSearchVO);
    }

    /**
     * 查询用户
     */
    @PostMapping("/findUser")
    @ApiOperation(value = "查询列表", notes = "查询用户,根据用户名查询,pageIndex从1开始，表示当前第几页", response = JsonResult.class)
    public JsonResult findUser(@RequestBody Map<String, String> requestMap) {
        return userService.findUser(requestMap);
    }

    /**
     * 验证用户名是否存在
     */
    @PostMapping("/checkcode")
    @ApiOperation(value = "验证用户名是否存在", notes = "验证用户名是否存在，不能为空", response = JsonResult.class)
    public JsonResult checkcode(@RequestBody Map<String, String> requestMap) {
        return userService.checkCode(requestMap);
    }

    /**
     * 验证手机号是否存在
     */
    @PostMapping("/checkmobile")
    @ApiOperation(value = "验证手机号是否存在", notes = "验证手机号是否存在，允许为空", response = JsonResult.class)
    public JsonResult checkmobile(@RequestBody Map<String, String> requestMap) {
        return userService.checkMobile(requestMap);
    }
    /**
     * 验证手机号是否存在
     */
    @PostMapping("/checkIdCard")
    @ApiOperation(value = "验证身份证号是否存在", notes = "验证身份证是否存在，允许为空", response = JsonResult.class)
    public JsonResult checkIdCard(@RequestBody Map<String, String> requestMap) {
        return userService.checkIdCard(requestMap);
    }

    /**
     * 修改密码
     */
    @PostMapping("/uppwd")
    @ApiOperation(value = "修改密码", notes = "修改密码", response = JsonResult.class)
    public JsonResult checkmobile(@RequestBody UpdatePassDto updatePassDto) {
        return userService.updatePwd(updatePassDto);
    }

    /**
     * 获取验证码
     */
    @PostMapping("/getValidateCode")
    @ApiOperation(value = "获取验证码", notes = "获取验证码", response = JsonResult.class)
    public JsonResult getValidateCode(@RequestBody Map<String, String> requestMap) {
        return userService.getValidateCode(requestMap);
    }

    /**
     * 密码找回第一步
     */
    @PostMapping("/retrieveOne")
    @ApiOperation(value = "密码找回第一步", notes = "密码找回", response = JsonResult.class)
    public JsonResult retrieveOne(@RequestBody Map<String, String> requestMap) {
        return userService.retrieveOne(requestMap);
    }

    /**
     * 密码找回第二步
     */
    @PostMapping("/retrieveTwo")
    @ApiOperation(value = "密码找回第二步", notes = "密码找回", response = JsonResult.class)
    public JsonResult retrieveTwo(@RequestBody Map<String, String> requestMap) {
        return userService.retrieveTwo(requestMap);
    }

    /**
     * 重置密码
     * @param userDto
     * @return
     */
    @PostMapping("/resetPwd")
    @ApiOperation(value = "重置密码", notes = "重置密码", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult resetPwd(@RequestBody UserDto userDto){
        return userService.resetPwd(userDto.getId());
    }

    /**
     * 审核用户
     * @param userDto
     * @return
     */
    @PostMapping("/reviewUser")
    @ApiOperation(value = "审核用户", notes = "审核用户", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult reviewUser(@RequestBody UserDto userDto){
        return userService.reviewUser(userDto.getId(), userDto.getAuditStatus());
    }

    /**
     * 验证用户名和密码
     * @param requestMap
     * @return
     */
    @PostMapping("/checkUser")
    @ApiOperation(value = "验证用户名密码", notes = "验证用户名密码", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult checkUser(@RequestBody Map<String, String> requestMap){
        return userService.checkUser(requestMap);
    }
}
