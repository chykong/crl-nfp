package com.cars.nfp.user.controller.base;

import com.cars.util.json.JsonResult;
import com.crl.user.base.dto.BaseFreightDto;
import com.crl.user.base.service.BaseFreightService;
import com.crl.user.base.vo.BaseFreightSearchVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: huo yun中心
 * @Date:2019/11/26 16:44
 * @Author:苗凯旋
 */
@RestController
@RequestMapping("/baseFreight")
@Api(description = " huo yun中心：增删改查。", tags = "03.BaseFreight", basePath = "/baseFreight")
public class BaseFreightController {
    @Autowired
    private BaseFreightService baseFreightService;
    /**
     * 查询 huo yun中心
     */
    @PostMapping("/list")
    @ApiOperation(value = "查询列表", notes = "查询 huo yun中心,pageIndex从1开始，表示当前第几页", response = JsonResult.class)
    public JsonResult list(@RequestBody BaseFreightSearchVO baseFreightSearchVO) {
        return baseFreightService.list(baseFreightSearchVO);
    }
    /**
     * 根据路局代码查询 huo yun中心信息
     */
    @PostMapping("/listByLjdm")
    @ApiOperation(value = "根据路局代码查询 huo yun中心信息",  response = JsonResult.class)
    public JsonResult listByLjdm(@RequestBody BaseFreightDto baseFreightDto){
        return baseFreightService.listByLjdm(baseFreightDto);
    }
}
