package com.cars.nfp.user.controller.opinion;

import com.cars.util.bean.BasePage;
import com.cars.util.json.JsonResult;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import com.crl.user.opinion.dto.OpinionDto;
import com.crl.user.opinion.service.OpinionService;
import com.crl.user.opinion.vo.OpinionSearchVO;
import com.crl.user.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 意见反馈相关操作
 */
@RestController
@RequestMapping("/opinion")
@Api(description = "意见反馈：增删改查。", tags = "02.opinion", basePath = "/opinion")
public class OpinionController {
    @Autowired
    private OpinionService opinionService;
    @Autowired
    private UserService userService;

    /**
     * 新增意见反馈信息
     */
    @PostMapping("/save")
    @ApiOperation(value = "新增", notes = "新增意见反馈信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult saveopinion(@Validated({AddValidGroup.class}) @RequestBody OpinionDto opinionDto, HttpServletRequest request) {
        return opinionService.saveOpinion(opinionDto);
    }

    /**
     * 删除意见反馈
     */
    @PostMapping("/del")
    @ApiOperation(value = "删除", notes = "删除意见反馈信息")
    public JsonResult deleteResource(@Validated({DeleteValidGroup.class}) @RequestBody OpinionDto opinionDto) {
        return opinionService.deleteOpinion(opinionDto);
    }

    /**
     * 修改意见信息
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改", notes = "修改意见反馈信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult updateopinion(@Validated({UpdateValidGroup.class}) @RequestBody OpinionDto opinionDto) {
        return opinionService.updateOpinion(opinionDto);
    }

    /**
     * 意见反馈信息
     */
    @PostMapping("/fbOpinion")
    @ApiOperation(value = "修改", notes = "意见反馈信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult fbOpinion(@Validated({UpdateValidGroup.class}) @RequestBody OpinionDto opinionDto){
        return opinionService.fbOpinion(opinionDto);
    }

    /**
     * 获取所有意见反馈信息
     */
    @PostMapping("/listAll")
    @ApiOperation(value = "获取所有", notes = "获取所有意见反馈信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<List<OpinionDto>> listAll() {
        return opinionService.listAll();
    }

    /**
     * 根据id获取意见反馈对象，用于修改
     */
    @PostMapping("/get")
    @ApiOperation(value = "根据id获取意见反馈对象", notes = "根据id获取意见反馈对象，用于修改意见反馈")
    public JsonResult<OpinionDto> getById(@Validated({DeleteValidGroup.class}) @RequestBody OpinionDto opinionDto) {
        return opinionService.getById(opinionDto);
    }

    /**
     * 根据条件查询意见反馈
     */
    @PostMapping("/search")
    @ApiOperation(value = "根据条件查询意见反馈", notes = "根据条件查询意见反馈信息", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<BasePage<List<OpinionDto>>> search(@Validated @RequestBody OpinionSearchVO opinionSearchVO, HttpServletRequest request) {
        return opinionService.listSearch(opinionSearchVO);
    }
}
