package com.cars.nfp.user.controller.system;

import com.cars.util.bean.BasePage;
import com.cars.util.json.JsonResult;
import com.crl.user.system.dto.SystemBindDto;
import com.crl.user.system.model.SystemBind;
import com.crl.user.system.service.SystemBindService;
import com.crl.user.system.vo.SystemBindSearchVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @Description:我的绑定列表
 * @Date:2020/4/16 10:58
 * @Author:苗凯旋
 */
@RestController
@RequestMapping("/systemMybind")
public class SystemMyBindController {
    @Autowired
    private SystemBindService systemBindService;

    /**
     * 根据条件查询我的绑定的系统列表
     */
    @PostMapping("/search")
    @ApiOperation(value = "查询", notes = "将系统id作为查询条件，并默认传入用户id，进行该用户绑定的信息查询功能", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<BasePage<List<SystemBindDto>>> listSearch(@Valid @RequestBody SystemBindSearchVO systemBindSearchVO) {
        return systemBindService.myListSearch(systemBindSearchVO);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改", notes = "修改业务系统，传个modifiedBy，at不需要传", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult update(@RequestBody SystemBindDto systemBindDto) {
        return systemBindService.updateMyBind(systemBindDto);
    }

    /**
     * 根据id查询
     */
    @PostMapping("/get")
    @ApiOperation(value = "根据id查询", notes = "根据id查询业务系统,只传一个id就可以", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public JsonResult<SystemBind> get(@RequestBody  SystemBindDto systemBindDto) {
        return systemBindService.getMyBindById(systemBindDto);
    }

    /**
     * 排序号上下移动
     * @param systemBindDto
     * @return
     */
    @PostMapping("/movePxh")
    public JsonResult movePxh(@RequestBody SystemBindDto systemBindDto) {
        return systemBindService.movePxh(systemBindDto);
    }
}
