package com.cars.nfp.user.controller.system;


import com.cars.util.json.JsonResult;
import com.crl.user.system.model.HgpLogin;
import com.crl.user.system.service.BindUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description:绑定用户接口
 * @Date:2020/5/8 16:34
 * @Author:苗凯旋
 */
@RestController
@RequestMapping("/hgp")
public class BindUserController {
    @Autowired
    private BindUserService bindUserService;


    /**
     * 绑定
     * @param request
     * @param hgpLogin
     * @return
     *//*
    @PostMapping("/commonBind")
    public JsonResult commonBind(HttpServletRequest request, @Validated({AddValidGroup.class}) @RequestBody HgpLogin hgpLogin){
        return bindUserService.commonBind(request,hgpLogin);
    }

    *//**
     * 解绑
     * @param request
     * @param hgpLogin
     * @return
     *//*
    @PostMapping("/commonUnBind")
    public JsonResult commonUnBind(HttpServletRequest request, @Validated({AddValidGroup.class}) @RequestBody HgpLogin hgpLogin){
        return bindUserService.commonUnBind(request,hgpLogin);
    }*/

    /**
     * 请求登陆
     * @param hgpLogin
     * @param request
     * @return
     */
    @PostMapping(value = "/commonLogin")
    public JsonResult commonLogin(@RequestBody HgpLogin hgpLogin, HttpServletRequest request){
        return bindUserService.commonLogin(request,hgpLogin);
    }
}
