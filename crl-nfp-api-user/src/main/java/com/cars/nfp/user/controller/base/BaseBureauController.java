package com.cars.nfp.user.controller.base;

import com.cars.util.json.JsonResult;
import com.crl.user.base.service.BaseBureauService;
import com.crl.user.base.vo.BaseBureauSearchVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:路局
 * @Date:2019/11/26 16:44
 * @Author:苗凯旋
 */
@RestController
@RequestMapping("/baseBureau")
@Api(description = "路局：增删改查。", tags = "03.BaseBureau", basePath = "/baseBureau")
public class BaseBureauController {
    @Autowired
    private BaseBureauService baseBureauService;
    /**
     * 查询路局
     */
    @PostMapping("/list")
    @ApiOperation(value = "查询列表", notes = "查询路局,pageIndex从1开始，表示当前第几页", response = JsonResult.class)
    public JsonResult list(@RequestBody BaseBureauSearchVO baseBureauSearchVO) {
        return baseBureauService.list(baseBureauSearchVO);
    }
    @PostMapping("/listAll")
    @ApiOperation(value = "查询全部",  response = JsonResult.class)
    public JsonResult listAll(){
        return baseBureauService.listAll();
    }
}
