package com.crl.portal.model;

import lombok.Data;

/**
 * @author linan
 * @date 2020-06-12 10:45
 */
@Data
public class DicCz {
    /**
     * 电报码
     */
    private String dbm;
    /**
     * 车站名称
     */
    private String czmc;
    /**
     * 路局代码
     */
    private String ljdm;
    /**
     * 路局名称
     */
    private String ljmc;
}
