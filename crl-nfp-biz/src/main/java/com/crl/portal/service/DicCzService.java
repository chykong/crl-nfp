package com.crl.portal.service;

import com.crl.portal.dao.DicCzDao;
import com.crl.portal.model.DicCz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author linan
 * @date 2020-06-12 10:42
 * @description 车站字典服务
 */
@Service
public class DicCzService {

    @Autowired
    private DicCzDao dicCzDao;

    /**
     * 获取车站列表
     */
    public List<DicCz> list() {
        return dicCzDao.list();
    }

    public String testException1() {
        throw new RuntimeException("测试运行时错误");
    }
}
