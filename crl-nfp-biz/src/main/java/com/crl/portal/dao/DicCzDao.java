package com.crl.portal.dao;

import com.crl.dao.BaseDao;
import com.crl.portal.model.DicCz;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author linan
 * @date 2020-06-12 10:53
 */
@Repository
public class DicCzDao extends BaseDao<DicCz,DicCz>{

    public List<DicCz> list() {
        String sql = "SELECT * FROM CRL_DZ_CZ ";
        return list(sql);
    }
}
