package com.crl.portal.vo;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author linan
 * @date 2020-06-12 16:38
 */
@Data
public class DicCzVO {

    @NotNull
    private String czmc;

    @NotEmpty
    private String dbm;
}
