package com.crl.user.base.dto;


import com.cars.util.bean.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @Description:车站数据交互对象
 * @Date:2019/11/26 16:09
 * @Author:苗凯旋
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel("车站数据交互对象")
public class BaseStationDto extends BaseModel {
    /**
     * 车站代码
     */
    @ApiModelProperty("车站代码")
    private String czdm;
    /**
     * 车站拼音码
     */
    @ApiModelProperty("车站拼音码")
    private String czpym;
    /**
     * 车站名称
     */
    @ApiModelProperty("车站名称")
    private String czmc;
    /**
     * 路局代码
     */
    @ApiModelProperty("路局代码")
    private String ljdm;
    /**
     * 路局拼音码
     */
    @ApiModelProperty("路局拼音码")
    private String ljpym;
    /**
     * tmism
     */
    @ApiModelProperty("tmism")
    private String cztmism;
    /**
     *  huo yun中心代码
     */
    @ApiModelProperty(" huo yun中心代码")
    private String hyzxdm;
}
