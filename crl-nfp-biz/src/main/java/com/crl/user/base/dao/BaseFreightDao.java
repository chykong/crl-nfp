package com.crl.user.base.dao;


import com.crl.user.base.model.BaseFreight;
import com.crl.user.base.vo.BaseFreightSearchVO;
import com.cars.util.page.PageUtil;
import com.crl.dao.BaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Description: huo yun中心
 * @Date:2019/11/26 16:24
 * @Author:苗凯旋
 */
@Repository
public class BaseFreightDao extends BaseDao<BaseFreight, BaseFreightSearchVO> {
    /**
     * 根据条件查询
     */
    public List<BaseFreight> list(BaseFreightSearchVO baseFreightSearchVO) {
        String sql = "select ljdm,ljpym,hyzxdm,hyzxmc from crl_base_freight where 1=1 ";
        sql += createSQL(baseFreightSearchVO);
        sql += " ORDER BY created_at DESC";
        sql = PageUtil.createOraclePageSQL(sql, baseFreightSearchVO.getPageIndex(), baseFreightSearchVO.getPageSize());
        return list(sql, baseFreightSearchVO);
    }

    /**
     * 根据条件统计数量
     */
    public int count(BaseFreightSearchVO baseFreightSearchVO) {
        String sql = "SELECT COUNT(*) FROM crl_base_freight  WHERE 1=1 ";
        sql += createSQL(baseFreightSearchVO);
        return count(sql, baseFreightSearchVO);
    }

    /**
     * 拼接查询条件
     */
    public String createSQL(BaseFreightSearchVO baseFreightSearchVO) {
        StringBuffer sql = new StringBuffer(50);

        if (baseFreightSearchVO.getLjdm() != null) {
            sql.append(" AND ljdm =:ljdm ");
        }

        if (baseFreightSearchVO.getHyzxmc() != null) {
            baseFreightSearchVO.setHyzxmc("%"+baseFreightSearchVO.getHyzxmc()+"%");
            sql.append(" AND hyzxmc like :hyzxmc ");
        }
        return sql.toString();
    }

    /**
     * 根据路局代码查询 huo yun中心信息
     * @param ljdm
     * @return
     */
    public List<BaseFreight> listByLjdm(String ljdm) {
        String sql = "select ljdm,ljpym,hyzxdm,hyzxmc from crl_base_freight where ljdm=?";
        return list(sql,ljdm);
    }
}
