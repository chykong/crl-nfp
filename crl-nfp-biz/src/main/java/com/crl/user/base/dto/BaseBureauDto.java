package com.crl.user.base.dto;

import com.cars.util.bean.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @Description:路局数据交互对象
 * @Date:2019/11/26 15:55
 * @Author:苗凯旋
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel("路局数据交互对象")
public class BaseBureauDto extends BaseModel {
    /**
     * 路局代码
     */
    @ApiModelProperty("路局代码")
    private String ljdm;
    /**
     * 路局拼音码
     */
    @ApiModelProperty("路局拼音码")
    private String ljpym;
    /**
     * 简称
     */
    @ApiModelProperty("简称")
    private String ljjc;
    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer ljpx;
    /**
     * 全称
     */
    @ApiModelProperty("全称")
    private String ljqc;
}
