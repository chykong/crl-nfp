package com.crl.user.base.service;

import com.cars.util.bean.BasePage;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.list.ListUtils;
import com.crl.user.base.dao.BaseStationDao;
import com.crl.user.base.dto.BaseStationDto;
import com.crl.user.base.model.BaseStation;
import com.crl.user.base.vo.BaseStationSearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description:车站
 * @Date:2019/11/26 16:34
 * @Author:苗凯旋
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class BaseStationService {
    @Autowired
    private BaseStationDao baseStationDao;
    /**
     * 根据条件查询路局
     */
    public JsonResult list(BaseStationSearchVO baseStationSearchVO) {
        BasePage<List<BaseStationDto>> basePage = new BasePage();
        int count = baseStationDao.count(baseStationSearchVO);
        basePage.setTotal(count);
        List<BaseStation> list = baseStationDao.list(baseStationSearchVO);
        List<BaseStationDto> listResult = ListUtils.copyList(list, BaseStationDto.class);
        basePage.setData(listResult);
        return JsonResultUtil.ok(basePage);
    }

    /**
     * 根据 huo yun中心代码查询车站信息
     * @param baseStationDto
     * @return
     */
    public JsonResult listByHyzxdm(BaseStationDto baseStationDto) {
        List<BaseStation> list = baseStationDao.listByHyzxdm(baseStationDto.getHyzxdm());
        return JsonResultUtil.ok(list);
    }
}
