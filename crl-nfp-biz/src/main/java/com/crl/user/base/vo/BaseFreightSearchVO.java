package com.crl.user.base.vo;

import com.cars.util.bean.BasePageSearchVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: huo yun中心查询条件接收实体
 * @Date:2019/11/26 16:03
 * @Author:苗凯旋
 */
@Data
@ApiModel(" huo yun中心查询条件接收实体")
public class BaseFreightSearchVO extends BasePageSearchVO {
    /**
     * 路局代码
     */
    @ApiModelProperty("路局代码")
    private String ljdm;
    /**
     *  huo yun中心名称
     */
    @ApiModelProperty(" huo yun中心名称")
    private String hyzxmc;

}
