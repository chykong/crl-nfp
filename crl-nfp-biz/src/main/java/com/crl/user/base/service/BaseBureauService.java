package com.crl.user.base.service;

import com.cars.util.bean.BasePage;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.list.ListUtils;
import com.crl.user.base.dao.BaseBureauDao;
import com.crl.user.base.dto.BaseBureauDto;
import com.crl.user.base.model.BaseBureau;
import com.crl.user.base.vo.BaseBureauSearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description:路局
 * @Date:2019/11/26 16:34
 * @Author:苗凯旋
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class BaseBureauService {
    @Autowired
    private BaseBureauDao baseBureauDao;
    /**
     * 根据条件查询路局
     */
    public JsonResult list(BaseBureauSearchVO baseBureauSearchVO) {
        BasePage<List<BaseBureauDto>> basePage = new BasePage();
        int count = baseBureauDao.count(baseBureauSearchVO);
        basePage.setTotal(count);
        List<BaseBureau> list = baseBureauDao.list(baseBureauSearchVO);
        List<BaseBureauDto> listResult = ListUtils.copyList(list, BaseBureauDto.class);
        basePage.setData(listResult);
        return JsonResultUtil.ok(basePage);
    }

    /**
     * 查询全部的路局信息
     * @return
     */
    public JsonResult listAll() {
        List<BaseBureau> list = baseBureauDao.listAll();
        return JsonResultUtil.ok(list);
    }
}
