package com.crl.user.base.vo;

import com.cars.util.bean.BasePageSearchVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:车站查询条件接收实体
 * @Date:2019/11/26 16:09
 * @Author:苗凯旋
 */
@Data
@ApiModel("车站查询条件接收实体")
public class BaseStationSearchVO extends BasePageSearchVO {

    /**
     * 车站名称
     */
    @ApiModelProperty("车站名称")
    private String czmc;
    /**
     * 路局代码
     */
    @ApiModelProperty("路局代码")
    private String ljdm;

}
