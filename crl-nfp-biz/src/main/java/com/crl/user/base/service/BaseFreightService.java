package com.crl.user.base.service;

import com.cars.util.bean.BasePage;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.list.ListUtils;
import com.crl.user.base.dao.BaseFreightDao;
import com.crl.user.base.dto.BaseFreightDto;
import com.crl.user.base.model.BaseFreight;
import com.crl.user.base.vo.BaseFreightSearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: huo yun中心
 * @Date:2019/11/26 16:34
 * @Author:苗凯旋
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class BaseFreightService {
    @Autowired
    private BaseFreightDao baseFreightDao;
    /**
     * 根据条件查询路局
     */
    public JsonResult list(BaseFreightSearchVO baseFreightSearchVO) {
        BasePage<List<BaseFreightDto>> basePage = new BasePage();
        int count = baseFreightDao.count(baseFreightSearchVO);
        basePage.setTotal(count);
        List<BaseFreight> list = baseFreightDao.list(baseFreightSearchVO);
        List<BaseFreightDto> listResult = ListUtils.copyList(list, BaseFreightDto.class);
        basePage.setData(listResult);
        return JsonResultUtil.ok(basePage);
    }

    /**
     * 根据路局代码查询 huo yun中心信息
     * @param baseFreightDto
     * @return
     */
    public JsonResult listByLjdm(BaseFreightDto baseFreightDto) {
        List<BaseFreight> list = baseFreightDao.listByLjdm(baseFreightDto.getLjdm());
        return JsonResultUtil.ok(list);
    }
}
