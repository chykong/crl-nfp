package com.crl.user.base.model;


import com.cars.util.bean.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @Description:路局实体类
 * @Date:2019/11/26 15:55
 * @Author:苗凯旋
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class BaseBureau extends BaseModel {
    /**
     * 路局代码
     */
    private String ljdm;
    /**
     * 路局拼音码
     */
    private String ljpym;
    /**
     * 简称
     */
    private String ljjc;
    /**
     * 排序
     */
    private Integer ljpx;
    /**
     * 全称
     */
    private String ljqc;
}
