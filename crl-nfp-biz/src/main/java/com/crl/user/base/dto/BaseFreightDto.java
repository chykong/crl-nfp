package com.crl.user.base.dto;


import com.cars.util.bean.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @Description: huo yun中心数据交互对象
 * @Date:2019/11/26 16:03
 * @Author:苗凯旋
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(" huo yun中心数据交互对象")
public class BaseFreightDto extends BaseModel {
    /**
     * 路局代码
     */
    @ApiModelProperty("代码")
    private String ljdm;
    /**
     * 路局拼音码
     */
    @ApiModelProperty("拼音码")
    private String ljpym;
    /**
     *  huo yun中心代码
     */
    @ApiModelProperty(" huo yun中心代码")
    private String hyzxdm;
    /**
     *  huo yun中心名称
     */
    @ApiModelProperty(" huo yun中心名称")
    private String hyzxmc;
}
