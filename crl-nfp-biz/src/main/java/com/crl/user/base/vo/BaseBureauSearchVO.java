package com.crl.user.base.vo;

import com.cars.util.bean.BasePageSearchVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:路局查询条件接收实体
 * @Date:2019/11/26 15:55
 * @Author:苗凯旋
 */
@Data
@ApiModel("路局查询条件接收实体")
public class BaseBureauSearchVO extends BasePageSearchVO {
    /**
     * 路局代码
     */
    @ApiModelProperty("路局代码")
    private String ljdm;

    /**
     * 全称
     */
    @ApiModelProperty("全称")
    private String ljqc;

    /**
     * 简称
     */
    @ApiModelProperty("简称")
    private String ljjc;

}
