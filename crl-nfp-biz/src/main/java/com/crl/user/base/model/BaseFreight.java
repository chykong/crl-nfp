package com.crl.user.base.model;


import com.cars.util.bean.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @Description: huo yun中心实体类
 * @Date:2019/11/26 16:03
 * @Author:苗凯旋
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class BaseFreight extends BaseModel {
    /**
     * 路局代码
     */
    private String ljdm;
    /**
     * 路局拼音码
     */
    private String ljpym;
    /**
     *  huo yun中心代码
     */
    private String hyzxdm;
    /**
     *  huo yun中心名称
     */
    private String hyzxmc;
}
