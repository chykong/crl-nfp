package com.crl.user.base.dao;


import com.crl.user.base.model.BaseBureau;
import com.crl.user.base.vo.BaseBureauSearchVO;
import com.cars.util.page.PageUtil;
import com.crl.dao.BaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Description:
 * @Date:2019/11/26 16:16
 * @Author:苗凯旋
 */
@Repository
public class BaseBureauDao extends BaseDao<BaseBureau, BaseBureauSearchVO> {
    /**
     * 根据条件查询
     */
    public List<BaseBureau> list(BaseBureauSearchVO baseBureauSearchVO) {
        String sql = "select ljdm,ljpym,ljjc,ljpx,ljqc from crl_base_bureau where 1=1 ";
        sql += createSQL(baseBureauSearchVO);
        sql += " ORDER BY created_at DESC";
        sql = PageUtil.createOraclePageSQL(sql, baseBureauSearchVO.getPageIndex(), baseBureauSearchVO.getPageSize());
        return list(sql, baseBureauSearchVO);
    }

    /**
     * 根据条件统计数量
     */
    public int count(BaseBureauSearchVO baseBureauSearchVO) {
        String sql = "SELECT COUNT(*) FROM crl_base_bureau  WHERE 1=1 ";
        sql += createSQL(baseBureauSearchVO);
        return count(sql, baseBureauSearchVO);
    }

    /**
     * 拼接查询条件
     */
    public String createSQL(BaseBureauSearchVO baseBureauSearchVO) {
        StringBuffer sql = new StringBuffer(50);

        if (baseBureauSearchVO.getLjdm() != null) {
            sql.append(" AND ljdm =:ljdm ");
        }
        if (baseBureauSearchVO.getLjjc() != null) {
            sql.append(" AND ljjc =:ljjc ");
        }
        if (baseBureauSearchVO.getLjqc() != null) {
            baseBureauSearchVO.setLjqc("%"+baseBureauSearchVO.getLjqc()+"%");
            sql.append(" AND ljqc like :ljqc ");
        }
        return sql.toString();
    }

    /**
     * 查询全部
     * @return
     */
    public List<BaseBureau> listAll() {
        String sql = "select ljdm,ljpym,ljjc,ljpx,ljqc from crl_base_bureau order by ljpx asc";
        return list(sql);
    }
}
