package com.crl.user.base.model;

import com.cars.util.bean.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @Description:车站实体类
 * @Date:2019/11/26 16:09
 * @Author:苗凯旋
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class BaseStation extends BaseModel {
    /**
     * 车站代码
     */
    private String czdm;
    /**
     * 车站拼音码
     */
    private String czpym;
    /**
     * 车站名称
     */
    private String czmc;
    /**
     * 路局代码
     */
    private String ljdm;
    /**
     * 路局拼音码
     */
    private String ljpym;
    /**
     * tmism
     */
    private String cztmism;
    /**
     *  huo yun中心代码
     */
    private String hyzxdm;
}
