package com.crl.user.base.dao;


import com.crl.user.base.model.BaseStation;
import com.crl.user.base.vo.BaseStationSearchVO;
import com.cars.util.page.PageUtil;
import com.crl.dao.BaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Description:车站
 * @Date:2019/11/26 16:30
 * @Author:苗凯旋
 */
@Repository
public class BaseStationDao extends BaseDao<BaseStation, BaseStationSearchVO> {
    /**
     * 根据条件查询
     */
    public List<BaseStation> list(BaseStationSearchVO baseStationSearchVO) {
        String sql = "select czdm,czpym,czmc,ljdm,ljpym,cztmism,hyzxdm from crl_base_station where 1=1 ";
        sql += createSQL(baseStationSearchVO);
        sql += " ORDER BY created_at DESC";
        sql = PageUtil.createOraclePageSQL(sql, baseStationSearchVO.getPageIndex(), baseStationSearchVO.getPageSize());
        return list(sql, baseStationSearchVO);
    }

    /**
     * 根据条件统计数量
     */
    public int count(BaseStationSearchVO baseStationSearchVO) {
        String sql = "SELECT COUNT(*) FROM crl_base_station  WHERE 1=1 ";
        sql += createSQL(baseStationSearchVO);
        return count(sql, baseStationSearchVO);
    }

    /**
     * 拼接查询条件
     */
    public String createSQL(BaseStationSearchVO baseStationSearchVO) {
        StringBuffer sql = new StringBuffer(50);

        if (baseStationSearchVO.getLjdm() != null) {
            sql.append(" AND ljdm =:ljdm ");
        }

        if (baseStationSearchVO.getCzmc() != null) {
            baseStationSearchVO.setCzmc("%"+baseStationSearchVO.getCzmc()+"%");
            sql.append(" AND czmc like :czmc ");
        }
        return sql.toString();
    }

    /**
     * 根据 huo yun中心代码查询
     * @param hyzxdm
     * @return
     */
    public List<BaseStation> listByHyzxdm(String hyzxdm) {
        String sql = "select czdm,czpym,czmc,ljdm,ljpym,cztmism,hyzxdm from crl_base_station where hyzxdm=? ";
        return list(sql,hyzxdm);
    }
}
