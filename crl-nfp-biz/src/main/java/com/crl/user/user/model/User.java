package com.crl.user.user.model;

import com.cars.util.bean.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 用户信息表HGP_BASE_USER
 *
 * @author sunchao
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class User extends BaseModel {
    /**
     * 用户id
     */
    private Integer id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 座机号
     */
    private String telephone;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 身份证号
     */
    private String idcard;
    /**
     * 部门id
     */
    private Integer departmentId;
    /**
     * 部门名称
     */
    private String departmentName;
    /**
     * 工作证号
     */
    private String workcard;
    /**
     * 所在岗位
     */
    private String position;
    /**
     * 备注
     */
    private String remark;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 审核状态
     */
    private Integer auditStatus;
    /**
     * 路局
     */
    private String bureauCode;
    /**
     *  huo yun中心
     */
    private String freightCode;
    /**
     * 车站
     */
    private String stationCode;
    /**
     * 是否为部门负责人，部门负责人回显用
     */
    private String flag;
    /**
     * 所属授权系统
     */
    private Integer authSystemId;
}

