package com.crl.user.user.vo;

import com.cars.util.bean.BasePageSearchVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @Desc:
 * @author: miaokaixuan
 * @Date: 2020/2/20
 */
@Data
@ApiModel("系统信息实体类")
public class AuthSystemVO extends BasePageSearchVO {

    /**
     *系统名称
     */
    private String systemName;
    /**
     *系统负责人
     */
    private String principal;

    private Integer userId;

}
