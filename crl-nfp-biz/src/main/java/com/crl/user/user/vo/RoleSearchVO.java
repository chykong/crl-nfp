package com.crl.user.user.vo;


import com.cars.util.bean.BasePageSearchVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 角色模块查询交互对象
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "角色模块查询交互对象")
public class RoleSearchVO extends BasePageSearchVO {
    /**
     * 角色名称
     */
    @ApiModelProperty("角色名称")
    private String roleName;
    @ApiModelProperty("角色类别")
    private String roleType;
    @ApiModelProperty("用户id")
    private Integer userId;
    /**
     * 所属授权系统
     */
    private Integer authSystemId;
}
