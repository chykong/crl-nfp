package com.crl.user.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("登录信息传输对象")
public class CommonLoginDto {
    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    @Length(max = 30, min = 5)
    private String hgpuser;
    /**
     * 密码
     */
    @ApiModelProperty("密码")
    @Length(min=1)
    private String code;
    /**
     * 加密盐
     */
    @ApiModelProperty("加密盐")
    @Length(min=6, max = 6)
    private String random;
    /**
     * 用户唯一标识
     */
    @ApiModelProperty("用户唯一标识")
    @Length(min=32, max = 32)
    private String id;
}
