package com.crl.user.user.dto;

import com.cars.util.bean.BaseModel;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * 用户新增修改交互对象
 *
 * @author sunchao
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "用户模块交互对象")
public class UserDto extends BaseModel {
    /**
     * 用户id
     */
    @ApiModelProperty("id")
    @NotNull(groups = {UpdateValidGroup.class, DeleteValidGroup.class}, message = "用户id不能为空")
    private Integer id;
    /**
     * 用户名
     */
    @NotNull(groups = {AddValidGroup.class, UpdateValidGroup.class}, message = "用户名不能为空")
    @ApiModelProperty("用户名")
    private String username;
    /**
     * 真实姓名
     */
    @ApiModelProperty("真实姓名")
    private String realName;
    /**
     * 座机号
     */
    @ApiModelProperty("座机号")
    private String telephone;
    /**
     * 手机号
     */
    @ApiModelProperty("手机号")
    private String mobile;
    /**
     * 身份证号
     */
    @ApiModelProperty("身份证号")
    private String idcard;
    /**
     * 部门id
     */
    @ApiModelProperty("部门id")
    private Integer departmentId;
    /**
     * 部门名称
     */
    @ApiModelProperty("部门名称")
    private String departmentName;
    /**
     * 工作证号
     */
    @ApiModelProperty("工作证号")
    private String workcard;
    /**
     * 所在岗位
     */
    @ApiModelProperty("所在岗位")
    private String position;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;
    /**
     * 角色id拼接后字符串
     */
    @ApiModelProperty("角色id，多个使用@分隔")
    private String roleIds;
    /**
     * 路由表
     */
    @ApiModelProperty("路由表")
    private List<RouteDto> routes;
    /**
     * 用户状态
     */
    @ApiModelProperty("用户状态")
    private Integer status;
    /**
     * 路局
     */
    @ApiModelProperty("路局")
    private String bureauCode;

    /**
     * huo yun中心
     */
    @ApiModelProperty(" huo yun中心")
    private String freightCode;

    /**
     * 车站
     */
    @ApiModelProperty("车站")
    private String stationCode;
    /**
     * 是否为部门负责人，部门负责人回显用
     */
    private String flag;
    /**
     * 密码修改时间
     */
    private Date changePasswordTime;

    private String oldMobile;

    private String code;

    /**
     * 审核状态
     */
    private Integer auditStatus;
    /**
     * 所属授权系统
     */
    private Integer authSystemId;

    private List<String> resourceCodes;
}
