package com.crl.user.user.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 路由信息
 */
@Data
@ApiModel("路由信息")
public class RouteDto {
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Integer id;
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Integer parentId;
    /**
     * 菜单名字
     */
    @ApiModelProperty("菜单名字")
    private String name;
    /**
     * 请求路径
     */
    @ApiModelProperty("请求路径")
    private String path;
    /**
     * 组件
     */
    @ApiModelProperty("组件")
    private String component;
    /**
     * 重定向地址
     */
    @ApiModelProperty("重定向地址")
    private String redirect;
    /**
     * 是否隐藏true隐藏，false不隐藏
     */
    @ApiModelProperty("是否隐藏true隐藏，false不隐藏")
    private Boolean hidden;
    /**
     * 元数据信息
     */
    @ApiModelProperty("元数据信息")
    private MetaDto meta;
    /**
     * 标题
     */
    private String title;
    /**
     * 地址
     */
    private String metaPath;
    /**
     * 图标
     */
    private String icon;
    /**
     * 缓存
     */
    private Boolean keepAlive;
    /**
     * 权限
     */
    private String permission;

    private Boolean alwaysShow;
    /**
     * 子路由
     */
    @ApiModelProperty("子路由")
    private List<RouteDto> children;
}
