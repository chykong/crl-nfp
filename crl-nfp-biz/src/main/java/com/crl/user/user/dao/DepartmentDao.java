package com.crl.user.user.dao;



import com.crl.user.user.model.Department;
import com.crl.dao.BaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 部门
 */
@Repository
public class DepartmentDao extends BaseDao<Department, Department> {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    /**
     * 新增
     */
    public int saveDepartment(Department department) {
        String sql = "INSERT INTO crl_base_department(id,dep_code,dep_name,parent_id,user_id,bureau_code,created_by,created_at,disc,dep_short,dep_order) " +
                "VALUES(seq_crl_base_department.nextval,:depCode,:depName,:parentId,:userId,:bureauCode,:createdBy,sysdate,:disc,:depShort,:depOrder)";
        return insert(sql, department);
    }

    /**
     * 删除
     */
    public int deleteDepartment(Integer departmentId) {
        String sql = "DELETE FROM crl_base_department WHERE id=?";
        return delete(sql, departmentId);
    }

    /**
     * 根据id获取
     */
    public Department getById(Integer departmentId) {
        String sql = "SELECT id,dep_code,dep_code as oldDepCode,dep_name,parent_id,user_id,disc,bureau_code,created_by,created_at,modified_by,modified_at,(SELECT real_name FROM crl_base_user WHERE id=t.user_id) realName,dep_short,dep_order FROM crl_base_department t WHERE id=?";
        return get(sql, departmentId);
    }

    /**
     * 根据代码获取
     */
    public Department getByCode(String departmentCode) {
        String sql = "SELECT id,dep_code,dep_name,parent_id,user_id,bureau_code,created_by,created_at,modified_by,modified_at,(SELECT real_name FROM crl_base_user WHERE id=t.user_id) realName,dep_short,dep_order FROM crl_base_department t WHERE dep_code=?";
        return get(sql, departmentCode);
    }

    /**
     * 根据名称和父级id
     */
    public Department getByName(Integer parentId, String deptName) {
        String sql = "SELECT id,dep_code,dep_name,parent_id,user_id,bureau_code,created_by,created_at,modified_by,modified_at FROM crl_base_department t WHERE parent_id=? and dep_name = ?";
        return get(sql, parentId, deptName);
    }

    /**
     * 根据父节点id获取
     */
    public List<Department> listByParentId(Integer departmentParentId) {
        String sql = "SELECT id,dep_code,dep_name,parent_id,user_id,bureau_code,created_by,created_at,modified_by,modified_at,(SELECT real_name FROM crl_base_user WHERE id=t.user_id) realName" +
                ",case when (select count(id) from crl_base_department WHERE parent_id = T . ID)> 0 then 0 else 1 end isLeaf,dep_order FROM crl_base_department t WHERE parent_id=? order by dep_order asc";
        return list(sql, departmentParentId);
    }

    /**
     * 获取所有
     * @param de
     */
    public List<Department> listAll(Department de) {
        String sql = "SELECT id,dep_code,dep_name,parent_id,user_id,bureau_code,created_by,created_at,modified_by,modified_at,(SELECT real_name FROM crl_base_user WHERE id=t.user_id) realName,dep_short,dep_order "
                +" FROM crl_base_department t where 1=1 order by dep_code asc";
        return list(sql,de);
    }

    /**
     * 修改
     */
    public int updateById(Department department) {
        String sql = "UPDATE crl_base_department SET " +
                "dep_code=:depCode," +
                "dep_name=:depName," +
                "parent_id=:parentId," +
                "user_id=:userId," +
                "bureau_code=:bureauCode,"+
                "modified_by=:modifiedBy," +
                "disc=:disc,"+
                "dep_short=:depShort,"+
                "dep_order=:depOrder,"+
                "modified_at=sysdate" +
                " WHERE id=:id";
        return update(sql, department);
    }

    /**
     * 修改负责人
     */
    public int updateUseridById(Department department) {
        String sql = "UPDATE crl_base_department SET " +
                "user_id=:userId," +
                "modified_by=:modifiedBy," +
                "modified_at=sysdate" +
                " WHERE id=:id";
        return update(sql, department);
    }

    /**
     * 查询用户负责的所有部门
     * @param department
     * @return
     */
    public List<Department> listByUserId(Department department) {
        String sql = "SELECT id,dep_code,dep_name,parent_id,user_id,bureau_code,created_by,created_at,modified_by,modified_at,(SELECT real_name FROM crl_base_user WHERE id=t.user_id) realName" +
                ",dep_order FROM crl_base_department t where t.user_id = :userId ";
        sql += " order by dep_code asc";
        return list(sql, department);
    }

    /**
     * 查询用户负责的所有部门以及子部门
     * @param department
     * @return
     */
    public List<Department> listAllByUserId(Department department) {
        String sql = "SELECT id,dep_code,dep_name,parent_id,(select listagg(user_id) within GROUP(ORDER BY DEPARTMENT_ID) from crl_base_DEP_USER du where DU.DEPARTMENT_ID = t.id) user_id"+
                ",bureau_code,created_by,created_at,modified_by,modified_at,(SELECT listagg(u.REAL_NAME,',') within GROUP(ORDER BY UD.DEPARTMENT_ID) FROM crl_base_DEP_USER ud "+
                "LEFT JOIN crl_base_user U  ON ud.user_id = U . ID where UD.DEPARTMENT_ID = t.id) realName," +
                "case when (select count(id) from crl_base_department WHERE parent_id = T . ID)> 0 then 0 else 1 end isLeaf, dep_order FROM crl_base_department t START WITH 1=1";
        if(null != department.getUserId()){
            sql += " and t.user_id = :userId ";
        }
        sql += " CONNECT BY PRIOR  ID = parent_id";
        sql += " order by dep_code asc";
        return list(sql, department);
    }

    /**
     * 查询上级用户的负责所有部门
     * @param userId
     * @return
     */
    public List<Department> listByUserIdAndDeptId(Integer userId, Integer deptId) {
        String sql = "SELECT id,dep_code,dep_name,parent_id,user_id,bureau_code,created_by,created_at,modified_by,modified_at FROM crl_base_department where user_id = ? start WITH id=? CONNECT by PRIOR parent_id = id";
        return list(sql, userId, deptId);
    }

    /**
     * 根据ROOT节点查所有子节点
     * @param id
     * @return
     */
    public List<Department> prior(Integer id) {
        String sql = "select id,dep_code,dep_name,parent_id,user_id,bureau_code,created_by,created_at,modified_by,modified_at,(SELECT real_name FROM crl_base_user WHERE id=t.user_id) realName,dep_short,dep_order from crl_base_department t start with t.id = ? connect by t.parent_id= prior t.id ";
        sql += " order by dep_order asc";
        return list(sql, id);
    }

    /**
     * 根据子节点查所有直系父节点直到最上级
     * @param id
     * @return
     */
    public List<Department> reversalPrior(Integer id) {
        String sql = "select id,dep_code,dep_name,parent_id,user_id,bureau_code,created_by,created_at,modified_by,modified_at,(SELECT real_name FROM crl_base_user WHERE id=t.user_id) realName from crl_base_department t start with t.id = ? connect by t.id= prior t.parent_id";
        return list(sql, id);
    }

    /**
     * 统计下级部门最大的depcode
     * @param id
     * @return
     */
    public String maxDepCode(Integer id){
        String sql = "select nvl(max(dep_code),'0') from crl_base_department where parent_id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, String.class);
    }

    /**
     * 根据原编码修改子机构所有的编码
     * @param department
     * @return
     */
    public int updateChiledCode( Department department) {
        String sql = "update crl_base_department set dep_code = replace(dep_code,:oldDepCode,:depCode) where dep_code like :oldDepCodeStr";
        return update(sql,department);
    }

    public List<Department> userList(Integer userId,Integer authId) {
        String sql = "SELECT id,dep_code,dep_name,parent_id, du.user_id"+
                ",bureau_code,created_by,created_at,modified_by,modified_at,(SELECT u.REAL_NAME FROM crl_base_DEP_USER ud "+
                "LEFT JOIN crl_base_user U  ON ud.user_id = U . ID where UD.DEPARTMENT_ID = t.id and ud.auth_id= ?) realName," +
                "case when (select count(id) from crl_base_department WHERE parent_id = T . ID)> 0 then 0 else 1 end isLeaf, dep_order FROM crl_base_department t left join crl_base_dep_user du on du.department_id = t.id and du.auth_id = ?  START WITH 1=1";

        sql += " and du.user_id = ? ";
        sql += " CONNECT BY PRIOR  ID = parent_id";
        sql += " order by dep_code asc";
        return list(sql, authId,authId,userId);
    }
}
