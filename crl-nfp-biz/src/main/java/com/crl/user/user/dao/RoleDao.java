package com.crl.user.user.dao;

import com.cars.util.code.GlobalConst;
import com.crl.user.user.model.Role;
import com.crl.user.user.model.RoleResource;
import com.crl.user.user.vo.RoleSearchVO;
import com.cars.util.page.PageUtil;
import com.cars.util.string.StringUtil;
import com.crl.dao.BaseDao;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 角色操作dao
 */
@Repository
public class RoleDao extends BaseDao<Role, RoleSearchVO> {
    /**
     * 增加角色
     */
    public int saveRole(Role role) {
        String sql = "INSERT INTO crl_base_role (id,role_code,role_name,role_desc,role_type,created_at,AUTH_SYSTEM_ID) VALUES(seq_crl_base_role.nextval,:roleCode,:roleName,:roleDesc,:roleType,sysdate,:authSystemId)";
        return insertForId(sql, role, "id");
    }

    /**
     * 添加角色和资源关系表
     */
    public int[] batchResourceId(List<RoleResource> roleResources) {
        String sql = "INSERT INTO crl_base_role_resource(role_id,resource_id) VALUES (:roleId,:resourceId)";
        return getNamedParameterJdbcTemplate().batchUpdate(sql, SqlParameterSourceUtils.createBatch(roleResources));
    }

    /**
     * 校验角色代码
     */
    public Role checkRoleCode(String roleCode) {
        String sql = "SELECT id,role_code FROM crl_base_role WHERE role_code=?";
        return get(sql, roleCode);
    }

    /**
     * 根据角色id删除
     */
    public int deleteRole(Integer roleId) {
        String sql = "DELETE FROM crl_base_role WHERE id=?";
        return update(sql, roleId);
    }

    /**
     * 根据角色id删除角色资源对应关系表中数据
     */
    public int deleteResourcesRef(Integer roleId) {
        String sql = "DELETE FROM crl_base_role_resource WHERE role_id =?";
        return update(sql, roleId);
    }

    /**
     * 根据角色id修改
     */
    public int updateRole(Role role) {
        String sql = "UPDATE crl_base_role SET role_code=:roleCode,role_name=:roleName,role_desc=:roleDesc,role_type=:roleType,modified_at=sysdate,AUTH_SYSTEM_ID=:authSystemId WHERE id=:id ";
        return update(sql, role);
    }

    /**
     * 查询全部，用于选择角色
     */
    public List<Role> listAllRole() {
        String sql = "SELECT id,role_code,role_name,created_at,modified_at,role_desc,role_type,AUTH_SYSTEM_ID FROM crl_base_role order by created_at";
        return list(sql);
    }

    /**
     * 查询全部，用于选择角色
     */
    public List<Role> listAllRole(Integer authSystemId) {
        String sql = "SELECT id,role_code,role_name,created_at,modified_at,role_desc,role_type,AUTH_SYSTEM_ID FROM crl_base_role where auth_system_id=? order by created_at";
        return list(sql, authSystemId);
    }

    /**
     * 查询全部，用于选择角色
     */
    public List<Role> listRoleType() {
        String sql = "SELECT id,role_code,role_name,created_at,modified_at,role_desc,role_type,AUTH_SYSTEM_ID FROM crl_base_role where role_type="+ GlobalConst.COMMON_ROLE_TYPE +" order by created_at";
        return list(sql);
    }

    /**
     * 查询全部，用于选择角色
     */
    public List<Role> listRoleType(Integer authSystemId) {
        String sql = "SELECT id,role_code,role_name,created_at,modified_at,role_desc,role_type,AUTH_SYSTEM_ID FROM crl_base_role where role_type="+ GlobalConst.COMMON_ROLE_TYPE +" and auth_system_id=? order by created_at";
        return list(sql, authSystemId);
    }

    /**
     * 根据id查询
     */
    public Role getById(Integer roleId) {
        String sql = "SELECT id,role_code,role_name,created_at,modified_at,role_desc,role_type,AUTH_SYSTEM_ID FROM crl_base_role WHERE id=?";
        return get(sql, roleId);
    }
    /**
     * 根据角色代码获取角色
     */
    public Role getByCode(String roleCode){
        String sql = "SELECT id,role_code,role_name,created_at,modified_at,role_desc,role_type,AUTH_SYSTEM_ID FROM crl_base_role WHERE role_code=?";
        return get(sql, roleCode);
    }

    /**
     * 根据角色id获取对应的资源id信息
     */
    public List<RoleResource> getByRoleId(Integer roleId) {
        String sql = "SELECT role_id,resource_id FROM crl_base_role_resource WHERE role_id=?";
        return jdbcTemplate.query(sql, new Object[]{roleId}, BeanPropertyRowMapper.newInstance(RoleResource.class));
    }


    /**
     * 按条件查询
     */
    public List<Role> listRole(RoleSearchVO roleSearchVO) {
        String sql = "SELECT id,role_code,role_name,created_at,modified_at,role_desc,role_type,AUTH_SYSTEM_ID FROM crl_base_role WHERE 1=1 ";
        sql += createSQL(roleSearchVO);
        sql = PageUtil.createOraclePageSQL(sql, roleSearchVO.getPageIndex(), roleSearchVO.getPageSize());
        return list(sql, roleSearchVO);
    }

    /**
     * 统计查询数据条数
     */
    public int count(RoleSearchVO roleSearchVO) {
        String sql = "SELECT COUNT(id) FROM crl_base_role WHERE 1=1 ";
        sql += createSQL(roleSearchVO);
        return count(sql, roleSearchVO);
    }

    /**
     * 拼接查询条件
     */
    private String createSQL(RoleSearchVO roleSearchVO) {
        String sql = "";
        if (StringUtil.isNotNullOrEmpty(roleSearchVO.getRoleName())) {
            roleSearchVO.setRoleName(roleSearchVO.getRoleName() + "%");
            sql += " AND role_name LIKE :roleName ";
        }
        if (StringUtil.isNotNullOrEmpty(roleSearchVO.getRoleType())){
            sql +=" and role_type = :roleType";
        }
        if (roleSearchVO.getAuthSystemId()!=null){
            sql += " and auth_system_id = :authSystemId";
        }
        sql += " ORDER BY created_at DESC";
        return sql;
    }

    /**
     * 根据用户id获取用户拥有角色列表
     */
    public List<Role> getRoleByUserId(Integer userId) {
        String sql = "SELECT id,role_code,role_name,created_at,modified_at,role_desc,role_type,AUTH_SYSTEM_ID FROM crl_base_role WHERE id IN (SELECT role_id FROM crl_base_user_role WHERE user_id=?)";
        return list(sql, userId);
    }

}
