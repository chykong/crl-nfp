package com.crl.user.user.dao;


import com.crl.user.user.model.AuthSystem;
import com.crl.user.user.vo.AuthSystemVO;
import com.cars.util.page.PageUtil;
import com.cars.util.string.StringUtil;
import com.crl.dao.BaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Desc: 系统信息
 * @author: miaokaixuan
 * @Date: 2020/2/20
 */
@Repository
public class AuthSystemDao extends BaseDao<AuthSystem, AuthSystemVO> {
    /**
     * 增加资源权限
     */
    public int save(AuthSystem AuthSystem) {
        String sql = "INSERT INTO crl_base_auth_system(ID,SYSTEM_NAME,principal,CREATED_BY,SYSTEM_DESC,CREATED_AT,MODIFIED_BY,MODIFIED_AT) " +
                "VALUES(seq_crl_base_auth_system.nextval,:systemName,:principal,:createdBy,:systemDesc,sysdate,:modifiedBy,:modifiedAt)";
        return insert(sql, AuthSystem);
    }

    /**
     * 权限系统名称验重
     */
    public AuthSystem checkSystemName(String systemName) {
        String sql = "SELECT id,system_name FROM crl_base_auth_system WHERE system_name=?";
        return get(sql, systemName);
    }

    /**
     * 根据id修改
     */
    public int updateAuthSystem(AuthSystem AuthSystem) {
        String sql = "UPDATE crl_base_auth_system SET " +
                "system_name=:systemName," +
                "principal=:principal," +
                "system_desc=:systemDesc," +
                "modified_at=sysdate," +
                "modified_by=:modifiedBy" +
                " WHERE id=:id";
        return update(sql, AuthSystem);
    }

    /**
     * 根据id删除资源
     */
    public int deleteAuthSystem(Integer id) {
        String sql = "DELETE FROM crl_base_auth_system WHERE id=?";
        return delete(sql, id);
    }

    /**
     * 根据条件查询
     */
    public List<AuthSystem> listAuthSystem(AuthSystemVO authSystemVO) {
        String sql = "SELECT ID,SYSTEM_NAME,principal,CREATED_BY,SYSTEM_DESC,CREATED_AT,MODIFIED_BY,MODIFIED_AT"+
                " FROM crl_base_auth_system t WHERE 1=1 ";
        sql += createSQL(authSystemVO);
        sql += " ORDER BY created_by DESC ";
        sql = PageUtil.createOraclePageSQL(sql, authSystemVO.getPageIndex(), authSystemVO.getPageSize());
        return list(sql, authSystemVO);
    }

    /**
     * 拼接查询条件
     */
    private String createSQL(AuthSystemVO authSystemVO) {
        String sql = "";
        if (StringUtil.isNotNullOrEmpty(authSystemVO.getSystemName())) {
            authSystemVO.setSystemName("%" + authSystemVO.getSystemName() + "%");
            sql+= " AND system_name LIKE :systemName ";
        }
        return sql;
    }

    /**
     * 根据条件统计总数
     */
    public int count(AuthSystemVO authSystemVO) {
        String sql = "SELECT COUNT(id) FROM crl_base_auth_system WHERE 1=1 ";
        sql += createSQL(authSystemVO);
        return count(sql, authSystemVO);
    }

    /**
     * 获取所有系统列表
     * @param
     */
    public List<AuthSystem> listAuthSystems() {
        String sql = "SELECT ID,SYSTEM_NAME,principal,CREATED_BY,SYSTEM_DESC,CREATED_AT,MODIFIED_BY,MODIFIED_AT FROM crl_base_auth_system WHERE 1=1";
        return list(sql);

    }

    /**
     * 根据id获取资源
     */
    public AuthSystem getById(Integer AuthSystemId) {
        String sql = "SELECT ID,SYSTEM_NAME,principal,CREATED_BY,SYSTEM_DESC,CREATED_AT,MODIFIED_BY,MODIFIED_AT FROM crl_base_auth_system WHERE id=?";
        return get(sql, AuthSystemId);
    }

}
