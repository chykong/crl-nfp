package com.crl.user.user.service;

import com.cars.util.code.Constants;
import com.cars.util.date.DateUtil;
import com.cars.util.encrypt.Encryp;
import com.cars.util.global.GlobalReturnCode;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.string.StringUtil;
import com.crl.user.system.dao.SystemBindDao;
import com.crl.user.system.model.SystemBind;
import com.crl.user.user.dao.LoginDao;
import com.crl.user.user.dao.ResourceDao;
import com.crl.user.user.dao.RoleDao;
import com.crl.user.user.dto.CommonLoginDto;
import com.crl.user.user.dto.UserLoginDto;
import com.crl.user.user.model.Resource;
import com.crl.user.user.model.Role;
import com.crl.user.user.model.UserLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class LoginService {
    @Autowired
    private LoginDao loginDao;
    @Autowired
    private ResourceDao resourceDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private SystemBindDao systemBindDao;
    /**
     * 用户登录
     *
     * @param userLoginDto
     * @return
     */
    public JsonResult login(HttpServletRequest request, UserLoginDto userLoginDto) {

        UserLogin userLogin = null;
        userLogin = loginDao.getUserLoginByNameOrMobile(userLoginDto.getUsername());
        if (userLoginDto.getFlag() != null && userLoginDto.getFlag() == 0){
            // 密码解密
            String password = new String(Base64Utils.decode(userLoginDto.getPassword().getBytes()));
            userLoginDto.setPassword(password);
            //判断用户是否被禁用

            if(userLogin!=null && userLogin.getStatus()==1){
                return JsonResultUtil.error("该用户已被禁用!");
            }
            if (userLogin == null) {
                return JsonResultUtil.error("用户名或密码错误！");
            }
            int errorCount = userLogin.getErrorTimes() == null? 0 : userLogin.getErrorTimes();
            if(errorCount>=3) {
                String code = (String) redisTemplate.opsForValue().get("_code");
                if(StringUtil.isNotNullOrEmpty(userLoginDto.getCode()) && !code.equals(userLoginDto.getCode())){
                    return JsonResultUtil.error("验证码错误！");
                }
            }
            if (StringUtil.isNullOrEmpty(userLoginDto.getUsername())) {
                return JsonResultUtil.error("用户名或密码错误！");
            }
            Date sysTime = new Date();
            // 如果错误次数大于等于5次，且锁定时间在30分钟内
            if(userLogin.getLastLoginTime() != null && errorCount >= Constants.ERROR_LOGIN_COUNTS){
                userLogin.getLastLoginTime().setMinutes(userLogin.getLastLoginTime().getMinutes() + Constants.LOCK_TIME);
                if(!DateUtil.compareToday(DateUtil.dateToString(userLogin.getLastLoginTime(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss")){
                    long t = Math.round(userLogin.getLastLoginTime().getTime() - new Date().getTime()) / (60 * 1000);
                    return JsonResultUtil.error("登录失败次数过多请"+ t +"分钟后重试！");
                } else{
                    //解锁后计数重置为0
                    errorCount = 0;
                }
            }
            //设置最后登录时间
            userLogin.setLastLoginTime(new Date());
            //校验用户密码
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10);
            if (!bCryptPasswordEncoder.matches(userLoginDto.getPassword(), userLogin.getPassword())) {
                errorCount = ++errorCount;
                userLogin.setErrorTimes(errorCount);
                loginDao.updateErrorLoginInfo(userLogin);
                return JsonResultUtil.error("用户名或密码错误！");
            }
            if(userLogin.getAuditStatus() != UserLogin.AUDIT_STATUS_SUCCESS){
                return JsonResultUtil.error("用户未通过审核！");
            }
            // 密码正确后 次数重置为0
            userLogin.setErrorTimes(0);
            loginDao.updateErrorLoginInfo(userLogin);
            //获取用户权限资源
            Map<String, Object> authMap = getAuth(userLogin.getUsername());
            authMap.put("userId", userLogin.getUserId());
            return JsonResultUtil.ok(authMap);
        } else {
            if(userLogin != null) {
                Map<String, Object> authMap = getAuth(userLogin.getUsername());
                authMap.put("userId", userLogin.getUserId());
                return JsonResultUtil.ok(authMap);
            }
            return JsonResultUtil.error();
        }

    }

    /**
     * 处理获取权限信息请求
     */
    public JsonResult auth(Map<String, String> requestMap) {
        String username = requestMap.get("username");
        if (StringUtil.isNullOrEmpty(username)) {
            return JsonResultUtil.error(GlobalReturnCode.PARAMS_ERROR.getReturnCode(), "用户名不能为空！", null);
        }
        Map<String, Object> authMap = getAuth(username);
        return JsonResultUtil.ok(authMap);
    }

    /**
     * 根据用户名获取信息
     *
     * @param username
     * @return
     */
    private Map<String, Object> getAuth(String username) {
        UserLogin userLogin = loginDao.getUserLogin(username);
        //根据用户名和系统标志获取用户信息
        if (userLogin == null) {
            return null;
        }
        List<Role> roles = roleDao.getRoleByUserId(userLogin.getUserId());
        List<Integer> roleIds = roles.stream().map(Role::getId).collect(Collectors.toList());
        List<Resource> resources = new ArrayList<>();
        if(!CollectionUtils.isEmpty(roleIds)){
            resources = resourceDao.listByRoles(roleIds);
        }
        List<String> roleCodes = roles.stream().map(Role::getRoleCode).collect(Collectors.toList());
        List<String> resourceUris = resources.stream().map(Resource::getUri).collect(Collectors.toList());
        Map<String, Object> map = new HashMap<>();
        map.put("roles", roleCodes);
        map.put("resources", resourceUris);
        return map;
    }


    /**
     * 统一用户登录校验
     *
     * @param commonLoginDto
     * @return
     */
    public JsonResult commonCheck(CommonLoginDto commonLoginDto) {
        if (StringUtil.isNullOrEmpty(commonLoginDto.getHgpuser())) {
            return JsonResultUtil.error("用户名不能为空！");
        }
        if(StringUtil.isNullOrEmpty(commonLoginDto.getRandom())){
            return JsonResultUtil.error("加密盐不能为空！");
        }
        if(StringUtil.isNullOrEmpty(commonLoginDto.getCode())){
            return JsonResultUtil.error("密码不能为空！");
        }
        if(StringUtil.isNullOrEmpty(commonLoginDto.getId())){
            return JsonResultUtil.error("用户唯一标识不能为空！");
        }
        UserLogin userLogin = loginDao.getUserLogin(commonLoginDto.getHgpuser());
        if(userLogin != null) {
            int errorCount = userLogin.getErrorTimes() == null? 0 : userLogin.getErrorTimes();
            if(userLogin.getLastLoginTime() != null && errorCount >= Constants.ERROR_LOGIN_COUNTS){
                userLogin.getLastLoginTime().setMinutes(userLogin.getLastLoginTime().getMinutes() + Constants.LOCK_TIME);
                if(!DateUtil.compareToday(DateUtil.dateToString(userLogin.getLastLoginTime(), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss")){
                    long t = Math.round(userLogin.getLastLoginTime().getTime() - new Date().getTime()) / (60 * 1000);
                    return JsonResultUtil.error("登录失败次数过多，请"+ t +"分钟后重试！");
                }
            }
            String random = commonLoginDto.getRandom();
            String code = commonLoginDto.getCode();
            String pwd = Encryp.de(code, random);
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10);
            if (!bCryptPasswordEncoder.matches(pwd, userLogin.getPassword())) {
                errorCount ++;
                userLogin.setErrorTimes(errorCount);
                userLogin.setLastLoginTime(new Date());
                loginDao.updateErrorLoginInfo(userLogin);
                return JsonResultUtil.error("用户名或密码错误！");
            }
            SystemBind systemBind = systemBindDao.getByIdAndUserId(commonLoginDto.getId(), userLogin.getUserId());
            if(systemBind == null) {
                return JsonResultUtil.error("统一平台账号未绑定本业务系统账号！");
            }
            //设置最后登录时间
            userLogin.setLastLoginTime(new Date());
            // 密码正确后 次数重置为0
            userLogin.setErrorTimes(errorCount);
            loginDao.updateErrorLoginInfo(userLogin);
            return JsonResultUtil.ok();
        }
        return JsonResultUtil.error("用户名或密码错误！");
    }
}
