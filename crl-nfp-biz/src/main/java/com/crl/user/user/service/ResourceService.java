package com.crl.user.user.service;

import cn.hutool.core.bean.BeanUtil;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.crl.user.user.dao.ResourceDao;
import com.crl.user.user.dto.ResourceDto;
import com.crl.user.user.dto.ResourceTreeDto;
import com.crl.user.user.model.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class ResourceService {
    @Autowired
    private ResourceDao resourceDao;

    /**
     * 验证资源代码是否重复
     */
    public JsonResult checkCode(String resourceCode) {
        Resource resource = resourceDao.checkCode(resourceCode);
        if (resource != null) {
            return JsonResultUtil.error("资源代码重复");
        } else {
            return JsonResultUtil.ok();
        }
    }

    /**
     * 新增
     */
    public JsonResult saveResource(ResourceDto resourceDto) {
        Resource resource = new Resource();
        BeanUtil.copyProperties(resourceDto, resource);
        if (resource.getParentId() == null) {
            resource.setParentId(0);
        }
        JsonResult jsonResult = checkCode(resource.getResourceCode());
        if (jsonResult.success()) {
            int flag = resourceDao.save(resource);
            if (flag > 0) {
                return JsonResultUtil.ok();
            } else {
                return JsonResultUtil.error("资源新增失败");
            }
        } else {
            return jsonResult;
        }
    }

    /**
     * 根据id修改
     */
    public JsonResult updateResource(ResourceDto resourceDto) {
        Resource resource = new Resource();
        BeanUtil.copyProperties(resourceDto, resource);
        Resource res = resourceDao.checkCode(resourceDto.getResourceCode());
        JsonResult jsonResult = JsonResultUtil.ok();
        if (res!=null &&res.getId().intValue() != resourceDto.getId().intValue()) {
            jsonResult = checkCode(resource.getResourceCode());
        }
        if (jsonResult.success()) {
            int flag = resourceDao.updateResource(resource);
            if (flag > 0) {
                return JsonResultUtil.ok();
            } else {
                return JsonResultUtil.error("资源修改失败");
            }
        } else {
            return jsonResult;
        }


    }

    /**
     * 删除
     */
    public JsonResult deleteResource(ResourceDto resourceDto) {
        int childrenNum = resourceDao.listChildren(resourceDto.getId());
        if (childrenNum > 0) {
            return JsonResultUtil.error("存在下级节点不能删除");
        }
        int flag = resourceDao.deleteResource(resourceDto.getId());
        if (flag > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error();
        }
    }

    /**
     * 禁用
     */
    public JsonResult updateStatus(ResourceDto resourceDto) {
        int flag = resourceDao.updateStatus(resourceDto.getId());
        if (flag > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error();
        }
    }

    /**
     * 获取资源树
     * @param authSystemId
     */
    public JsonResult<List<ResourceTreeDto>> listResources(Integer authSystemId) {
        List<Resource> resourceList = resourceDao.listResources(authSystemId);
        List<ResourceTreeDto> tmp = new ArrayList<>();
        for(Resource resource: resourceList){
            ResourceTreeDto dto = new ResourceTreeDto();
            BeanUtils.copyProperties(resource, dto);
            tmp.add(dto);
        }
        List<ResourceTreeDto> result = createResourceTree(tmp, 0);
        return JsonResultUtil.ok(result);
    }

    private List<ResourceTreeDto> createResourceTree(List<ResourceTreeDto> list, int parentId) {
        List<ResourceTreeDto> listChildren = new ArrayList<>();
        for (ResourceTreeDto tmp : list) {
            if (tmp.getParentId() == parentId) {
                listChildren.add(tmp);
                tmp.setChildren(createResourceTree(list, tmp.getId()));
            }
        }
        return listChildren;
    }

    /**
     * 根据资源id获取资源对象
     */
    public JsonResult<ResourceDto> getById(ResourceDto resourceDto) {
        Resource resource = resourceDao.getById(resourceDto.getId());
        BeanUtils.copyProperties(resource, resourceDto);
        return JsonResultUtil.ok(resourceDto);
    }

}
