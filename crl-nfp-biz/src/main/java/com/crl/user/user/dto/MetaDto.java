package com.crl.user.user.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 路由元数据信息
 */
@ApiModel("路由元数据信息")
@Data
public class MetaDto {
    /**
     * 标题
     */
    @ApiModelProperty("标题")
    private String title;
    /**
     * 图标
     */
    @ApiModelProperty("图标")
    private String icon;
    /**
     * 页面缓存 true缓存false不缓存
     */
    @ApiModelProperty("页面缓存 true缓存false不缓存")
    private Boolean keepAlive;
    /**
     * 类型1模块2功能
     */
    @ApiModelProperty("类型1模块2功能")
    private Integer type;
    /**
     * 权限代码
     */
    @ApiModelProperty("权限代码")
    private String permission;
}
