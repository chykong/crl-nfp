package com.crl.user.user.service;

import cn.hutool.core.bean.BeanUtil;
import com.cars.util.bean.BasePage;
import com.cars.util.code.GlobalConst;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.list.ListUtils;
import com.cars.util.string.StringUtil;
import com.crl.user.user.dao.ResourceDao;
import com.crl.user.user.dao.RoleDao;
import com.crl.user.user.dto.RoleDto;
import com.crl.user.user.model.Resource;
import com.crl.user.user.model.Role;
import com.crl.user.user.model.RoleResource;
import com.crl.user.user.vo.RoleSearchVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(rollbackFor = Exception.class)
public class RoleService {
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private ResourceDao resourceDao;

    /**
     * 验证角色代码是否存在
     */
    public JsonResult chekcRoleCode(String roleCode) {
        Role role = roleDao.checkRoleCode(roleCode);
        if (role != null) {
            return JsonResultUtil.error("角色代码重复");
        } else {
            return JsonResultUtil.ok();
        }
    }

    /**
     * 增加
     */
    public JsonResult saveRole(RoleDto roleDto) {
        Role role = new Role();
        BeanUtil.copyProperties(roleDto, role);
        JsonResult jsonResult = chekcRoleCode(roleDto.getRoleCode());
        if (!jsonResult.success()) {
            return jsonResult;
        }
        String[] resourceIds = new String[0];
        if (StringUtil.isNotNullOrEmpty(roleDto.getResourceIds())) {
            resourceIds = roleDto.getResourceIds().split("@");
        }
        int[] flags = new int[0];
        int roleId = roleDao.saveRole(role);
        if (roleId > 0) {
            List<RoleResource> roleResources = new ArrayList<>();
            for (int i = 0; i < resourceIds.length; i++) {
                roleResources.add(new RoleResource(roleId, Integer.valueOf(resourceIds[i])));
            }
            if (roleResources.isEmpty()) {
                return JsonResultUtil.ok();
            } else {
                flags = roleDao.batchResourceId(roleResources);
                if (flags.length == 0) {
                    return JsonResultUtil.error("角色添加失败");
                } else {
                    return JsonResultUtil.ok();
                }
            }
        } else {
            return JsonResultUtil.error();
        }

    }

    /**
     * 删除
     */
    public JsonResult deleteRole(RoleDto roleDto) {
        int flag = roleDao.deleteRole(roleDto.getId());
        if (flag > 0) {
            flag = roleDao.deleteResourcesRef(roleDto.getId());
            if (flag == 0) {
                return JsonResultUtil.error();
            } else {
                return JsonResultUtil.ok();
            }
        }
        return JsonResultUtil.error();
    }

    /**
     * 修改
     */
    public JsonResult updateRole(RoleDto roleDto) {
        Role role = new Role();
        Role checkRole = roleDao.checkRoleCode(roleDto.getRoleCode());
        if (checkRole != null && !checkRole.getId().equals(roleDto.getId())) {
            return JsonResultUtil.error("角色代码重复");
        }
        BeanUtil.copyProperties(roleDto, role);
        int flag = roleDao.updateRole(role);
        if (flag > 0) {
            roleDao.deleteResourcesRef(roleDto.getId());
            String[] resourceIds = new String[0];
            if (StringUtil.isNotNullOrEmpty(roleDto.getResourceIds())) {
                resourceIds = roleDto.getResourceIds().split("@");
            }
            if (resourceIds.length == 0) {
                return JsonResultUtil.ok();
            } else {
                List<RoleResource> roleResources = new ArrayList<>();
                for (int i = 0; i < resourceIds.length; i++) {
                    roleResources.add(new RoleResource(roleDto.getId(), Integer.valueOf(resourceIds[i])));
                }
                int[] flags = roleDao.batchResourceId(roleResources);
                if (flags.length == 0) {
                    return JsonResultUtil.error("修改角色失败");
                } else {
                    return JsonResultUtil.ok();
                }
            }
        } else {
            return JsonResultUtil.error("修改角色失败");
        }
    }


    /**
     * 获取所有角色
     */
    public JsonResult<List<RoleDto>> listAll() {
        List<Role> list = roleDao.listAllRole();
        List<RoleDto> roleDtoList = ListUtils.copyList(list, RoleDto.class);
        return JsonResultUtil.ok(roleDtoList);
    }

    /**
     * 根据id获取数据
     */
    public JsonResult<RoleDto> getById(RoleDto roleDto) {
        Role role = roleDao.getById(roleDto.getId());
        if(role == null){
            return JsonResultUtil.error("角色id错误");
        }
        List<Resource> resourceList = resourceDao.listResources(role.getAuthSystemId());
        List<RoleResource> roleResourceList = roleDao.getByRoleId(roleDto.getId());
        StringBuffer stringBuffer = new StringBuffer(50);
        roleResourceList.forEach(tmp -> {
            if(isLeaf(tmp.getResourceId(), resourceList)){
                stringBuffer.append(String.valueOf(tmp.getResourceId()));
                stringBuffer.append("@");
            }
        });
        BeanUtils.copyProperties(role, roleDto);
        roleDto.setResourceIds(stringBuffer.toString());
        return JsonResultUtil.ok(roleDto);
    }
    /**
     * 获取条件查询角色
     */
    public JsonResult<BasePage<List<RoleDto>>> listSearch(RoleSearchVO roleSearchVO) {
        if(roleSearchVO.getUserId()!= GlobalConst.ADMIN_USER_ID){
            roleSearchVO.setRoleType(GlobalConst.COMMON_ROLE_TYPE);
        }
        List<Role> roleList = roleDao.listRole(roleSearchVO);
        int count = roleDao.count(roleSearchVO);
        BasePage<List<RoleDto>> basePage  = new BasePage<>();
        List<RoleDto> roleDtoList = ListUtils.copyList(roleList,RoleDto.class);
        basePage.setTotal(count);
        basePage.setData(roleDtoList);
        return JsonResultUtil.ok(basePage);
    }

    /**
     * 判断当前节点是否是叶子节点
     * @param resourceId
     * @param resourceList
     * @return true:叶子节点
     */
    private Boolean isLeaf(Integer resourceId, List<Resource> resourceList){
        for(Resource resource: resourceList){
            if(resource.getParentId().intValue() == resourceId.intValue()){
                return false;
            }
        }
        return true;
    }

    /**
     * 根据用户id查询角色信息
     * @param userId
     * @return
     */
    public JsonResult<Map<String,Object>> listByUserId(Integer userId) {
        Map<String,Object> result = new HashMap<>();
        List<Role> list = new ArrayList<>();
        if(userId == GlobalConst.ADMIN_USER_ID){
            list = roleDao.listAllRole();
        }else{
            list = roleDao.listRoleType();
        }

        result.put("default",81);
        List<RoleDto> roleDtoList = ListUtils.copyList(list,RoleDto.class);
        result.put("list",roleDtoList);
        return JsonResultUtil.ok(result);
    }

    /**
     * 根据用户id和所属系统ID查询角色信息
     * @param userId, authSystemId
     * @return
     */
    public JsonResult<Map<String,Object>> listByUserAndSystem(Integer userId, Integer authSystemId) {
        Map<String,Object> result = new HashMap<>();
        List<Role> list = new ArrayList<>();
        if(userId == GlobalConst.ADMIN_USER_ID){
            list = roleDao.listAllRole(authSystemId);
        }else{
            list = roleDao.listRoleType(authSystemId);
        }
        result.put("default",81);
        List<RoleDto> roleDtoList = ListUtils.copyList(list,RoleDto.class);
        result.put("list",roleDtoList);
        return JsonResultUtil.ok(result);
    }
}
