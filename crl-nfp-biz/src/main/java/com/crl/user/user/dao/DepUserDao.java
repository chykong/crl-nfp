package com.crl.user.user.dao;


import com.crl.user.user.model.DepUser;
import com.crl.dao.BaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Description:部门负责人dao
 * @Date:2020/4/14 16:52
 * @Author:苗凯旋
 */
@Repository
public class DepUserDao extends BaseDao<DepUser,DepUser> {
    /**
     * 根据机构和系统id获取用户id
     * @param departmentId
     * @param authId
     * @return
     */
    public DepUser getUserId(Integer departmentId, Integer authId) {
        String sql = "select user_id,department_id,auth_id from crl_base_dep_user where department_id=? and auth_id=?";
        return get(sql,departmentId,authId);
    }

    /**
     * 修改负责人
     * @param depUser
     * @return
     */
    public int updateUserId(DepUser depUser) {
        String sql = "update crl_base_dep_user set user_id = :userId where department_id = :departmentId and auth_id = :authId";
        return update(sql,depUser);
    }

    /**
     * 根据用户id获取机构权限
     * @param de
     * @return
     */
    public List<DepUser> listByUserId(DepUser de) {
        String sql = "select user_id,department_id,auth_id from crl_base_dep_user where 1=1";
        if(de.getUserId()!=null) {
            sql += " and user_id = :userId";
        }
        return list(sql,de);
    }

    /**
     * 新增数据
     * @param depUser
     * @return
     */
    public int add(DepUser depUser) {
        String sql = "insert into crl_base_dep_user(user_id,department_id,auth_id) values(:userId,:departmentId,:authId) ";
        return insert(sql,depUser);
    }
}
