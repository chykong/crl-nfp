package com.crl.user.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "注册信息对象")
public class RegisterDto {
    /**
     * 用户名
     */
    @ApiModelProperty("用户名，需要包含字母数字等，不能为纯数字,长度不能超过20")
    @NotBlank(message = "用户名不能为空")
    @Length(max = 20,message = "用户名长度不能超过20")
    private String username;
    /**
     * 手机号
     */
    @ApiModelProperty("手机号")
    private String mobile;
    /**
     * 验证码
     */
    @ApiModelProperty("验证码")
    private String code;
    /**
     * 登录密码
     */
    @ApiModelProperty("登录密码")
    @NotBlank(message = "密码不能为空")
    private String password;
    /**
     * 确认登录密码
     */
    @ApiModelProperty("确认登录密码")
    @NotBlank(message = "确认密码不能为空")
    private String repassword;
    /**
     * 真实姓名
     */
    @ApiModelProperty("真实姓名")
    @NotBlank(message = "真实姓名")
    private String realName;
    /**
     * 所属部门Id
     */
    @ApiModelProperty("所属部门id")
    @NotBlank(message = "所属部门id")
    private Integer departmentId;
    /**
     * 所属部门名称
     */
    @ApiModelProperty("所属部门名称")
    @NotBlank(message = "所属部门名称")
    private String departmentName;
    /**
     * 身份证号
     */
    @ApiModelProperty("身份证号")
    @NotBlank(message = "身份证号")
    private String idcard;
    /**
     * 路局
     */
    @ApiModelProperty("路局")
    @NotBlank(message = "路局")
    private String bureauCode;

    /**
     *  huo yun中心
     */
    @ApiModelProperty(" huo yun中心")
    @NotBlank(message = " huo yun中心")
    private String freightCode;

    /**
     * 车站
     */
    @ApiModelProperty("车站")
    @NotBlank(message = "车站")
    private String stationCode;
}
