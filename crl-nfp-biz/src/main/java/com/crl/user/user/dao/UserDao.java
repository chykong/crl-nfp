package com.crl.user.user.dao;


import com.crl.user.user.model.User;
import com.crl.user.user.model.UserRole;
import com.crl.user.user.vo.UserSearchVO;
import com.cars.util.page.PageUtil;
import com.cars.util.string.StringUtil;
import com.crl.dao.BaseDao;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDao extends BaseDao<User, UserSearchVO> {
    /**
     * 根据用户id获取用户
     */
    public User getById(Integer id) {
        String sql = "SELECT id,username,real_name,telephone,mobile,idcard,department_id,auth_system_id,workcard,position,remark,bureau_code,freight_code,station_code,created_by,created_at,modified_by,modified_at,(SELECT status FROM crl_base_userlogin WHERE user_id=id) status FROM crl_base_user WHERE id=?";
        return get(sql, id);
    }

    /**
     * 根据用户名获取用户
     */
    public User getByUsername(String username) {
        String sql = "SELECT u.id,u.username,u.real_name,u.telephone,u.mobile,u.idcard,u.department_id,u.auth_system_id,u.workcard,u.position,u.remark,u.bureau_code,freight_code,station_code,u.created_by,u.created_at,u.modified_by,u.modified_at,(SELECT status FROM crl_base_userlogin WHERE user_id=u.id) status FROM crl_base_user u WHERE username=?";
        return get(sql, username);
    }
    /**
     * 根据手机号获取用户
     */
    public User getByMobile(String mobile) {
        String sql = "SELECT id,username,real_name,telephone,mobile,idcard,department_id,auth_system_id,workcard,position,remark,bureau_code,freight_code,station_code,created_by,created_at,modified_by,modified_at FROM crl_base_user WHERE mobile=?";
        return get(sql, mobile);
    }

    /**
     * 根据条件查询
     */
    public List<User> listUser(UserSearchVO userSearchVO) {
        String sql = "SELECT id,username,real_name,telephone,mobile,idcard,department_id,auth_system_id,workcard,position,remark,bureau_code,freight_code,station_code,created_by,created_at,modified_by,modified_at,(SELECT status FROM crl_base_userlogin WHERE user_id=t.id) status,(SELECT audit_status FROM crl_base_userlogin WHERE user_id=t.id) audit_status,(SELECT dep_name FROM crl_base_department WHERE id=t.department_id) department_name,"+
                "case when (select count(1) from crl_base_dep_user du where du.user_id =t.id and du.department_id = :deptid)> 0 then 1 else 0 end flag FROM crl_base_user t WHERE 1=1 ";
        sql += createSQL(userSearchVO);
        sql += " ORDER BY created_by DESC ";
        sql = PageUtil.createOraclePageSQL(sql, userSearchVO.getPageIndex(), userSearchVO.getPageSize());
        return list(sql, userSearchVO);
    }

    /**
     * 拼接查询条件
     */
    private String createSQL(UserSearchVO userSearchVO) {
        StringBuffer stringBuffer = new StringBuffer(50);
        if (StringUtil.isNotNullOrEmpty(userSearchVO.getUsername())) {
            userSearchVO.setUsername("%" + userSearchVO.getUsername() + "%");
            stringBuffer.append(" AND username LIKE :username ");
        }
        if (userSearchVO.getAuthSystemId()!=null){
            stringBuffer.append(" and auth_system_id = :authSystemId");
        }
        if (StringUtil.isNotNullOrEmpty(userSearchVO.getRealName())) {
            userSearchVO.setRealName("%" + userSearchVO.getRealName() + "%");
            stringBuffer.append(" AND real_name LIKE :realName ");
        }
        //查询当前登录用户所能查看机构 下所有用户
        if (StringUtil.isNotNullOrEmpty(userSearchVO.getUserId())) {
            stringBuffer.append(" AND department_id in (SELECT id FROM crl_base_department t start with t.id in (SELECT id FROM crl_base_department where user_id = :userId) connect by t.parent_id= prior t.id) ");
        }
        //根据root往树末梢递归查询，当前部门下所有子部门的用户列表
        if (StringUtil.isNotNullOrEmpty(userSearchVO.getDepCode())) {
            stringBuffer.append(" AND department_id in (SELECT id FROM crl_base_department t where t.dep_code like :depCodeStr )");
        }
        if (StringUtil.isNotNullOrEmpty(userSearchVO.getDeptid())) {
            stringBuffer.append(" AND department_id = :deptid");
        }
        return stringBuffer.toString();
    }

    /**
     * 根据条件统计总数
     */
    public int count(UserSearchVO userSearchVO) {
        String sql = "SELECT COUNT(id) FROM crl_base_user WHERE 1=1 ";
        sql += createSQL(userSearchVO);
        return count(sql, userSearchVO);
    }

    /**
     * 根据用户id删除用户信息
     */
    public int deleteByUserId(Integer userId) {
        String sql = "DELETE FROM crl_base_user WHERE id=?";
        return update(sql, userId);
    }

    /**
     * 新增用户,并返回id
     */
    public Integer saveUser(User user) {
        String sql = "INSERT INTO crl_base_user (id,username,real_name,auth_system_id,telephone,mobile,idcard,department_id,workcard,position,remark,bureau_code,freight_code,station_code,created_by,created_at) " +
                " VALUES(seq_crl_base_user.nextval,:username,:realName,:authSystemId,:telephone,:mobile,:idcard,:departmentId,:workcard,:position,:remark,:bureauCode,:freightCode,:stationCode,:createdBy,sysdate)";
        return insertForId(sql, user, "id");
    }

    /**
     * 根据用户id修改用户
     */
    public int update(User user) {
        String sql = "UPDATE crl_base_user SET " +
                "username=:username," +
                "real_name=:realName," +
                "telephone=:telephone," +
                "mobile=:mobile," +
                "idcard=:idcard," +
                "auth_system_id=:authSystemId," +
                "department_id=:departmentId," +
                "workcard=:workcard," +
                "position=:position," +
                "remark=:remark," +
                "bureau_code=:bureauCode," +
                "freight_code=:freightCode," +
                "station_code=:stationCode," +
                "modified_by=:modifiedBy," +
                "modified_at=:modifiedAt" +
                " WHERE id=:id";
        return update(sql, user);
    }

    /**
     * 添加角色和资源关系表
     */
    public int[] batchRoleId(List<UserRole> userRoles) {
        String sql = "INSERT INTO crl_base_user_role(user_id,role_id) VALUES (:userId,:roleId)";
        return getNamedParameterJdbcTemplate().batchUpdate(sql, SqlParameterSourceUtils.createBatch(userRoles));
    }

    /**
     * 根据用户和角色查询该用户是否拥有此角色
     * @param roleId
     * @return
     */
    public int countUserRole(Integer userId, Integer roleId){
        String sql = "select count(*) from crl_base_user_role where user_id=? and role_id=?";
        return count(sql,userId,roleId);
    }

    /**
     * 根据用户id删除资源角色关系表
     */
    public int deleteRoleIds(Integer userId) {
        String sql = "DELETE FROM  crl_base_user_role WHERE user_id=?";
        return delete(sql, userId);
    }

    /**
     * 根据条件查询用户列表
     * @param userSearchVO
     * @return
     */
    public List<User> findUser(UserSearchVO userSearchVO) {
        String sql = "SELECT id,username,real_name,telephone,mobile,idcard,department_id,auth_system_id,workcard,position,remark,bureau_code,freight_code,station_code,created_by,created_at,modified_by,modified_at,(SELECT status FROM crl_base_userlogin WHERE user_id=t.id) status,(SELECT audit_status FROM crl_base_userlogin WHERE user_id=t.id) audit_status,(SELECT dep_name FROM crl_base_department WHERE id=t.department_id) department_name FROM crl_base_user t WHERE 1=1 ";
        sql += createSQL(userSearchVO);
        sql += " ORDER BY created_by DESC ";
        return list(sql, userSearchVO);
    }

    /**
     * 根据用户id和角色id删除
     * @param userId
     * @param roleId
     * @return
     */
    public int deleteByUserAndRole(Integer userId, Integer roleId) {
        String sql = "DELETE FROM  crl_base_user_role WHERE user_id=? and role_id = ?";
        return delete(sql, userId, roleId);
    }

    /**
     * 查询当前部门下有多少用户
     * @param id
     * @return
     */
    public Integer countByDepartmentId(Integer id) {
        String sql = "select count(*) from crl_base_user where department_id = ?";
        return count(sql,id);
    }
}
