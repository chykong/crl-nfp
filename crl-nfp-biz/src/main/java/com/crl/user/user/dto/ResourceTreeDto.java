package com.crl.user.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("资源树数据")
public class ResourceTreeDto {
    /**
     * 资源id
     */
    @ApiModelProperty("资源id")
    private Integer id;
    /**
     * 资源名称
     */
    @ApiModelProperty("资源名称")
    private String resourceName;
    /**
     * 资源代码
     */
    @ApiModelProperty("资源代码")
    private String resourceCode;
    /**
     * 父节点id
     */
    @ApiModelProperty("父节点id")
    private Integer parentId;
    /**
     * 资源类型0模块，1功能
     */
    @ApiModelProperty("资源类型0模块，1功能")
    private Integer resourceType;
    /**
     * 资源路径
     */
    @ApiModelProperty("资源路径")
    private String uri;
    /**
     * 子节点列表
     */
    @ApiModelProperty("子节点列表")
    private List<ResourceTreeDto> children;
}
