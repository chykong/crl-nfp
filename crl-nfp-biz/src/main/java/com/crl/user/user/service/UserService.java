package com.crl.user.user.service;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.RandomUtil;
import com.cars.util.bean.BasePage;
import com.cars.util.encrypt.Encryp;
import com.cars.util.global.GlobalReturnCode;
import com.cars.util.http.HttpUtil;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.json.JsonUtil;
import com.cars.util.list.ListUtils;
import com.cars.util.string.StringUtil;
import com.crl.user.user.dao.*;
import com.crl.user.user.dto.HyRouteDto;
import com.crl.user.user.dto.RouteDto;
import com.crl.user.user.dto.UpdatePassDto;
import com.crl.user.user.dto.UserDto;
import com.crl.user.user.model.*;
import com.crl.user.user.vo.UserSearchVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class UserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private RegisterDao registerDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private LoginDao loginDao;
    @Autowired
    private ResourceDao resourceDao;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private DepartmentDao departmentDao;
    @Autowired
    private DepartmentService departmentService;

    /**
     * 默认密码
     */
    public static final String DEFAULT_PASSWORD = "0123456789";
    /**
     * 默认角色代码
     */
    public static final String DEFAULT_ROLE = "DEFAULT";
    /**
     * 系统管理员
     */
    public static final String SYSTEM_ROLE = "admin";
    /**
     * 部门管理员默认角色
     */
    public static final String DEPARTMENT_MANAGER = "DEPARTMENT_MANAGER";

    /**
     * 修改密码
     */
    public JsonResult updatePwd(UpdatePassDto updatePassDto) {
        String username = updatePassDto.getUsername();
        Integer id = updatePassDto.getId();
        if (null == id) {
            return JsonResultUtil.error("用户信息有误！");
        }
        String password = updatePassDto.getPassword();
        String rePassword = updatePassDto.getRePassword();
        if (!password.equals(rePassword)) {
            return JsonResultUtil.error("两次输入密码不一致！");
        }
        UserLogin userLogin = loginDao.getUserLogin(id);
        if (userLogin == null) {
            return JsonResultUtil.error("用户不存在！");
        }
        //判断原密码是否正确
        String oldPassword = updatePassDto.getOldPassword();
        //校验用户密码
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10);
        if (!bCryptPasswordEncoder.matches(oldPassword, userLogin.getPassword())) {
            return JsonResultUtil.error("原密码错误！");
        }
        String pwd = new BCryptPasswordEncoder(10).encode(password);
        userLogin.setUserId(id);
        userLogin.setUsername(username);
        userLogin.setPassword(pwd);
        userLogin.setChangePasswordTime(new Date());
        userLogin.setErrorTimes(0);
        int flag = loginDao.updatePwd(userLogin);
        if (flag > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("修改密码失败！");
        }
    }

    /**
     * 验证登录账号是否存在
     */
    public JsonResult checkCode(Map<String, String> requestMap) {
        String username = requestMap.get("username");
        if (StringUtil.isNullOrEmpty(username)) {
            return JsonResultUtil.error(GlobalReturnCode.PARAMS_ERROR.getReturnCode(), "用户名不能为空", null);
        }
        User user = userDao.getByUsername(username);
        if (user == null) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("用户名重复");
        }
    }

    /**
     * 验证手机号是否存在
     */
    public JsonResult checkMobile(Map<String, String> requestMap) {
        String mobile = requestMap.get("mobile");
        if (StringUtil.isNullOrEmpty(mobile)) {
            return JsonResultUtil.ok();
        }
        if (!Validator.isMobile(mobile)) {
            return JsonResultUtil.error("手机号格式不正确(?:0|86|\\+86)?1[3456789]\\d{9}");
        }
//        User user = userDao.getByMobile(mobile);
        UserLogin userLogin = loginDao.getUserLoginByMobile(mobile);
        if (userLogin == null) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("手机号已注册");
        }
    }
    /**
     * 验证身份证号是否存在
     */
    public JsonResult checkIdCard(Map<String, String> requestMap) {
        String idCard = requestMap.get("idCard");
        if (StringUtil.isNullOrEmpty(idCard)) {
            JsonResultUtil.error(GlobalReturnCode.PARAMS_ERROR.getReturnCode(), "身份证号不能为空", null);
        }
        UserLogin userLogin = loginDao.getUserLoginByIdCard(idCard);
        if (userLogin == null) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("身份证号已注册");
        }
    }
    /**
     * 增加用户信息
     * 先增加用户信息表，然后增加登录表
     */
    public JsonResult saveUser(UserDto userDto) {
        String password = new BCryptPasswordEncoder(10).encode(UserService.DEFAULT_PASSWORD);
        UserLogin userLogin = new UserLogin();
        BeanUtils.copyProperties(userDto, userLogin);
        userLogin.setPassword(password);

        User user = new User();
        BeanUtils.copyProperties(userDto, user);
        //增加用户信息
        int userId = userDao.saveUser(user);
        if (userId <= 0) {
            return JsonResultUtil.error("创建用户失败");
        }
        userLogin.setUserId(userId);
        int flag = registerDao.register(userLogin);
        if (flag == 0) {
            return JsonResultUtil.error("创建用户失败");
        }
        //  将用户id插入用户实体类新增用户角色用
        userDto.setId(userId);
        List<UserRole> userRoles = listUserRole(userDto);
        int[] flags = userDao.batchRoleId(userRoles);
        if (flags.length > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("分配角色失败");
        }
    }

    /**
     * 根据用户id和角色id构建集合，插入到用户角色对应表中
     *
     * @param userDto
     * @return
     */
    private List<UserRole> listUserRole(UserDto userDto) {
        String roleIds = userDto.getRoleIds();
        if (StringUtil.isNullOrEmpty(roleIds)) {
            //分配默认角色
            Role role = roleDao.getByCode(UserService.DEFAULT_ROLE);
            roleIds = String.valueOf(role.getId());
        }
        //分配角色
        List<String> roleIdstrs = Arrays.asList(roleIds.split("@"));
        List<UserRole> userRoles = new ArrayList<>();
        roleIdstrs.forEach(tmp -> {
            UserRole userRole = new UserRole();
            userRole.setRoleId(Integer.valueOf(tmp));
            userRole.setUserId(userDto.getId());
            userRoles.add(userRole);
        });
        return userRoles;
    }

    /**
     * 修改个人信息
     */
    public JsonResult update(UserDto userDto) {
        if(!userDto.getOldMobile().equals(userDto.getMobile())){
            String mobile = userDto.getMobile();
            String code = userDto.getCode();
            //验证手机号
            if(!Validator.isMobile(mobile)){
                return JsonResultUtil.error("手机号有错误");
            }
            ValueOperations operations = redisTemplate.opsForValue();
            Object obj = operations.get(mobile);
            if(obj == null){
                return JsonResultUtil.error("验证码已过期");
            }
            if(!code.equalsIgnoreCase(obj.toString())){
                return JsonResultUtil.error("验证码不正确");
            }
            redisTemplate.delete(mobile);
        }
        //删除角色关系表
        int flag = userDao.deleteRoleIds(userDto.getId());
        if (flag <= 0) {
            return JsonResultUtil.error("删除角色关系失败");
        }

        List<UserRole> userRoles = listUserRole(userDto);
        int[] flags = userDao.batchRoleId(userRoles);
        if (flags.length > 0) {
            User user = new User();
            BeanUtils.copyProperties(userDto, user);
            flag = userDao.update(user);
            if (flag == 0) {
                return JsonResultUtil.error("修改用户信息失败");
            } else {
                //如果手机号修改，则修改登陆表里的用户手机号
                if(!userDto.getOldMobile().equals(userDto.getMobile())){
                    UserLogin userLogin = new UserLogin();
                    userLogin.setUserId(user.getId());
                    userLogin.setMobile(user.getMobile());
                    flag = loginDao.updateMobile(userLogin);
                    if(flag == 0) {
                        return JsonResultUtil.error("修改用户信息失败");
                    }else{
                        return JsonResultUtil.ok();
                    }
                }else{
                    return JsonResultUtil.ok();
                }

            }
        } else {
            return JsonResultUtil.error("分配角色失败");
        }
    }

    /**
     * 删除个人信息
     */
    public JsonResult deleteUser(UserDto userDto) {
        //删除角色关系表
        int flag = userDao.deleteRoleIds(userDto.getId());
        if (flag == 0) {
            return JsonResultUtil.error("删除角色关系失败");
        }
        //删除登录信息表
        flag = loginDao.deleteUserLogin(userDto.getId());
        if (flag == 0) {
            return JsonResultUtil.error("删除登录信息失败");
        }
        //删除基本信息表
        flag = userDao.deleteByUserId(userDto.getId());
        if (flag == 0) {
            return JsonResultUtil.error("删除用户信息失败");
        }
        return JsonResultUtil.ok();
    }

    /**
     * 修改用户状态
     */
    public JsonResult updateStatus(UserDto userDto) {
        int flag = loginDao.updateStatus(userDto.getId());
        if (flag > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("更新用户状态失败");
        }
    }

    /**
     * 根据条件查询用户
     */
    public JsonResult listUser(UserSearchVO userSearchVO) {
        BasePage<List<UserDto>> basePage = new BasePage();
        if(userSearchVO.getDepartmentId()!=null){
            Department department = departmentDao.getById(userSearchVO.getDepartmentId());
            if(department!=null){
                userSearchVO.setDepCode(department.getDepCode());
            }
        }
        int count = userDao.count(userSearchVO);
        basePage.setTotal(count);
        List<User> list = userDao.listUser(userSearchVO);
        List<UserDto> listResult = ListUtils.copyList(list, UserDto.class);
        basePage.setData(listResult);
        return JsonResultUtil.ok(basePage);
    }

    public JsonResult getByUsername(UserDto userDto) {
        User user = userDao.getByUsername(userDto.getUsername());
        BeanUtils.copyProperties(user, userDto);
        return JsonResultUtil.ok(userDto);
    }

    public UserDto getByUsername(String username) {
        User user = userDao.getByUsername(username);
        UserDto dto = new UserDto();
        if(user != null) {
            BeanUtils.copyProperties(user, dto);
        }
        return dto;
    }

    public UserDto getById(Integer id) {
        User user = userDao.getById(id);
        UserDto dto = new UserDto();
        if(user != null) {
            BeanUtils.copyProperties(user, dto);
        }
        return dto;
    }

    /**
     * 根据id获取用户信息
     *
     * @param userDto
     * @return
     */
    public JsonResult get(UserDto userDto) {
        //获取用户
        User user = userDao.getById(userDto.getId());
        Integer depId = user.getDepartmentId();
        BeanUtils.copyProperties(user, userDto);
        //获取登录信息
        UserLogin userLogin = loginDao.getUserLogin(user.getId());
        userDto.setChangePasswordTime(userLogin.getChangePasswordTime());
        //获取角色ids
        List<Role> roleList = roleDao.getRoleByUserId(userDto.getId());
        StringBuffer stringBuffer = new StringBuffer(50);
        roleList.stream().forEach(tmp -> {
            stringBuffer.append(tmp.getId());
            stringBuffer.append("@");
        });
        //权限
        List<Integer> roleIds = roleList.stream().map(Role::getId).collect(Collectors.toList());
        List<Resource> resources = resourceDao.listByRoles(roleIds);
        List<String> resourceCodes = resources.stream().map(Resource::getResourceCode).collect(Collectors.toList());
        userDto.setResourceCodes(resourceCodes);
        userDto.setRoleIds(stringBuffer.toString());
        userDto.setRoutes(getRouterList(userDto));
        userDto.setOldMobile(user.getMobile());
        if(null != depId){
//            userDto.setDepartmentName(departmentService.getAllNameByDepid(depId));
        }
        return JsonResultUtil.ok(userDto);
    }

    /**
     * 根据id获取用户信息
     *
     * @param userDto
     * @return
     */
    public JsonResult getPermission(UserDto userDto) {
//        //获取用户
//        User user = userDao.getById(userDto.getId());
//        //获取角色ids
//        List<Role> roleList = roleDao.getRoleByUserId(userDto.getId());
//        StringBuffer stringBuffer = new StringBuffer(50);
//        roleList.stream().forEach(tmp -> {
//            stringBuffer.append(String.valueOf(tmp.getId()));
//            stringBuffer.append("@");
//        });
//        BeanUtils.copyProperties(user, userDto);
//        userDto.setRoleIds(stringBuffer.toString());
//        //根据角色获取资源
//        List<Integer> roleIds = roleList.stream().map(Role::getId).collect(Collectors.toList());
//        List<Resource> resourceList = resourceDao.listByRoles(roleIds);
//        List<RouteDto> routeDtoList = new ArrayList<>();
//        for(Resource tmp: resourceList){
//            RouteDto routeDto = new RouteDto();
//            if(tmp.getComponent() != null) {
//                //路由信息
//                routeDto.setId(tmp.getId());
//                routeDto.setParentId(tmp.getParentId());
//                routeDto.setName(tmp.getResourceCode());
//                routeDto.setPath(tmp.getPath());
//                routeDto.setComponent(tmp.getComponent());
//                routeDto.setRedirect(tmp.getRedirectPath());
//                routeDto.setHidden(tmp.getIsShow() == 1);
//                routeDto.setTitle(tmp.getResourceName());
//                routeDto.setIcon(tmp.getIcon());
//                routeDto.setPermission(tmp.getResourceCode());
//                routeDtoList.add(routeDto);
//            }
//        }
//        return JsonResultUtil.ok(createRouteTree(routeDtoList, 0));
        return null;
    }

    /**
     * 根据id获取用户信息
     *
     * @param userDto
     * @return
     */
    public List<RouteDto> getRouterList(UserDto userDto) {
        //获取用户
        User user = userDao.getById(userDto.getId());
        //获取角色ids
        List<Role> roleList = roleDao.getRoleByUserId(userDto.getId());
        StringBuffer stringBuffer = new StringBuffer(50);
        roleList.stream().forEach(tmp -> {
            stringBuffer.append(String.valueOf(tmp.getId()));
            stringBuffer.append("@");
        });
        BeanUtils.copyProperties(user, userDto);
        userDto.setRoleIds(stringBuffer.toString());
        //根据角色获取资源
        List<Integer> roleIds = roleList.stream().map(Role::getId).collect(Collectors.toList());
        List<Resource> resourceList = resourceDao.listByRoles(roleIds);
        List<RouteDto> routeDtoList = new ArrayList<>();
        for(Resource tmp: resourceList){
            RouteDto routeDto = new RouteDto();
            if(tmp.getComponent() != null) {
                //路由信息
                routeDto.setId(tmp.getId());
                routeDto.setParentId(tmp.getParentId());
                routeDto.setName(tmp.getResourceCode());
                routeDto.setPath(tmp.getPath());
                routeDto.setComponent(tmp.getComponent());
                routeDto.setRedirect(tmp.getRedirectPath());
                routeDto.setHidden(tmp.getIsShow() == 1);
                routeDto.setTitle(tmp.getResourceName());
                routeDto.setIcon(tmp.getIcon());
                routeDto.setPermission(tmp.getResourceCode());
                routeDtoList.add(routeDto);
            }
        }
        List<RouteDto> retList  = createRouteTree(routeDtoList, 0);
        routeMenuTree(retList);
        return retList;
    }

    private List<RouteDto> createRouteTree(List<RouteDto> list, int parentId) {
        List<RouteDto> listChildren = new ArrayList<>();
        for (RouteDto tmp : list) {
            if (tmp.getParentId() == parentId) {
                listChildren.add(tmp);
                tmp.setChildren(createRouteTree(list, tmp.getId()));
            }
        }
        return listChildren;
    }

    /**
     * 设置菜单，目前只设置了3级
     * @param list
     */
    private void routeMenuTree(List<RouteDto> list) {
        for(RouteDto tmp : list){
            if(tmp.getParentId() == 0){
                List<RouteDto> children = tmp.getChildren();
                if(CollectionUtils.isEmpty(children)){
                    tmp.setHidden(true);
                } else {
                    boolean hidden = true;
                    int showCount = 0;
                    for(RouteDto child: children) {
                        if(!child.getHidden()){
                            List<RouteDto> subChildren = child.getChildren();
                            for(RouteDto subChild: subChildren){
                                if(!subChild.getHidden()) {
                                    hidden = false;
                                    showCount ++;
                                }
                            }
                            if(hidden) {
                                child.setHidden(true);
                            }
                        }
                    }
                    if(!hidden){
                        if(showCount == 1){
                            tmp.setAlwaysShow(true);
                        }
                    }
                    tmp.setHidden(hidden);
                }
            }
        }
    }

    /**
     * 获取验证码
     * @param requestMap
     * @return
     */
    public JsonResult getValidateCode(Map<String, String> requestMap) {
        String mobile = requestMap.get("mobile");
        if(Validator.isMobile(mobile)){
            String randomCode = RandomUtil.randomNumbers(4);
            String appid = "asnsgi3v4ky8v";
            Map<String, String> params = new HashMap<>();
            String type = requestMap.get("type");
            if ("1".equals(type)){
                params.put("template", " huo yun信息统一登录平台用户注册验证码："+randomCode+"，5分钟有效，请勿泄露。");
            } else {
                params.put("template", " huo yun信息统一登录平台忘记密码验证码："+randomCode+"，5分钟有效，请勿泄露。");
            }

            params.put("mobile", mobile);
            String ret = HttpUtil.postJson("http://10.3.70.81:8080/sms/common", JsonUtil.toStr(params), appid);
            if(JsonUtil.toMap(ret).get("returnCode").equals(GlobalReturnCode.OK.getReturnCode())){
                redisTemplate.delete(mobile);
                ValueOperations operations = redisTemplate.opsForValue();
                operations.set(mobile, randomCode);
                return JsonResultUtil.ok();
            } else {
                return JsonResultUtil.error(JsonUtil.toMap(ret).get("msg") + ":"+ JsonUtil.toMap(ret).get("data"));
            }
        } else {
            return JsonResultUtil.error("手机号有错误");
        }
    }

    /**
     * 密码找回第一步
     * @param requestMap
     * @return
     */
    public JsonResult retrieveOne(Map<String, String> requestMap) {
        String mobile = requestMap.get("mobile");
        String code = requestMap.get("code");
        if(Validator.isMobile(mobile)){
            ValueOperations operations = redisTemplate.opsForValue();
            Object obj = operations.get(mobile);
            if(obj == null){
                return JsonResultUtil.error("验证码已过期");
            }
            if(code.equalsIgnoreCase(obj.toString())){
                redisTemplate.delete(mobile);
                return JsonResultUtil.ok();
            } else {
                return JsonResultUtil.error("验证码不正确");
            }
        }
        return JsonResultUtil.error("手机号有错误");
    }

    /**
     * 密码找回第二步
     * @param requestMap
     * @return
     */
    public JsonResult retrieveTwo(Map<String, String> requestMap) {
        String mobile = requestMap.get("mobile");
        String password = requestMap.get("password");
        String rePassword = requestMap.get("repassword");
        if(Validator.isMobile(mobile)){
            if (!password.equals(rePassword)) {
                return JsonResultUtil.error("两次输入密码不一致！");
            }
            UserLogin userLogin = loginDao.getUserLoginByMobile(mobile);
            if(userLogin ==null){
                return JsonResultUtil.error("手机号码不存在！");
            }
            String pwd = new BCryptPasswordEncoder(10).encode(password);
            userLogin.setPassword(pwd);
            userLogin.setChangePasswordTime(new Date());
            userLogin.setErrorTimes(0);
            loginDao.updatePwd(userLogin);
            return JsonResultUtil.ok();
        }
        return JsonResultUtil.error("手机号有错误");
    }

    public JsonResult findUser(Map<String, String> requestMap) {
        UserSearchVO userSearchVO = JsonUtil.toObject(JsonUtil.toStr(requestMap), UserSearchVO.class);
        List<User> list = userDao.findUser(userSearchVO);
        List<UserDto> listResult = ListUtils.copyList(list, UserDto.class);
        return JsonResultUtil.ok(listResult);
    }



    /**
     * 重置用户密码
     * @param id
     * @return
     */
    public JsonResult resetPwd(Integer id) {
        UserLogin userLogin = loginDao.getUserLogin(id);
        String password = new BCryptPasswordEncoder(10).encode(UserService.DEFAULT_PASSWORD);
        userLogin.setPassword(password);
        //重置密码修改时间，登录需要重新设置密码
        userLogin.setChangePasswordTime(null);
        //重置登录错误次数
        userLogin.setErrorTimes(0);
        int flag = loginDao.updatePwd(userLogin);
        if (flag == 0) {
            return JsonResultUtil.error("重置密码失败");
        } else {
            return JsonResultUtil.ok();
        }
    }

    /**
     * 审核用户
     * @param id
     * @return
     */
    public JsonResult reviewUser(Integer id, Integer status){
        UserLogin userLogin = loginDao.getUserLogin(id);
        userLogin.setAuditStatus(status);
        int flag = loginDao.updateStatus(userLogin);
        if (flag == 0) {
            return JsonResultUtil.error("审核失败");
        } else {
            return JsonResultUtil.ok();
        }
    }

    public JsonResult getHy(UserDto userDto) {
        //获取角色ids
        List<Role> roleList = roleDao.getRoleByUserId(userDto.getId());
        //根据角色获取资源
        List<Integer> roleIds = roleList.stream().map(Role::getId).collect(Collectors.toList());
        List<Resource> resourceList = resourceDao.listByRoles(roleIds);
        List<HyRouteDto> menuList = getRouterList(resourceList);
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("List", createHyRouteTree(menuList, 0));
        //权限
        List<String> resourceCodes = resourceList.stream().map(Resource::getResourceCode).collect(Collectors.toList());
        retMap.put("resourceCodes",resourceCodes);
        return JsonResultUtil.ok(retMap);
    }

    public List<HyRouteDto> getRouterList(List<Resource> resourceList) {
        List<HyRouteDto> dtoList = new ArrayList<>();
        for(Resource res: resourceList){
            if(res.getPath() != null){
                HyRouteDto hy = new HyRouteDto();
                hy.setId(res.getId());
                hy.setParentId(res.getParentId());
                hy.setTitle(res.getResourceName());
                hy.setPath(res.getPath());
                dtoList.add(hy);
            }
        }
        return dtoList;
    }


    private List<HyRouteDto> createHyRouteTree(List<HyRouteDto> list, int parentId) {
        List<HyRouteDto> listChildren = new ArrayList<>();
        for (HyRouteDto tmp : list) {
            if (tmp.getParentId() == parentId) {
                listChildren.add(tmp);
                tmp.setChildren(createHyRouteTree(list, tmp.getId()));
            }
        }
        return listChildren;
    }

    /**
     * 验证用户名密码
     * @param requestMap
     * @return
     */
    public JsonResult checkUser(Map<String, String> requestMap) {
        //用户名
        String userName = requestMap.get("userName");
        //加密后密码
        String code = requestMap.get("code");
        //密码加密盐
        String random = requestMap.get("random");
        if(StringUtil.isNullOrEmpty(userName)){
            return JsonResultUtil.error("业务系统账号不能为空");
        }
        if(StringUtil.isNullOrEmpty(code)){
            return JsonResultUtil.error("业务系统密码不能为空");
        }
        if(StringUtil.isNullOrEmpty(random)){
            return JsonResultUtil.error("加密随机数不能为空");
        }
        String pwd = Encryp.de(code,random);
        UserLogin userLogin = loginDao.getUserLogin(userName);
        if(userLogin==null){
            return JsonResultUtil.error("用户不存在");
        }
        //校验用户密码
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10);
        if (!bCryptPasswordEncoder.matches(pwd, userLogin.getPassword())) {
            return JsonResultUtil.error("用户名或密码错误！");
        }
        return JsonResultUtil.ok();
    }
}
