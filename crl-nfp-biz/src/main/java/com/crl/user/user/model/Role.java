package com.crl.user.user.model;

import com.cars.util.bean.BaseModel;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 用户角色对象
 *
 * @author sunchao
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel("角色基本信息")
public class Role extends BaseModel {
    /**
     * 角色id
     */
    private Integer id;
    /**
     * 角色代码
     */
    private String roleCode;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色描述
     */
    private String roleDesc;
    /**
     * 角色标识
     */
    private String roleType;
    /**
     * 所属授权系统
     */
    private Integer authSystemId;
}
