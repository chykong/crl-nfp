package com.crl.user.user.model;


import lombok.Data;

/**
 * @Description:部门负责人
 * @Date:2020/4/14 16:51
 * @Author:苗凯旋
 */
@Data
public class DepUser {
    /**
     * 部门id
     */
    private Integer departmentId;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 接入系统id
     */
    private Integer authId;
}
