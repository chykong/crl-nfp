package com.crl.user.user.model;


import com.cars.util.bean.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Department extends BaseModel {
    /**
     * 部门id
     */
    private Integer id;
    /**
     * 部门代码
     */
    private String depCode;
    /**
     * 修改前部门代码，修改时用
     */
    private String oldDepCode;
    /**
     * 部门名称
     */
    private String depName;
    /**
     * 父节点id
     */
    private Integer parentId;
    /**
     * 负责人id
     */
    private Integer userId;
    /**
     * 负责人名
     */
    private String realName;
    /**
     * 路局代码
     */
    private String bureauCode;
    /**
     * 部门描述
     */
    private String disc;

    private String depShort;//简称
    private Integer depOrder;//排序
    private Boolean isLeaf; //是否叶子节点

    public String getOldDepCodeStr(){
        return this.oldDepCode+"%";
    }
}
