package com.crl.user.user.dto;

import com.cars.util.bean.BaseModel;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 接收增加和修改数据传数对象
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel("角色传输对象")
public class RoleDto extends BaseModel {
    /**
     * 角色id
     */
    @ApiModelProperty("角色id")
    @NotNull(groups = {UpdateValidGroup.class, DeleteValidGroup.class})
    private Integer id;
    /**
     * 角色代码
     */
    @ApiModelProperty(value = "角色代码", required = true)
    @NotBlank(groups = {UpdateValidGroup.class, AddValidGroup.class})
    private String roleCode;
    /**
     * 角色名称
     */
    @ApiModelProperty("角色名称")
    @NotBlank(groups = {UpdateValidGroup.class, AddValidGroup.class})
    private String roleName;
    /**
     * 角色描述
     */
    @ApiModelProperty("角色描述")
    private String roleDesc;
    /**
     * 角色对应的资源id
     */
    @ApiModelProperty("角色对应的资源id,每个id用@分隔")
    private String resourceIds;

    /**
     * 角色标识 0管理员角色 1普通角色
     */
    @ApiModelProperty("角色标识")
    private String roleType;
    /**
     * 所属授权系统
     */
    private Integer authSystemId;
}
