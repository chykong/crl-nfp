package com.crl.user.user.service;

import cn.hutool.core.bean.BeanUtil;
import com.cars.util.bean.BasePage;
import com.cars.util.code.GlobalConst;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.list.ListUtils;
import com.crl.user.user.dao.AuthSystemDao;
import com.crl.user.user.dao.UserDao;
import com.crl.user.user.dto.AuthSystemDto;
import com.crl.user.user.model.AuthSystem;
import com.crl.user.user.model.User;
import com.crl.user.user.vo.AuthSystemVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Desc:
 * @author: miaokaixuan
 * @Date: 2020/2/20
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AuthSystemService {
    @Autowired
    private AuthSystemDao authSystemDao;
    @Autowired
    private UserDao userDao;

    /**
     * 验证资源代码是否重复
     */
    public JsonResult checkSystemName(String systemName, Integer systemId) {
        AuthSystem authSystem = authSystemDao.checkSystemName(systemName);
        if (authSystem != null) {
            if(systemId == null || authSystem.getId().intValue() != systemId) {
                return JsonResultUtil.error("系统名称重复");
            }
        }
        return JsonResultUtil.ok();
    }

    /**
     * 新增
     */
    public JsonResult saveAuthSystem(AuthSystemDto authSystemDto) {
        Integer userId = authSystemDto.getUserLoginId();
        User user = userDao.getById(userId);
        if(user!=null){
            authSystemDto.setCreatedBy(user.getRealName());
        }
        AuthSystem authSystem = new AuthSystem();
        BeanUtil.copyProperties(authSystemDto, authSystem);
        JsonResult jsonResult = checkSystemName(authSystem.getSystemName(), null);
        if (jsonResult.success()) {
            int flag = authSystemDao.save(authSystem);
            if (flag > 0) {
                return JsonResultUtil.ok();
            } else {
                return JsonResultUtil.error("系统新增失败");
            }
        } else {
            return jsonResult;
        }
    }

    /**
     * 根据id修改
     */
    public JsonResult updateAuthSystem(AuthSystemDto authSystemDto) {
        Integer userId = authSystemDto.getUserLoginId();
        User user = userDao.getById(userId);
        if(user!=null){
            authSystemDto.setModifiedBy(user.getRealName());
        }
        AuthSystem authSystem = new AuthSystem();
        BeanUtil.copyProperties(authSystemDto, authSystem);
        JsonResult jsonResult = checkSystemName(authSystem.getSystemName(), authSystem.getId());
        if (jsonResult.success()) {
            int flag = authSystemDao.updateAuthSystem(authSystem);
            if (flag > 0) {
                return JsonResultUtil.ok();
            } else {
                return JsonResultUtil.error("系统修改失败");
            }
        } else {
            return jsonResult;
        }


    }

    /**
     * 删除
     */
    public JsonResult deleteAuthSystem(AuthSystemDto AuthSystemDto) {
        int flag = authSystemDao.deleteAuthSystem(AuthSystemDto.getId());
        if (flag > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error();
        }
    }

    /**
     * 获取条件查询角色
     */
    public JsonResult<BasePage<List<AuthSystemDto>>> listSearch(AuthSystemVO authSystemVO) {
        List<AuthSystem> authSystemList = authSystemDao.listAuthSystem(authSystemVO);
        int count = authSystemDao.count(authSystemVO);
        BasePage<List<AuthSystemDto>> basePage  = new BasePage<>();
        List<AuthSystemDto> AuthSystemDtoList = ListUtils.copyList(authSystemList,AuthSystemDto.class);
        basePage.setTotal(count);
        basePage.setData(AuthSystemDtoList);
        return JsonResultUtil.ok(basePage);
    }

    /**
     * 查询所有的系统信息
     * @return
     * @param map
     */
    public JsonResult listAllSystem(Map<String, String> map) {
        Integer userId = Integer.parseInt(map.get("userId"));
        AuthSystem authSystem = new AuthSystem();
        List<AuthSystem> list = new ArrayList<>();
        if(userId!= GlobalConst.ADMIN_USER_ID){
            User user = userDao.getById(userId);
            AuthSystem system = authSystemDao.getById(user.getAuthSystemId());
            list.add(system);
        }else{
            List<AuthSystem> authSystemList = authSystemDao.listAuthSystems();
            list.addAll(authSystemList);
        }

        return JsonResultUtil.ok(list);
    }

    /**
     * 根据id获取信息
     * @param authSystemDto
     * @return
     */
    public JsonResult<AuthSystemDto> getById(AuthSystemDto authSystemDto) {
        AuthSystem authSystem = authSystemDao.getById(authSystemDto.getId());
        AuthSystemDto dto = new AuthSystemDto();
        BeanUtil.copyProperties(authSystem, dto);
        return JsonResultUtil.ok(dto);
    }
}
