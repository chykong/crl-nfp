package com.crl.user.user.vo;


import com.cars.util.bean.BasePageSearchVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 用户模块查询交互对象
 *
 * @author sunchao
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "用户模块查询交互对象")
public class UserSearchVO extends BasePageSearchVO {
    @ApiModelProperty(value = "用户名")
    private String username;
    @ApiModelProperty(value = "真实姓名")
    private String realName;
    @ApiModelProperty(value = "部门id")
    private Integer departmentId;
    @ApiModelProperty(value = "登录人id")
    private String userId;
    @ApiModelProperty(value = "部门code")
    private String depCode;
    /**
     * 部门负责人绑定用户查询回显用
     */
    private String deptid;
    public String getDepCodeStr(){
        return this.depCode+"%";
    }
    /**
     * 所属授权系统
     */
    private Integer authSystemId;
}
