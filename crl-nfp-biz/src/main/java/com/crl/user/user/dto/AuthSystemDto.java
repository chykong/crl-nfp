package com.crl.user.user.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

/**
 * @Desc:
 * @author: miaokaixuan
 * @Date: 2020/2/20
 */
@Data
@ApiModel("系统信息实体类")
public class AuthSystemDto {
    /**
     * id
     */
    private Integer id;
    /**
     *系统名称
     */
    private String systemName;
    /**
     *系统负责人
     */
    private String principal;
    /**
     *创建人
     */
    private String createdBy;
    /**
     *系统描述
     */
    private String systemDesc;
    /**
     *创建时间
     */
    private Date createdAt;
    /**
     *修改人
     */
    private String modifiedBy;
    /**
     *修改时间
     */
    private Date modifiedAt;
    /**
     * 当前登录人id
     */
    private Integer userLoginId;
}
