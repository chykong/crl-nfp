package com.crl.user.user.dao;


import com.crl.user.user.model.UserLogin;
import com.crl.dao.BaseDao;
import org.springframework.stereotype.Repository;

@Repository
public class RegisterDao extends BaseDao<UserLogin, UserLogin> {
    /**
     * 注册用户，向crl_base_USERLOGIN中添加记录
     */
    public int register(UserLogin userLogin) {
        String sql = "INSERT INTO crl_base_userlogin(user_id,username,mobile,password,status,audit_status)" +
                " VALUES(:userId,:username,:mobile,:password,0,0)";
        return insert(sql, userLogin);
    }
}
