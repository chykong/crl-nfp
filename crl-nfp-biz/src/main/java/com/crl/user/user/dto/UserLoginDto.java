package com.crl.user.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("登录信息传输对象")
public class UserLoginDto {
    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    @NotBlank(message = "用户名不能为空")
    @Length(max = 30, min = 5)
    private String username;
    /**
     * 密码
     */
    @ApiModelProperty("用户名")
    @NotBlank(message = "密码不能为空")
    @Length(min=1)
    private String password;
    /**
     * 验证码
     */
    @ApiModelProperty("验证码")
    @Length(min=1)
    private String code;

    @ApiModelProperty("系统标识")
    @NotBlank(message = "系统标识不能为空")
    private String systemFlag;

    private Integer flag;
}
