package com.crl.user.user.dao;

import com.crl.user.user.model.Resource;
import com.crl.dao.BaseDao;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ResourceDao extends BaseDao<Resource, Resource> {
    /**
     * 增加资源权限
     */
    public int save(Resource resource) {
        String sql = "INSERT INTO crl_base_resource(id,parent_id,auth_system_id,resource_name,resource_type,resource_code,display_order,uri,target,icon,is_show,redirect_path,keep_alive,status,resource_desc,component,path) " +
                "VALUES(seq_crl_base_resource.nextval,:parentId,:authSystemId,:resourceName,:resourceType,:resourceCode,:displayOrder,:uri,:target,:icon,:isShow,:redirectPath,:keepAlive,0,:resourceDesc,:component,:path)";
        return insert(sql, resource);
    }

    /**
     * 权限代码验重
     */
    public Resource checkCode(String resourceCode) {
        String sql = "SELECT id,resource_code FROM crl_base_resource WHERE resource_code=?";
        return get(sql, resourceCode);
    }

    /**
     * 根据id修改
     */
    public int updateResource(Resource resource) {
        String sql = "UPDATE crl_base_resource SET " +
                "parent_id=:parentId," +
                "resource_name=:resourceName," +
                "resource_type=:resourceType," +
                "resource_code=:resourceCode," +
                "display_order=:displayOrder," +
                "uri=:uri," +
                "target=:target," +
                "icon=:icon," +
                "is_show=:isShow," +
                "redirect_path=:redirectPath," +
                "keep_alive=:keepAlive," +
                "resource_desc=:resourceDesc," +
                "component=:component," +
                "path=:path" +
                " WHERE id=:id";
        return update(sql, resource);
    }

    /**
     * 根据id删除资源
     */
    public int deleteResource(Integer id) {
        String sql = "DELETE FROM crl_base_resource WHERE id=?";
        return delete(sql, id);
    }


    /**
     * 根据id修改状态
     */
    public int updateStatus(Integer id) {
        String sql = "UPDATE crl_base_resource SET status=((status+1) -BITAND(status,1)*2) WHERE id=?";
        return update(sql, id);
    }

    /**
     * 获取所有资源列表
     * @param authSystemId
     */
    public List<Resource> listResources(Integer authSystemId) {
        String sql = "SELECT id,parent_id,resource_name,resource_type,resource_code,display_order,uri,target,icon,is_show,redirect_path,keep_alive,status,resource_desc,component,path FROM crl_base_resource WHERE status=0 and auth_system_id=? ORDER BY parent_id,display_order";
        return list(sql,authSystemId);
    }

    /**
     * 根据id获取资源
     */
    public Resource getById(Integer resourceId) {
        String sql = "SELECT id,parent_id,resource_name,auth_system_id,resource_type,resource_code,display_order,uri,target,icon,is_show,redirect_path,keep_alive,status,resource_desc,component,path FROM crl_base_resource WHERE id=?";
        return get(sql, resourceId);
    }

    /**
     * 根据资源代码获取资源
     */
    public Resource getByCode(String resourceCode) {
        String sql = "SELECT id,parent_id,resource_name,auth_system_id,resource_type,resource_code,display_order,uri,target,icon,is_show,redirect_path,keep_alive,status,resource_desc,component,path FROM crl_base_resource WHERE resource_code=?";
        return get(sql, resourceCode);
    }

    /**
     * 根据角色id列表获取资源信息
     */
    public List<Resource> listByRoles(List<Integer> roleIds) {
        Map<String, Object> map = new HashMap<>();
        map.put("roleIds", roleIds);
        String sql = "SELECT id,parent_id,resource_name,auth_system_id,resource_type,resource_code,display_order,uri,target,icon,is_show,redirect_path,keep_alive,status,resource_desc,component,path FROM crl_base_resource WHERE id IN (SELECT resource_id FROM crl_base_role_resource WHERE role_id IN (:roleIds)) ";
        sql += " order by parent_id, display_order ";
        return getNamedParameterJdbcTemplate().query(sql, map, BeanPropertyRowMapper.newInstance(Resource.class));
    }

    /**
     * 根据id获取下级节点
     */
    public int listChildren(Integer parentId) {
        String sql = "SELECT COUNT(id) FROM crl_base_resource WHERE parent_id=?";
        return count(sql, parentId);
    }
}
