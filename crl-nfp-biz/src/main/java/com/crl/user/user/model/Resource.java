package com.crl.user.user.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * base_resource资源表，从数据库查出来时接收数据的对象
 *
 * @author sunchao
 */
@Data
@ApiModel("资源权限实体类")
public class Resource {
    /**
     * 主键，SEQ_HGP_BASE_RESOURCE
     */
    private Integer id;
    /**
     * 自关联上级资源
     */
    private Integer parentId;
    /**
     * 资源名称
     */
    private String resourceName;
    /**
     * 资源类别0模块，1功能
     */
    private Integer resourceType;
    /**
     * 资源代码，唯一，校验权限
     */
    private String resourceCode;
    /**
     * 资源排序
     */
    private Integer displayOrder;
    /**
     * 资源路径
     */
    private String uri;
    /**
     * 目标
     */
    private String target;
    /**
     * 图标
     */
    private String icon;
    /**
     * 是否显示0显示，1不显示
     */
    private Integer isShow;
    /**
     * 重定向地址，父级菜单默认地址（VUE）
     */
    private String redirectPath;
    /**
     * 是否缓存0缓存，1不缓存
     */
    private Integer keepAlive;
    /**
     * 状态0正常，1删除，2锁定
     */
    private Integer status;
    /**
     * 资源描述
     */
    private String resourceDesc;

    /**
     * 组件
     */
    private String component;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 所属授权系统
     */
    private Integer authSystemId;
}
