package com.crl.user.user.service;

import com.cars.util.code.GlobalConst;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.list.ListUtils;
import com.cars.util.string.StringUtil;
import com.crl.user.user.dao.DepUserDao;
import com.crl.user.user.dao.DepartmentDao;
import com.crl.user.user.dao.RoleDao;
import com.crl.user.user.dao.UserDao;
import com.crl.user.user.dto.DepUserDto;
import com.crl.user.user.dto.DepartmentDto;
import com.crl.user.user.model.*;
import io.netty.util.Constant;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 部门操作
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DepartmentService {
    @Autowired
    private DepartmentDao departmentDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private DepUserDao depUserDao;
    /**
     * 新增
     */
    public JsonResult save(DepartmentDto departmentDto) {
        Department department = departmentDao.getByCode(departmentDto.getDepCode());
        if (department != null) {
            return JsonResultUtil.error("部门代码重复");
        }
        if (null == departmentDto.getParentId()) {
           return JsonResultUtil.error("上级部门不能为空");
        }
        department = new Department();
        BeanUtils.copyProperties(departmentDto, department);
        int flag = departmentDao.saveDepartment(department);
        if (flag > 0) {
            Integer userId = departmentDto.getUserId();
            if (userId != null) {
                //分配默认角色
                Role role = roleDao.getByCode(UserService.DEPARTMENT_MANAGER);
                List<UserRole> userRoles = new ArrayList<>();
                UserRole userRole = new UserRole();
                userRole.setRoleId(Integer.valueOf(role.getId()));
                userRole.setUserId(userId);
                userRoles.add(userRole);
                userDao.batchRoleId(userRoles);
            }
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("新增部门失败");
        }
    }

    /**
     * 删除
     */
    public JsonResult delete(DepartmentDto departmentDto) {
        List<Department> list = departmentDao.listByParentId(departmentDto.getId());
        if (list.size() > 0) {
            return JsonResultUtil.error("存在下级节点，不可以删除");
        }
        Integer userCount = userDao.countByDepartmentId(departmentDto.getId());
        if(userCount>0){
            return JsonResultUtil.error("部门下存在用户，不可以删除");
        }
        int flag = departmentDao.deleteDepartment(departmentDto.getId());
        if (flag > 0) {
            Department department = new Department();
            department.setUserId(departmentDto.getUserId());
            List<Department> departmentList = departmentDao.listByUserId(department);
            if (CollectionUtils.isEmpty(departmentList)) {
                // 如果没有负责的部门，删除部门负责人角色
                Role role = roleDao.getByCode(UserService.DEPARTMENT_MANAGER);
                userDao.deleteByUserAndRole(departmentDto.getUserId(), role.getId());
            }
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("删除部门失败");
        }
    }

    /**
     * 获取父节点到跟节点全名
     */
    public String getAllNameByDepid(Integer depId){
        List<Department> priorList = departmentDao.reversalPrior(depId);
        // 按自定义排序..部门编号排序
        priorList = priorList.stream().sorted(Comparator.comparing(Department:: getDepCode)).collect(Collectors.toList());
        String parentName = "";
        for(Department d : priorList){
            if(null == d.getParentId()){
                continue;
            }
            parentName += d.getDepName()+"/";
        }
        if(StringUtil.isNotNullOrEmpty(parentName)){
            return parentName.substring(0, parentName.length()-1);
        }
        return parentName;
    }

    /**
     * 根据id获取
     */
    public JsonResult getById(DepartmentDto departmentDto) {
        Department department = departmentDao.getById(departmentDto.getId());
        BeanUtils.copyProperties(department, departmentDto);
        departmentDto.setAllParentName(getAllNameByDepid(department.getParentId()));
        return JsonResultUtil.ok(departmentDto);
    }

    /**
     * 根据代码获取代码是否存在
     */
    public JsonResult checkCode(Map<String, String> requestMap) {
        if (StringUtil.isNullOrEmpty(requestMap.get("depCode"))) {
            return JsonResultUtil.error("部门代码不能为空");
        }
        Department department = departmentDao.getByCode(requestMap.get("depCode"));
        if (department == null) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("部门代码重复");
        }
    }

    /**
     * 根据代码获取代码是否存在
     */
    public JsonResult checkName(DepartmentDto departmentDto) {
        Integer parentId = departmentDto.getParentId();
        String deptName = departmentDto.getDepName();

        if (parentId != null && parentId.intValue() != 0 && StringUtil.isNotNullOrEmpty(deptName)) {
            Department department = departmentDao.getByName(parentId, deptName);
            if (department == null) {
                return JsonResultUtil.ok();
            } else {
                return JsonResultUtil.error("该级部门名称已存在");
            }
        }
        return JsonResultUtil.ok();
    }

    /**
     * 修改
     */
    public JsonResult update(DepartmentDto departmentDto) {
        Department d = departmentDao.getById(departmentDto.getId());
        //父机构修改，需要修改部门编码
        if(!departmentDto.getParentId().equals(d.getParentId())){
            departmentDto.setDepCode(generateDepCode(departmentDto.getParentId()));
        }
        Department department = new Department();
        BeanUtils.copyProperties(departmentDto, department);
        int flag = departmentDao.updateById(department);
        if (flag > 0) {
            //根据旧部门编码修改当前部门及下所有部门的部门编码
            departmentDao.updateChiledCode(department);
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("更新部门失败");
        }
    }

    /**
     * 修改负责人
     */
    public JsonResult updateUserid(DepUserDto depUserDto) {
        DepUser oldUser = depUserDao.getUserId(depUserDto.getDepartmentId(),depUserDto.getAuthId());
        DepUser depUser = new DepUser();
        BeanUtils.copyProperties(depUserDto, depUser);
        int flag = 0;
        if(oldUser == null){
            flag = depUserDao.add(depUser);
        } else {
            flag = depUserDao.updateUserId(depUser);
        }

        if (flag > 0) {
            Role role = roleDao.getByCode(UserService.DEPARTMENT_MANAGER);
            flag = userDao.countUserRole(depUserDto.getUserId(),role.getId());
            if (flag == 0){
                UserRole userRole = new UserRole();
                List<UserRole> userRoleList = new ArrayList<>();
                userRole.setRoleId(role.getId());
                userRole.setUserId(depUserDto.getUserId());
                userRoleList.add(userRole);
                userDao.batchRoleId(userRoleList);
            }
            if (oldUser!= null){
                Integer oldUserId = oldUser.getUserId();
                DepUser de = new DepUser();
                de.setUserId(oldUserId);
                List<DepUser> depUserList = depUserDao.listByUserId(de);
                if (CollectionUtils.isEmpty(depUserList)) {
                    // 如果没有负责的部门，删除部门负责人角色
                    userDao.deleteByUserAndRole(oldUserId, role.getId());
                }
            }

            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("更新部门负责人失败");
        }
    }

    public String generateDepCode(Integer parentId){
        //查询所有直系父级
        List<Department> parentList = departmentDao.reversalPrior(parentId);
        StringBuffer depCode = new StringBuffer();
        String codeNum = departmentDao.maxDepCode(parentId);
        String[] nums = codeNum.split("\\.");
        int num = Integer.parseInt(nums[nums.length-1]);
        if (parentId == 0) {
            if(Integer.toString(num+1).length()==1){
                depCode.append("0");
            }
            depCode.append(++num);
        } else {
            Department department = departmentDao.getById(parentId);
            depCode.append(department.getDepCode()+".");
            String[] format = department.getDepCode().split("\\.");
            int count = format[format.length-1].length()+1;
            depCode.append(String.format("%0"+count+"d",num+1));
        }
        return depCode.toString();
    }

    /**
     * 根据parentId获取部门编码
     */
    public JsonResult getDepCode(DepartmentDto departmentDto) {
        Integer parentId = departmentDto.getParentId();
        return JsonResultUtil.ok(generateDepCode(parentId));
    }

    /**
     * 获取所有
     */
    public JsonResult<List<DepartmentDto>> tree(Map<String, String> requestMap) {
        Department de = new Department();
        List<Department> list = departmentDao.listAll(de);
        List<DepartmentDto> resultLis = ListUtils.copyList(list, DepartmentDto.class);
        return JsonResultUtil.ok(createDepartmentTree(resultLis, 0));
    }

    /**
     * 查询用户负责所有父级部门
     * @param requestMap
     * @return
     */
    public JsonResult<List<DepartmentDto>> listParents(Map<String, String> requestMap) {
        Object userObj = requestMap.get("userId");
        List<Department> departmentList = new ArrayList<Department>();
        List<Department> middleList = new ArrayList<Department>();
        Integer userId = null;
        Department de = new Department();
        if (userObj != null) {
            de.setUserId(userId);
            userId = Integer.parseInt(userObj.toString());
            // 查询当前用户的所有负责部门及下层所有部门
            de.setUserId(userId);
        }
        departmentList = departmentDao.listAllByUserId(de);
        // 筛除所有不是父级的部门 --用于异步加载
        for(Department d : departmentList){
            boolean flag = true;
            String allCode = d.getDepCode()+".";
            for(Department dd : middleList){
                String code = dd.getDepCode();
                if(allCode.indexOf(code) > -1){
                    flag = false;
                    break;
                }
            }
            if(flag && null != d.getParentId()){
                middleList.add(d);
            }
        }
        List<DepartmentDto> resultList = ListUtils.copyList(middleList, DepartmentDto.class);
        // 按自定义排序..部门编号排序
        resultList = resultList.stream().sorted(Comparator.comparing(DepartmentDto :: getDepOrder)).collect(Collectors.toList());
        return JsonResultUtil.ok(resultList);
    }

    /**
     * 根据父级ID查询子部门
     * @param parentId
     * @return
     */
    public JsonResult<List<DepartmentDto>> listChildren(Integer parentId){
        List<Department> list = departmentDao.listByParentId(parentId);
        List<DepartmentDto> resultLis = ListUtils.copyList(list, DepartmentDto.class);
        return JsonResultUtil.ok(resultLis);
    }

    /**
     * 获取所有
     */
    public JsonResult<List<DepartmentDto>> listAll(Map<String, String> requestMap) {
        Object userObj = requestMap.get("userId");
        List<Department> list = new ArrayList<Department>();
        Integer userId = null;
        if (userObj != null) {
            Department de = new Department();
            de.setUserId(userId);

            userId = Integer.parseInt(userObj.toString());
            // 查询当前用户的所有负责部门及下层部门
            de.setUserId(userId);
            if(userId == GlobalConst.ADMIN_USER_ID){
                list = departmentDao.listAllByUserId(de);
            } else {
                User user = userDao.getById(userId);
                list = departmentDao.userList(userId,user.getAuthSystemId());
            }

        }
        List<Department> miidleList = new ArrayList<Department>();
        // 筛除所有不是父级的部门
        for(Department d : list){
            boolean flag = true;
            String allCode = d.getDepCode()+".";
            for(Department dd : miidleList){
                String code = dd.getDepCode();
                if(allCode.indexOf(code) > -1){
                    flag = false;
                    break;
                }
            }
            if(flag){
                miidleList.add(d);
            }
        }
        List<DepartmentDto> resultLis = ListUtils.copyList(miidleList, DepartmentDto.class);
        List<DepartmentDto> resultList = ListUtils.copyList(list, DepartmentDto.class);
        List<Integer> parentIds = new ArrayList<Integer>();
        // 按自定义排序..部门编号排序
        resultList = resultList.stream().sorted(Comparator.comparing(DepartmentDto :: getDepOrder)).collect(Collectors.toList());
        for (DepartmentDto department : resultLis) {
            Integer parentId = department.getParentId();
            // 顶级部门-登录用户为管理员
            if(null == parentId) {
                parentIds.clear();
                parentIds.add(0);
                break;
            }
            // 重复数据校验
            if(!parentIds.contains(parentId)){
                parentIds.add(parentId);
            }
        }
        //树结构组建各parentId下数据
        List<DepartmentDto> listChildren = new ArrayList<>();
        for(Integer parentId : parentIds){
            listChildren.addAll(createDepartmentTree(resultList, parentId));
        }
        return JsonResultUtil.ok(listChildren);
    }

    /**
     * 树结构组建数据
     */
    private List<DepartmentDto> createDepartmentTree(List<DepartmentDto> list, int parentId){
        List<DepartmentDto> listChildren = new ArrayList<>();
        for (DepartmentDto tmp : list) {
            if(null == tmp.getParentId()){
                continue;
            }
            if (tmp.getParentId() == parentId) {
                listChildren.add(tmp);
                tmp.setChildren(createDepartmentTree(list, tmp.getId()));
            }
        }
        return listChildren;
    }
}
