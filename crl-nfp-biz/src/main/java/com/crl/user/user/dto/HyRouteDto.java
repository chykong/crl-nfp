package com.crl.user.user.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 货价运调-路由信息
 */
@Data
@ApiModel("路由信息")
public class HyRouteDto {
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Integer id;
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Integer parentId;
    /**
     * 菜单名字
     */
    @ApiModelProperty("菜单名字")
    private String title;
    /**
     * 地址
     */
    private String path;
    /**
     * 子路由
     */
    @ApiModelProperty("子路由")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<HyRouteDto> children;
}
