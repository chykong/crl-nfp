package com.crl.user.user.model;

import lombok.Data;

/**
 * 用户角色关联表
 *
 * @author sunchao
 */
@Data
public class UserRole {
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 角色id
     */
    private Integer roleId;
}
