package com.crl.user.user.model;

import lombok.Data;

import java.util.Date;

/**
 * hgp_base_userlogin 用户登录信息表
 *
 * @author sunchao
 */
@Data
public class UserLogin {
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 登录密码，密文
     */
    private String password;
    /**
     * 状态 0正常1禁用2待审核3审核不通过
     */
    private Integer status;
    /**
     * 密码错误次数
     */
    private Integer errorTimes;
    /**
     * 上次登录时间
     */
    private Date lastLoginTime;

    /**
     * 审核状态0未审核，1审核通过，2审核未通过
     */
    private Integer auditStatus;
    /**
     * 密码修改时间
     */
    private Date changePasswordTime;

    public static final Integer AUDIT_STATUS_DEFAULT = 0; //0未审核
    public static final Integer AUDIT_STATUS_SUCCESS = 1; //1审核通过
    public static final Integer AUDIT_STATUS_FAILED = 2; //2审核未通过
}
