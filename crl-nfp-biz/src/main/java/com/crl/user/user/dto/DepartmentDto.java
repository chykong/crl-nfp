package com.crl.user.user.dto;


import com.cars.util.bean.BaseModel;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel("部门传输对象")
public class DepartmentDto extends BaseModel {
    /**
     * 部门id
     */
    @ApiModelProperty("部门id")
    @NotNull(groups = {UpdateValidGroup.class, DeleteValidGroup.class})
    private Integer id;
    /**
     * 部门代码
     */
    @NotNull(groups = {AddValidGroup.class, UpdateValidGroup.class})
    @ApiModelProperty("部门代码")
    private String depCode;
    /**
     * 修改前部门代码，修改时用
     */
    private String oldDepCode;
    /**
     * 部门名称
     */
    @ApiModelProperty("部门名称")
    private String depName;
    /**
     * 父节点id
     */
    @ApiModelProperty("父节点id")
    private Integer parentId;
    /**
     * 负责人id
     */
    @ApiModelProperty("负责人id")
    private Integer userId;
    /**
     * 下级节点集合
     */
    @ApiModelProperty("下级节点集合")
    private List<DepartmentDto> children;
    /**
     * 负责人名
     */
    @ApiModelProperty("负责人名")
    private String realName;
    /**
     * 所属路局代码
     */
    @ApiModelProperty("路局代码")
    private String bureauCode;
    /**
     * 部门描述
     */
    private String disc;

    private String depShort;//简称
    private Integer depOrder;//排序
    private Boolean isLeaf; //是否叶子节点
    private String allParentName;//父节点到根节点全名

}
