package com.crl.user.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 个人修改密码
 */
@Data
@ApiModel("个人修改密码")
public class UpdatePassDto {
    /**
     * 部门id
     */
    @ApiModelProperty("用户id")
    @NotNull
    private Integer id;
    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    @NotBlank
    private String username;
    /**
     * 原密码
     */
    @ApiModelProperty("原密码")
    @NotBlank
    private String oldPassword;

    /**
     * 新密码
     */
    @ApiModelProperty("新密码")
    @NotBlank
    private String password;
    /**
     * 确认的新密码
     */
    @ApiModelProperty("确认的新密码")
    @NotBlank
    private String rePassword;
}
