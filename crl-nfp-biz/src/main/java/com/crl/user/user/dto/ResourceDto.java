package com.crl.user.user.dto;


import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel("资源传输实体类")
public class ResourceDto {
    /**
     * 主键，SEQ_HGP_BASE_RESOURCE
     */
    @NotNull(groups = {UpdateValidGroup.class, DeleteValidGroup.class})
    private Integer id;
    /**
     * 自关联上级资源
     */
    private Integer parentId;
    /**
     * 资源名称
     */
    @NotBlank(groups = {UpdateValidGroup.class, AddValidGroup.class})
    private String resourceName;
    /**
     * 资源类别0模块，1功能
     */
    @NotNull(groups = {UpdateValidGroup.class, AddValidGroup.class})
    private Integer resourceType;
    /**
     * 资源代码，唯一，校验权限
     */
    @NotBlank(groups = {UpdateValidGroup.class, AddValidGroup.class})
    private String resourceCode;
    /**
     * 资源排序
     */
    private Integer displayOrder;
    /**
     * 资源路径
     */
    @NotBlank(groups = {UpdateValidGroup.class, AddValidGroup.class})
    private String uri;
    /**
     * 目标
     */
    private String target;
    /**
     * 图标
     */
    private String icon;
    /**
     * 是否显示0显示，1不显示
     */
    @NotNull
    private Integer isShow;
    /**
     * 重定向地址，父级菜单默认地址（VUE）
     */
    private String redirectPath;
    /**
     * 是否缓存0缓存，1不缓存
     */
    @NotNull
    private Integer keepAlive;
    /**
     * 资源状态
     */
    private Integer status;
    /**
     * 资源描述
     */
    private String resourceDesc;
    /**
     * 路由组件
     */
    private String component;
    /**
     * 路由地址
     */
    private String path;
    /**
     * 所属授权系统
     */
    private Integer authSystemId;
}
