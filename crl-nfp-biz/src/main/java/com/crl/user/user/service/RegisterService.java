package com.crl.user.user.service;

import cn.hutool.core.lang.Validator;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.string.StringUtil;
import com.crl.user.user.dao.DepartmentDao;
import com.crl.user.user.dao.RegisterDao;
import com.crl.user.user.dao.RoleDao;
import com.crl.user.user.dao.UserDao;
import com.crl.user.user.dto.RegisterDto;
import com.crl.user.user.model.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

/**
 * 注册相关功能
 *
 * @author sunchao
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class RegisterService {
    @Autowired
    private RegisterDao registerDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private DepartmentDao departmentDao;

    /**
     * 注册用户
     * 先将用户名+手机号添加至用户信息，获取用户id，然后添加登录信息表
     *
     * @param registerDto
     * @return
     */
    public JsonResult register(RegisterDto registerDto) {
        String mobile = registerDto.getMobile();
        String code = registerDto.getCode();
        //验证手机号
        if(!Validator.isMobile(mobile)){
            return JsonResultUtil.error("手机号有错误");
        }
        ValueOperations operations = redisTemplate.opsForValue();
        Object obj = operations.get(mobile);
        if(obj == null){
            return JsonResultUtil.error("验证码已过期");
        }
        if(!code.equalsIgnoreCase(obj.toString())){
            return JsonResultUtil.error("验证码不正确");
        }
        redisTemplate.delete(mobile);
        String pwd = registerDto.getPassword();
        String repwd = registerDto.getRepassword();
        if(!pwd.equals(repwd)){
            return JsonResultUtil.error("两次密码不一致");
        }
        UserLogin userLogin = new UserLogin();
        String password = new BCryptPasswordEncoder(10).encode(pwd);
        userLogin.setUsername(registerDto.getUsername());
        userLogin.setPassword(password);
        userLogin.setMobile(registerDto.getMobile());
        //添加用户信息
        User user = new User();
        BeanUtils.copyProperties(registerDto, user);
        // todo 暂时写死 --苗凯旋
        user.setAuthSystemId(2);
        //获取机构所属路局给注册用户
        Department department = departmentDao.getById(user.getDepartmentId());
        if(department!=null && StringUtil.isNotNullOrEmpty(department.getBureauCode())){
            user.setBureauCode(department.getBureauCode());
        }
        int userId = userDao.saveUser(user);
        if (userId <= 0) {
            return JsonResultUtil.error("注册失败");
        }
        userLogin.setUserId(userId);
        int flag = registerDao.register(userLogin);
        if (flag > 0) {
            //分配默认角色
            Role role = roleDao.getByCode(UserService.DEFAULT_ROLE);
            //分配角色
            List<UserRole> userRoles = new ArrayList<>();
            UserRole userRole = new UserRole();
            userRole.setRoleId(role.getId());
            userRole.setUserId(userId);
            userRoles.add(userRole);
            int[] flags = userDao.batchRoleId(userRoles);
            if (flags.length > 0) {
                Map<String,Integer> userMap = new HashMap<>();
                userMap.put("user",userId);
                return JsonResultUtil.ok(userMap);
            } else {
                return JsonResultUtil.error("注册失败");
            }
        } else {
            return JsonResultUtil.error("注册失败");
        }
    }
    /**
     *实时校验验证码
     * @param registerDto
     * @return
     */
    public JsonResult checkMoibleCode(RegisterDto registerDto) {
        String mobile = registerDto.getMobile();
        String code = registerDto.getCode();
        ValueOperations operations = redisTemplate.opsForValue();
        Object obj = operations.get(mobile);
        if(obj == null){
            return JsonResultUtil.error("验证码已过期");
        }
        if(!code.equalsIgnoreCase(obj.toString())){
            return JsonResultUtil.error("验证码不正确");
        }
        return JsonResultUtil.ok();
    }
}
