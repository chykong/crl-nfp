package com.crl.user.user.dao;

import com.crl.user.user.model.UserLogin;
import com.crl.dao.BaseDao;
import org.springframework.stereotype.Repository;

@Repository
public class LoginDao extends BaseDao<UserLogin, UserLogin> {
    /**
     * 根据用户名获取用户登录信息
     */
    public UserLogin getUserLogin(String username) {
        String sql = "SELECT user_id,username,password,status,audit_status,error_times,last_login_time,change_password_time FROM crl_base_userlogin WHERE username=? AND status=0";
        return get(sql, username);
    }

    /**
     * 根据用户id获取用户登录信息
     */
    public UserLogin getUserLogin(Integer userId) {
        String sql = "SELECT user_id,username,password,status,audit_status,error_times,last_login_time,change_password_time FROM crl_base_userlogin WHERE user_id=? ";
        return get(sql, userId);
    }

    /**
     * 根据手机号获取用户登录信息
     */
    public UserLogin getUserLoginByMobile(String mobile) {
        String sql = "SELECT user_id,username,password,audit_status FROM crl_base_userlogin WHERE mobile=? AND status=0 ";
        return get(sql, mobile);
    }
    /**
     * 根据身份证获取用户登录信息
     */
    public UserLogin getUserLoginByIdCard(String mobile) {
        String sql = "SELECT l.user_id,l.username,l.password FROM crl_base_userlogin l left join crl_base_user u on u.id=l.user_id WHERE u.idcard=? AND l.status=0";
        return get(sql, mobile);
    }

    /**
     * 修改用户状态
     */
    public int updateStatus(Integer userId) {
        String sql = "UPDATE crl_base_userlogin SET status=((status+1) -BITAND(status,1)*2) WHERE user_id=?";
        return update(sql, userId);
    }
    /**
     * 修改用户审核状态
     */
    public int updateStatus(UserLogin userLogin) {
        String sql = "UPDATE crl_base_userlogin SET audit_status=:auditStatus WHERE user_id=:userId";
        return update(sql, userLogin);
    }
    /**
     * 修改用户状态
     */
    public int updatePwd(UserLogin userLogin) {
        String sql = "UPDATE crl_base_userlogin SET password=:password,change_password_time = :changePasswordTime, error_times = :errorTimes WHERE user_id=:userId ";
        return update(sql, userLogin);
    }
    /**
     * 修改用户登录错误次数和最后登录时间
     */
    public int updateErrorLoginInfo(UserLogin userLogin){
        String sql = "UPDATE crl_base_userlogin SET error_times=:errorTimes,last_login_time=:lastLoginTime WHERE user_id=:userId ";
        return update(sql, userLogin);
    }

    /**
     * 根据用户id删除用户登录信息
     */
    public int deleteUserLogin(Integer userId) {
        String sql = "DELETE FROM crl_base_userlogin WHERE user_id=?";
        return update(sql, userId);
    }

    /**
     * 根据用户id修改手机号
     * @param userLogin
     * @return
     */
    public int updateMobile(UserLogin userLogin) {
        String sql = "update crl_base_userlogin set mobile=:mobile where user_id=:userId";
        return update(sql,userLogin);
    }

    public UserLogin getUserLoginByNameOrMobile(String username) {
        String sql = "SELECT user_id,username,password,status,audit_status,error_times,last_login_time,change_password_time FROM crl_base_userlogin WHERE username=? or mobile=?";
        return get(sql, username,username);
    }
}
