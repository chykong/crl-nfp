package com.crl.user.system.vo;


import com.cars.util.bean.BasePageSearchVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel("业务系统查询条件接收对象")
public class SystemSearchVO extends BasePageSearchVO {
    @ApiModelProperty("系统名称")
    private String systemName;
    @ApiModelProperty("类别id")
    private Integer cateId;
    @ApiModelProperty("路局代码")
    private String ljdm;
}
