package com.crl.user.system.vo;

import com.cars.util.bean.BasePageSearchVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 业务系统大类表查询条件接收
 */
@Data
@ApiModel("业务系统大类查询条件接收实体")
public class SystemCategorySearchVO extends BasePageSearchVO {
    /**
     * 系统名称
     */
    @ApiModelProperty("系统名称")
    private String cateName;
    @ApiModelProperty("路局代码")
    private String ljdm;
}
