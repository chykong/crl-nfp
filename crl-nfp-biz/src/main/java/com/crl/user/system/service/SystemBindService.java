package com.crl.user.system.service;

import cn.hutool.core.util.RandomUtil;
import com.cars.util.bean.BasePage;
import com.cars.util.code.RandomCodeUtil;
import com.cars.util.encrypt.Encryp;
import com.cars.util.http.HttpUtil;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.json.JsonUtil;
import com.cars.util.list.ListUtils;
import com.cars.util.log.LogUtil;
import com.cars.util.string.StringUtil;
import com.crl.user.system.dao.SystemBindDao;
import com.crl.user.system.dao.SystemCategoryDao;
import com.crl.user.system.dao.SystemDao;
import com.crl.user.system.dto.SystemBindDto;
import com.crl.user.system.model.SystemBind;
import com.crl.user.system.model.System;
import com.crl.user.system.model.SystemCategory;
import com.crl.user.system.vo.SystemBindSearchVO;
import com.crl.user.user.dao.LoginDao;
import com.crl.user.user.model.UserLogin;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.net.ssl.SSLException;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.net.UnknownHostException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class SystemBindService {
    @Autowired
    private SystemBindDao systemBindDao;
    @Autowired
    private SystemDao systemDao;
    @Autowired
    private SystemCategoryDao systemCategoryDao;
    @Autowired
    private LoginDao loginDao;
    /**
     * 系统架构类型0B/S 1C/S
     */
    private final Integer SYSTEM_TYPE_CS = 1;
    private final Integer SYSTEM_TYPE_BS = 0;
    private final Integer SYSTEM_TYPE_VUE = 2;
    private final Integer SYSTEM_TYPE_WEBSERVICE = 3;

    /**
     * 绑定
     */
    public JsonResult bind(HttpServletRequest request, SystemBindDto systemBindDto) {
        String username = systemBindDto.getBindUsername();
        String password = systemBindDto.getBindPassword();
        Integer systemId = systemBindDto.getSystemId();
        String bindLjdm = systemBindDto.getBindLjdm();
        Integer userId = systemBindDto.getUserId();
        UserLogin userLogin = loginDao.getUserLogin(userId);
        if(userLogin != null) {
            String clieltIp = StringUtil.getIp(request);
            String salt = RandomCodeUtil.createRandomCode(6);
            String code = Encryp.en(password, salt);
            // 获取referer
            String referer = request.getHeader("referer");
            SystemBind sBind = systemBindDao.checkExist(systemId, userId, bindLjdm);
            if(sBind != null) {
                // 已存在绑定系统
                return JsonResultUtil.error("系统已绑定");
//                return updatePwd(systemBindDto, sBind, referer, clieltIp);
            }
            //根据用户id获取最大的排序号
            Integer maxPxh = systemBindDao.getMaxPxhByUserId(userId);
            maxPxh += 1;
            systemBindDto.setPxh(maxPxh);
            if (null == systemId) {
                return JsonResultUtil.error("绑定系统为空");
            }
            //根据系统id查询系统信息
            System system = systemDao.getById(systemId);
            if (null == system) {
                return JsonResultUtil.error("系统不存在");
            }
            //生成id
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            if (StringUtil.isNullOrEmpty(password)) {
                SystemBind systemBind = new SystemBind();
                BeanUtils.copyProperties(systemBindDto, systemBind);
                systemBind.setId(uuid);
                int flag = systemBindDao.save(systemBind);
                return returnFlag(flag, "绑定失败", null);
            }
            //获取系统大类
            SystemCategory systemCategory = systemCategoryDao.getById(system.getCateId());
            //系统类别，判断是C/S还是B/S
            //加密密码
            Integer systemType = systemCategory.getFramework();
            // referer = referer.substring(0, referer.lastIndexOf("/")) + "/";
            Map<String, String> userMap = new HashMap<>();
            userMap.put("hgpuser", userLogin.getUsername());
            userMap.put("username", username);
            userMap.put("ljdm", systemBindDto.getBindLjdm());
            userMap.put("code", code);
            userMap.put("random", salt);
            userMap.put("ip", referer);
            userMap.put("clientIp", clieltIp);
            userMap.put("id", uuid);
            //验证
            JsonResult jsonResult = checkSystemType(systemType, system.getBindPath(), userMap, system.getAppid());
            if (!jsonResult.success()) {
                return jsonResult;
            }
            SystemBind systemBind = new SystemBind();
            BeanUtils.copyProperties(systemBindDto, systemBind);
            //设置id和salt
            systemBind.setId(uuid);
            systemBind.setBindPassword(code);
            systemBind.setSalt(salt);
            int flag = systemBindDao.save(systemBind);
            return returnFlag(flag, "绑定失败", null);
        }
        return returnFlag(0, "绑定失败", "用户信息不存在");
    }

    /**
     * 解绑
     */
    public JsonResult unbind(HttpServletRequest request, SystemBindDto systemBindDto) {
        String username = systemBindDto.getBindUsername();
        String password = systemBindDto.getBindPassword();
        Integer systemId = systemBindDto.getSystemId();
        Integer userId = systemBindDto.getUserId();
        String bindLjdm = systemBindDto.getBindLjdm();
        //加密密码
        String salt = RandomCodeUtil.createRandomCode(6);
        String code = Encryp.en(password, salt);
        //根据系统id查询系统信息
        System system = systemDao.getById(systemId);
        //获取绑定信息
        SystemBind systemBind = systemBindDao.checkExist(systemId, userId, bindLjdm);
        // 获取referer
        String referer = request.getHeader("referer");
        //获取系统大类
        SystemCategory systemCategory = systemCategoryDao.getById(system.getCateId());
        //系统类别，判断是C/S还是B/S
        Integer systemType = systemCategory.getFramework();
        if (null == systemBind) {
            return JsonResultUtil.error("系统未绑定");
        }
        if(StringUtil.isNotNullOrEmpty(systemBind.getBindPassword())){
            //验证
            Map<String, String> userMap = new HashMap<>();
            userMap.put("username", username);
            userMap.put("ljdm", systemBindDto.getBindLjdm());
            userMap.put("code", code);
            userMap.put("random", salt);
            userMap.put("ip", referer);
            userMap.put("clientIp", StringUtil.getIp(request));
            userMap.put("id", systemBind.getId());
            JsonResult jsonResult = checkSystemType(systemType, system.getUnbindPath(), userMap, system.getAppid());
            if (!jsonResult.success()) {
                return jsonResult;
            }
        }
        int flag = systemBindDao.bindDelete(systemBindDto.getSystemId(), systemBindDto.getUserId(), bindLjdm);
        return returnFlag(flag, "绑定失败", null);

    }

    /**
     * 使用条件查询
     */
    public JsonResult<BasePage<List<SystemBindDto>>> listSearch(SystemBindSearchVO systemBindSearchVO) {
        List<SystemBind> list = systemBindDao.listSearch(systemBindSearchVO);
        List<SystemBindDto> resultList = ListUtils.copyList(list, SystemBindDto.class);
        List<SystemCategory> systemCategorieList = systemCategoryDao.listAll();
        Map<Integer, SystemCategory> integerSystemCategoryMap = systemCategorieList.stream().collect(Collectors.toMap(SystemCategory::getId, Function.identity(), (key1, key2) -> key2));
        resultList.forEach(tmp -> {
            SystemCategory systemCategory = integerSystemCategoryMap.get(tmp.getCateId());
            if (null != systemCategory) {
                tmp.setCateName(systemCategory.getCateName());
            }
        });
        int count = systemBindDao.count(systemBindSearchVO);
        BasePage<List<SystemBindDto>> basePage = new BasePage<>();
        basePage.setTotal(count);
        basePage.setData(resultList);
        return JsonResultUtil.ok(basePage);
    }

    /**
     * 使用用户id查询列表
     */
    public JsonResult<List<SystemBindDto>> listByUserId(SystemBindDto systemBindDto) {
        List<SystemBind> list = systemBindDao.listByUserId(systemBindDto.getUserId());
        List<SystemBindDto> resultList = ListUtils.copyList(list, SystemBindDto.class);
        return JsonResultUtil.ok(resultList);
    }

    /**
     * 根据用户id，系统id，绑定用户名获取单个对象(修改密码用）
     */
    public JsonResult<SystemBindDto> getBy(SystemBindDto systemBindDto) {
        SystemBind systemBind = systemBindDao.getBy(systemBindDto.getSystemId(), systemBindDto.getUserId(), systemBindDto.getBindUsername());
        if (systemBind == null) {
            return JsonResultUtil.error("用户信息错误");
        }
        return JsonResultUtil.ok(systemBindDto);
    }

    /**
     * 修改绑定账户密码
     */
    public JsonResult updatePwd(SystemBindDto systemBindDto, SystemBind systemBind,String referer, String clientIp) {
        if (systemBind == null) {
            systemBind = systemBindDao.getBy(systemBindDto.getSystemId(), systemBindDto.getUserId());
        }
        if(systemBind == null) {
            return JsonResultUtil.error("系绑定信息错误");
        }
        systemBind.setBindLjdm(systemBindDto.getBindLjdm());
        if(StringUtil.isNotNullOrEmpty(systemBindDto.getBindPassword())){
            UserLogin userLogin = loginDao.getUserLogin(systemBindDto.getUserId());
            if(userLogin == null) {
                return JsonResultUtil.error("用户信息异常");
            }
            System system = systemDao.getById(systemBindDto.getSystemId());
            if(system == null) {
                return JsonResultUtil.error("系统不存在");
            }
            SystemCategory systemCategory = systemCategoryDao.getById(system.getCateId());
            if(systemCategory == null) {
                return JsonResultUtil.error("系统分类信息不存在");
            }
            Map<String, String> userMap = new HashMap<>();
            String random = RandomUtil.randomNumbers(6);
            String code = Encryp.en(systemBindDto.getBindPassword(), random);
            userMap.put("hgpuser", userLogin.getUsername());
            userMap.put("username", systemBindDto.getBindUsername());
            userMap.put("ljdm", systemBindDto.getBindLjdm());
            userMap.put("code", code);
            userMap.put("random", random);
            userMap.put("id",systemBind.getId());
            userMap.put("clientIp", clientIp);
            userMap.put("ip",referer);
            //验证新密码是否正确
            JsonResult jsonResult = checkSystemType(systemCategory.getFramework(), system.getBindPath(), userMap, system.getAppid());
            if (!jsonResult.success()) {
                return jsonResult;
            }
            systemBind.setBindPassword(code);
            systemBind.setSalt(random);
        } else {
            systemBind.setBindPassword(null);
            systemBind.setSalt(null);
        }
        systemBind.setBindUsername(systemBindDto.getBindUsername());
        int flag = systemBindDao.updatePwd(systemBind);
        return returnFlag(flag, "修改密码失败", null);
    }

    /**
     * 根据不同系统进行不同调用
     *  @param systemType 系统类别
     * @param path       请求路径
     * @param userMap    用户名密码map
     * @param appid
     */
    public JsonResult checkSystemType(int systemType, String path, Map userMap, String appid) {
        JsonResult jsonResult = JsonResultUtil.error();
        if (SYSTEM_TYPE_CS == systemType) {
            //CS类别的系统验证
            jsonResult = checkCSUser(path, userMap);
        }
        if (SYSTEM_TYPE_BS == systemType || SYSTEM_TYPE_VUE == systemType ) {
            jsonResult = checkBSUser(path, userMap);
        }
        if (SYSTEM_TYPE_WEBSERVICE == systemType) {
            jsonResult = checkWebUser(path, userMap,appid);
        }
        return jsonResult;
    }

    /**
     * cs验证
     *
     * @param path    请求路径
     * @param userMap 用户名密码map
     */
    public JsonResult checkCSUser(String path, Map userMap) {
        String checkResult = HttpUtil.postPara(path, JsonUtil.toStr(userMap));
        try {
            JsonResult jsonResult = JsonUtil.toObject(checkResult, JsonResult.class);
            if (!jsonResult.success()) {
                if (StringUtil.isNullOrEmpty(jsonResult.getMsg())){
                    return JsonResultUtil.error(jsonResult.getMsg());
                }
                return JsonResultUtil.error("验证用户名或密码错误！");
            }
            return jsonResult;
        } catch (Exception e) {
            LogUtil.error(LogUtil.LOG_SERVICE, "路径：[{}],验证用户：[{}-{}]失败，异常为：[{}]。", path, userMap.get("username"), userMap.get("code"), e.getMessage());
            return JsonResultUtil.error("验证失败！");
        }
    }

    /**
     * webservice验证
     *
     *
     * @param path    请求路径
     * @param userMap 用户名密码map
     */
    public JsonResult checkWebUser(String path, Map userMap,String appid) {
        //BS类别的系统验证
        String checkResult = postPara(path, JsonUtil.toStr(userMap),appid);
        try {
            JsonResult jsonResult = JsonUtil.toObject(checkResult, JsonResult.class);
            if (!jsonResult.success()) {
                if (StringUtil.isNullOrEmpty(jsonResult.getMsg())){
                    return JsonResultUtil.error(jsonResult.getMsg());
                }
                return JsonResultUtil.error("验证用户名或密码错误！");
            }
            return jsonResult;
        } catch (Exception e) {
            LogUtil.error(LogUtil.LOG_SERVICE, "路径：[{}],验证用户：[{}-{}]失败，异常为：[{}]。", path, userMap.get("username"), userMap.get("code"), e.getMessage());
            return JsonResultUtil.error("验证失败！");
        }
    }
    /**
     * bs验证
     *
     * @param path    请求路径
     * @param userMap 用户名密码map
     */
    public JsonResult checkBSUser(String path, Map userMap) {
        //BS类别的系统验证
        String checkResult = HttpUtil.postPara(path, JsonUtil.toStr(userMap));
        try {
            JsonResult jsonResult = JsonUtil.toObject(checkResult, JsonResult.class);
            if (!jsonResult.success()) {
                if (StringUtil.isNullOrEmpty(jsonResult.getMsg())){
                    return JsonResultUtil.error(jsonResult.getMsg());
                }
                return JsonResultUtil.error("验证用户名或密码错误！");
            }
            return jsonResult;
        } catch (Exception e) {
            LogUtil.error(LogUtil.LOG_SERVICE, "路径：[{}],验证用户：[{}-{}]失败，异常为：[{}]。", path, userMap.get("username"), userMap.get("code"), e.getMessage());
            return JsonResultUtil.error("验证失败！");
        }
    }
    /**
     * @param flag
     * @param message
     * @param e
     * @param <E>
     * @return
     */
    private <E> JsonResult<E> returnFlag(int flag, String message, E e) {
        if (flag > 0) {
            return JsonResultUtil.ok(e);
        } else {
            return JsonResultUtil.error(message, e);
        }
    }

    public static void main(String[] args) {
        Map<String, String> userMap = new HashMap<>();
        userMap.put("username", "1");
        userMap.put("password", "1");
        HttpUtil.postPara("http://localhost:8082/systembind/test", JsonUtil.toStr(userMap));
    }

    /**
     * 我的绑定系统列表
     * @param systemBindSearchVO
     * @return
     */
    public JsonResult<BasePage<List<SystemBindDto>>> myListSearch(SystemBindSearchVO systemBindSearchVO) {
        List<SystemBind> list = systemBindDao.myListSearch(systemBindSearchVO);
        List<SystemBindDto> resultList = ListUtils.copyList(list, SystemBindDto.class);
        int count = systemBindDao.myListCount(systemBindSearchVO);
        BasePage<List<SystemBindDto>> basePage = new BasePage<>();
        basePage.setTotal(count);
        basePage.setData(resultList);
        return JsonResultUtil.ok(basePage);
    }

    /**
     * 修改我的绑定系统的排序号 颜色 图标
     * @param systemBindDto
     * @return
     */
    public JsonResult updateMyBind(SystemBindDto systemBindDto) {
        SystemBind systemBind = new SystemBind();
        SystemBind oldBind = systemBindDao.getMyBindById(systemBindDto.getId());
        //如果现排序号跟原排序号不同则修改其他排序号
        if(oldBind.getPxh()!=null && !oldBind.getPxh().equals(systemBindDto.getPxh())) {
            updateOtherPx(oldBind.getPxh(),systemBindDto.getPxh(),oldBind.getUserId());
        }
        BeanUtils.copyProperties(systemBindDto,systemBind);
        int result = systemBindDao.updateMyBind(systemBind);
        if(result>0){
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("更新失败");
        }
    }

    /**
     * 根据原排序号和现排序号，修改排序
     * @param oldPxh
     * @param pxh
     */
    private void updateOtherPx(Integer oldPxh, Integer pxh,Integer userId) {
        if(oldPxh < pxh) {
            systemBindDao.updateDpx(oldPxh,pxh,userId);
        } else {
            systemBindDao.updateXpx(oldPxh,pxh,userId);
        }
    }

    /**
     * 根据id获取我的绑定信息
     * @param systemBindDto
     * @return
     */
    public JsonResult<SystemBind> getMyBindById(SystemBindDto systemBindDto) {
        SystemBind systemBind = systemBindDao.getMyBindById(systemBindDto.getId());
        return JsonResultUtil.ok(systemBind);
    }

    /**
     * 排序号上下移动
     * @param systemBindDto
     * @return
     */
    public JsonResult movePxh(SystemBindDto systemBindDto) {
        SystemBind systemBind = systemBindDao.getMyBindById(systemBindDto.getId());
        //根据userid  排序号  上下移动标记获取紧挨着的实体类
        SystemBind moveBind = systemBindDao.getMyBindByPxh(systemBind.getPxh(),systemBindDto.getMoveType(),systemBind.getUserId());

        if(moveBind == null) {
            return JsonResultUtil.ok();
        } else {
            Integer pxh = moveBind.getPxh();
            moveBind.setPxh(systemBind.getPxh());
            systemBind.setPxh(pxh);
            systemBindDao.updatePxh(systemBind);
            systemBindDao.updatePxh(moveBind);
            return JsonResultUtil.ok();
        }
    }

    /**
     * 计量绑定和解绑专用
     * @param url
     * @param jsonParam
     * @return
     */
    public String postPara(String url, String jsonParam,String appid) {
        LogUtil.info("httpLog", "请求url：" + url + ";请求参数：" + jsonParam, new Object[0]);
        String getJson = "";
        CloseableHttpClient httpclient = null;
        HttpPost httpPost = null;

        try {
            httpclient = HttpClients.custom().setRetryHandler(myRetryHandler).build();
            httpPost = new HttpPost(url);
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30000).setConnectionRequestTimeout(30000).setSocketTimeout(30000).build();
            httpPost.setConfig(requestConfig);
            StringEntity entity = new StringEntity(jsonParam.toString(), "utf-8");
            entity.setContentEncoding("UTF-8");
            entity.setContentType("application/json");
            httpPost.setHeader("appid",appid);
            httpPost.setEntity(entity);
            CloseableHttpResponse response = httpclient.execute(httpPost);
            response.getStatusLine().getStatusCode();
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));

            for(String str = ""; (str = rd.readLine()) != null; getJson = getJson + str) {
            }

            LogUtil.info("httpLog", "返回值：" + getJson, new Object[0]);
            response.close();
        } catch (Exception var18) {
            LogUtil.error("httpLog", "参数错误信息：{}" + var18.getMessage(), new Object[0]);
        } finally {
            httpPost.abort();
            httpPost.releaseConnection();

            try {
                httpclient.close();
            } catch (IOException var17) {
                var17.printStackTrace();
            }

        }

        return getJson;
    }
    public  HttpRequestRetryHandler myRetryHandler = new HttpRequestRetryHandler() {
        public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
            if (executionCount >= 5) {
                return false;
            } else if (exception instanceof InterruptedIOException) {
                return false;
            } else if (exception instanceof UnknownHostException) {
                return false;
            } else if (exception instanceof ConnectTimeoutException) {
                return false;
            } else if (exception instanceof SSLException) {
                return false;
            } else {
                HttpClientContext clientContext = HttpClientContext.adapt(context);
                HttpRequest request = clientContext.getRequest();
                boolean idempotent = !(request instanceof HttpEntityEnclosingRequest);
                return idempotent;
            }
        }
    };
}
