package com.crl.user.system.service;

import com.cars.util.bean.BasePage;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.list.ListUtils;
import com.crl.user.system.dao.SystemBindDao;
import com.crl.user.system.dao.SystemDao;
import com.crl.user.system.dao.SystemLjDao;
import com.crl.user.system.dto.SystemDto;
import com.crl.user.system.model.SystemBind;
import com.crl.user.system.model.SystemLj;
import com.crl.user.system.model.System;
import com.crl.user.system.vo.SystemSearchVO;
import com.crl.user.user.dao.UserDao;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 业务系统管理
 */
@Service
public class SystemService {
    @Autowired
    private SystemDao systemDao;
    @Autowired
    private SystemBindDao systemBindDao;
    @Autowired
    private SystemLjDao systemLjDao;
    @Autowired
    private UserDao userDao;

    /**
     * 新增
     */
    public JsonResult save(SystemDto systemDto) {
        System system = new System();
        BeanUtils.copyProperties(systemDto, system);
        int flag = systemDao.save(system);
        if (flag > 0) {
            List<String> ljdmList = systemDto.getLjdms();
            for(String ljdm: ljdmList){
                SystemLj systemLj = new SystemLj();
                systemLj.setLjdm(ljdm);
                systemLj.setSystemId(flag);
                systemLjDao.save(systemLj);
            }
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("新增系统失败");
        }
    }

    /**
     * 删除
     */
    public JsonResult delete(SystemDto systemDto) {
        List<SystemBind> list = systemBindDao.listBySystemId(systemDto.getId());
        if (list.size() > 0) {
            return JsonResultUtil.error("该系统存在绑定用户，不能删除");
        }
        int flag = systemDao.delete(systemDto.getId());
        if (flag > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("删除系统失败");
        }
    }

    /**
     * 修改
     */
    public JsonResult update(SystemDto systemDto) {
        System system = new System();
        BeanUtils.copyProperties(systemDto, system);
        int flag = systemDao.update(system);
        if (flag > 0) {
            systemLjDao.deleteBySystemId(systemDto.getId());
            List<String> ljdmList = systemDto.getLjdms();
            for(String ljdm: ljdmList){
                SystemLj systemLj = new SystemLj();
                systemLj.setLjdm(ljdm);
                systemLj.setSystemId(systemDto.getId());
                systemLjDao.save(systemLj);
            }
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("删除系统失败");
        }
    }

    /**
     * 按照条件查询
     */
    public JsonResult<BasePage<List<SystemDto>>> list(SystemSearchVO systemSearchVO) {
        List<System> list = systemDao.list(systemSearchVO);
        int count = systemDao.count(systemSearchVO);
        List<SystemDto> systemDtoList = ListUtils.copyList(list, SystemDto.class);
        BasePage<List<SystemDto>> basePage = new BasePage<>();
        basePage.setTotal(count);
        basePage.setData(systemDtoList);
        return JsonResultUtil.ok(basePage);
    }

    /**
     * 根据id查询
     */
    public JsonResult<SystemDto> getById(SystemDto systemDto) {
        System system = systemDao.getById(systemDto.getId());
        List<SystemLj> systemLjList = systemLjDao.getBySystemId(systemDto.getId());
        List<String> ljdms = new ArrayList<>();
        for(SystemLj systemLj:systemLjList){
            ljdms.add(systemLj.getLjdm());
        }
        SystemDto systemDto1 = new SystemDto();
        BeanUtils.copyProperties(system, systemDto1);
        systemDto1.setLjdms(ljdms);
        return JsonResultUtil.ok(systemDto1);
    }

    /**
     * 根据条件查询所有
     */
    public JsonResult<Map<String,Object>> listAll(Map<String, Object> requestMap) {
        Integer userId = Integer.parseInt(requestMap.get("userId").toString());
        //根据userid查询用户所在路局
        String ljdm = userDao.getById(userId).getBureauCode();
        requestMap.put("ljdm",ljdm);
        List<System> list = systemDao.listAll(requestMap);
        Map<String,Object> result = new HashMap();
        for(System system:list){
            if(ljdm.equals(system.getLjdm())){
                result.put("systemId",system.getId());
                break;
            }
        }
        List<SystemDto> systemDtoList = ListUtils.copyList(list, SystemDto.class);
        result.put("list",systemDtoList);
        return JsonResultUtil.ok(result);
    }

    /**
     * 获取最大排序号
     * @return
     */
    public JsonResult getMaxOrder(Integer cateId) {
        return JsonResultUtil.ok(systemDao.getMaxOrder(cateId));
    }

    /**
     * 根据字典id 查询对应系统信息
     *
     * @param dicId
     * @return
     */
    public JsonResult systemListByDicId(String dicId) {
        List<System> list = systemDao.listSystemByDicId(Integer.parseInt(dicId));
        List<SystemDto> systemDtoList = ListUtils.copyList(list, SystemDto.class);
        return JsonResultUtil.ok(systemDtoList);
    }

    public JsonResult<List<SystemLj>> listSystemLj(Map<String, Object> requestMap) {
        Integer systemId = Integer.parseInt(requestMap.get("systemId").toString());
        List<SystemLj> ljList = systemLjDao.getBySystemId(systemId);
        return JsonResultUtil.ok(ljList);
    }
}
