package com.crl.user.system.service;

import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.string.StringUtil;
import com.crl.user.system.dao.SystemBindDao;
import com.crl.user.system.model.HgpLogin;
import com.crl.user.system.model.SystemBind;
import com.crl.user.user.dto.UserDto;
import com.crl.user.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description:绑定用户
 * @Date:2020/5/8 16:49
 * @Author:苗凯旋
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class BindUserService {
    @Autowired
    private UserService userService;

    @Autowired
    private SystemBindDao systemBindDao;


    /*public JsonResult commonBind(HttpServletRequest request, HgpLogin hgpLogin) {
        String hgpuser = hgpLogin.getHgpuser();
        String username = hgpLogin.getUsername();
        String code = hgpLogin.getCode();
        String random = hgpLogin.getRandom();
        String id = hgpLogin.getId();
        String refererIp = hgpLogin.getIp();
        // TODO 如果需要ljdm/tmis，请自行处理
        String ljdm = hgpLogin.getLjdm();

        // 获取请求头信息
        String clientIp = request.getHeader("ClientIp");
        //判断是否是空
        if (StringUtil.isNullOrEmpty(hgpuser)) {
            return JsonResultUtil.error("统一平台账号不能为空");
        }
        if (StringUtil.isNullOrEmpty(username)) {
            return JsonResultUtil.error("业务系统账号不能为空");
        }
        if (StringUtil.isNullOrEmpty(code)) {
            return JsonResultUtil.error("业务系统密码不能为空");
        }
        if (StringUtil.isNullOrEmpty(random)) {
            return JsonResultUtil.error("加密随机数不能为空");
        }
        if (StringUtil.isNullOrEmpty(id)) {
            return JsonResultUtil.error("用户唯一标识不能为空");
        }
        if (StringUtil.isNullOrEmpty(refererIp)) {
            return JsonResultUtil.error("请求IP源不能为空");
        }


        //根据用户名获取，判断密码是否正确
        Map<String, String> map = new HashMap<>();
        map.put("userName", username);
        map.put("code", code);
        map.put("random", random);
        JsonResult jsonResult = userService.checkUser(map);
        if(jsonResult.getReturnCode().equals(GlobalReturnCode.OK.getReturnCode())){
            int result = bindUserDao.insert(hgpLogin);
            if(result>0) {
                return JsonResultUtil.ok("绑定成功");
            }else {
                return JsonResultUtil.error("绑定失败");
            }

        } else {
            return JsonResultUtil.error("绑定失败");
        }
    }*/


    /*public JsonResult commonUnBind(HttpServletRequest request,  HgpLogin hgpLogin) {
        String username = hgpLogin.getUsername();
        String code = hgpLogin.getCode();
        String random = hgpLogin.getRandom();
        String id = hgpLogin.getId();
        String refererIp = hgpLogin.getIp();
        // TODO 如果需要ljdm/tmis，请自行处理
        String ljdm = hgpLogin.getLjdm();
        // 获取请求头信息
        String clientIp = request.getHeader("ClientIp");
        //判断是否是空

        if (StringUtil.isNullOrEmpty(username)) {
            return JsonResultUtil.error("业务系统账号不能为空");
        }
        if (StringUtil.isNullOrEmpty(code)) {
            return JsonResultUtil.error("业务系统密码不能为空");
        }
        if (StringUtil.isNullOrEmpty(random)) {
            return JsonResultUtil.error("加密随机数不能为空");
        }
        if (StringUtil.isNullOrEmpty(id)) {
            return JsonResultUtil.error("用户唯一标识不能为空");
        }
        if (StringUtil.isNullOrEmpty(refererIp)) {
            return JsonResultUtil.error("请求IP源不能为空");
        }
        //根据用户名获取，判断密码是否正确
        Map<String, String> map = new HashMap<>();
        map.put("userName", username);
        map.put("code", code);
        map.put("random", random);
        JsonResult jsonResult = userService.checkUser(map);
        if(jsonResult.getReturnCode().equals(GlobalReturnCode.OK.getReturnCode())){
            int result = bindUserDao.delete(hgpLogin.getId());
            if(result>0) {
                return JsonResultUtil.ok("解绑成功");
            }else {
                return JsonResultUtil.error("解绑失败");
            }

        } else {
            return JsonResultUtil.error("解绑失败");
        }

    }*/

    public JsonResult commonLogin(HttpServletRequest request, HgpLogin hgpLogin) {
        String id = hgpLogin.getId();
        String ljdm = hgpLogin.getLjdm();
        if (StringUtil.isNullOrEmpty(id)) {
            return JsonResultUtil.error();
        }
        SystemBind systemBind = systemBindDao.getById(id);
        //referer
        String refererIp = request.getHeader("refererIp");
        /*if ( null == login) {
            return JsonResultUtil.error();
        }*/
        //绑定ip和refererip不一样
       /* if (!refererIp.equals(login.getIp())) {
            return JsonResultUtil.error();
        }*/

       if(systemBind != null) {
           UserDto user = userService.getByUsername(systemBind.getBindUsername());
           if (user == null) {
               return JsonResultUtil.error();
           } else {
               if (user.getStatus() == 2) {
                   return JsonResultUtil.error();
               } else {
                   return JsonResultUtil.ok(user);
               }
           }
       } else {
           return JsonResultUtil.error();
       }
    }
}
