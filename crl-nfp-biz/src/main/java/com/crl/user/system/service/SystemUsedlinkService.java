package com.crl.user.system.service;

import com.cars.util.bean.BasePage;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.list.ListUtils;
import com.crl.user.system.dao.SystemDao;
import com.crl.user.system.dao.SystemUsedlinkDao;
import com.crl.user.system.dto.SystemUsedlinkDto;
import com.crl.user.system.model.SystemUsedlink;
import com.crl.user.system.model.System;
import com.crl.user.system.vo.SystemUsedlinkSearchVO;
import com.crl.user.user.dao.UserDao;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SystemUsedlinkService {
    @Autowired
    private SystemUsedlinkDao systemUsedLinkDao;
    @Autowired
    private SystemDao systemDao;
    @Autowired
    private UserDao userDao;

    /**
     * 新增
     */
    public JsonResult saveSystemUsedlink(SystemUsedlinkDto systemUsedLinkDto) {
        SystemUsedlink systemUsedLink = new SystemUsedlink();
        BeanUtils.copyProperties(systemUsedLinkDto, systemUsedLink);
        updateSavePxh(systemUsedLink.getDisplayOrder(),systemUsedLink.getCreatedBy());
        int flag = systemUsedLinkDao.saveSystemUsedlink(systemUsedLink);
        if (flag > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("新增常用链接失败");
        }
    }

    /**
     * 新增时，如果现有排序号大于或等于当前排序号，则所有排序号+1
     * @param displayOrder
     * @param createdBy
     */
    private void updateSavePxh(Integer displayOrder, String createdBy) {
        systemUsedLinkDao.updateSavePxh(displayOrder,createdBy);
    }

    /**
     * 删除
     */
    public JsonResult deleteSystemUsedlink(SystemUsedlinkDto systemUsedLinkDto) {
        int flag = systemUsedLinkDao.deleteSystemUsedlink(systemUsedLinkDto.getId());
        if (flag > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("删除常用链接失败");
        }
    }


    /**
     * 根据id查询
     */
    public JsonResult<SystemUsedlinkDto> getById(SystemUsedlinkDto systemUsedLinkDto) {
        SystemUsedlink systemUsedLink = systemUsedLinkDao.getById(systemUsedLinkDto.getId());
        if (systemUsedLink == null) {
            return JsonResultUtil.error("id不存在");
        }
        SystemUsedlinkDto systemUsedlinkDto1 = new SystemUsedlinkDto();
        BeanUtils.copyProperties(systemUsedLink, systemUsedlinkDto1);
        return JsonResultUtil.ok(systemUsedlinkDto1);
    }

    /**
     * 查询所有
     */
    public JsonResult<List<SystemUsedlinkDto>> listAll() {
        List<SystemUsedlink> list = systemUsedLinkDao.listAll();
        List<SystemUsedlinkDto> listResult = ListUtils.copyList(list, SystemUsedlinkDto.class);
        return JsonResultUtil.ok(listResult);
    }

    /**
     * 根据创建人查询
     */
    public JsonResult<List<SystemUsedlinkDto>> listByCreatedBy(String userId) {
        List<SystemUsedlink> list = systemUsedLinkDao.listByCreatedBy(userId);
        List<SystemUsedlinkDto> listResult = ListUtils.copyList(list, SystemUsedlinkDto.class);
        return JsonResultUtil.ok(listResult);
    }

    /**
     * 获取最大排序号
     * @return
     */
    public JsonResult getMaxOrder(String userId) {
        return JsonResultUtil.ok(systemUsedLinkDao.getMaxOrder(userId));
    }


    /**
     * 我的绑定系统列表
     * @param systemBindSearchVO
     * @return
     */
    public JsonResult<BasePage<List<SystemUsedlinkDto>>> myListSearch(SystemUsedlinkSearchVO systemBindSearchVO) {
        List<SystemUsedlink> list = systemUsedLinkDao.myListSearch(systemBindSearchVO);
        List<SystemUsedlinkDto> resultList = ListUtils.copyList(list, SystemUsedlinkDto.class);
        int count = systemUsedLinkDao.myListCount(systemBindSearchVO);
        BasePage<List<SystemUsedlinkDto>> basePage = new BasePage<>();
        basePage.setTotal(count);
        basePage.setData(resultList);
        return JsonResultUtil.ok(basePage);
    }

    /**
     * 修改我的绑定系统的排序号 颜色 图标
     * @param systemUsedlinkDto
     * @return
     */
    public JsonResult updateMyUsedlink(SystemUsedlinkDto systemUsedlinkDto) {
        SystemUsedlink systemUsedlink = new SystemUsedlink();
        SystemUsedlink oldLink = systemUsedLinkDao.getMyUsedlinkById(systemUsedlinkDto.getId());
        //如果现排序号跟原排序号不同则修改其他排序号
        if(oldLink.getDisplayOrder()!=null && !oldLink.getDisplayOrder().equals(systemUsedlinkDto.getDisplayOrder())) {
            updateOtherPx(oldLink.getDisplayOrder(),systemUsedlinkDto.getDisplayOrder(),oldLink.getCreatedBy());
        }
        BeanUtils.copyProperties(systemUsedlinkDto,systemUsedlink);
        int result = systemUsedLinkDao.updateMyUsedlink(systemUsedlink);
        if(result>0){
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("更新失败");
        }
    }
    /**
     * 根据原排序号和现排序号，修改排序
     * @param oldPxh
     * @param pxh
     */
    private void updateOtherPx(Integer oldPxh, Integer pxh,String userId) {
        if(oldPxh < pxh) {
            systemUsedLinkDao.updateDpx(oldPxh,pxh,userId);
        } else {
            systemUsedLinkDao.updateXpx(oldPxh,pxh,userId);
        }
    }
    /**
     * 根据id获取我的绑定信息
     * @param systemUsedlinkDto
     * @return
     */
    public JsonResult<SystemUsedlink> getMyUsedlinkById(SystemUsedlinkDto systemUsedlinkDto) {
        SystemUsedlink systemBind = systemUsedLinkDao.getMyUsedlinkById(systemUsedlinkDto.getId());
        return JsonResultUtil.ok(systemBind);
    }

    /**
     * 排序号上下移动
     * @param systemUsedlinkDto
     * @return
     */
    public JsonResult movePxh(SystemUsedlinkDto systemUsedlinkDto) {
        SystemUsedlink systemUsedlink = systemUsedLinkDao.getMyUsedlinkById(systemUsedlinkDto.getId());
        //根据userid  排序号  上下移动标记获取紧挨着的实体类
        SystemUsedlink moveUsedlink = systemUsedLinkDao.getMyBindByPxh(systemUsedlink.getDisplayOrder(),systemUsedlinkDto.getMoveType(),systemUsedlink.getCreatedBy());

        if(moveUsedlink == null) {
            return JsonResultUtil.ok();
        } else {
            Integer pxh = moveUsedlink.getDisplayOrder();
            moveUsedlink.setDisplayOrder(systemUsedlink.getDisplayOrder());
            systemUsedlink.setDisplayOrder(pxh);
            systemUsedLinkDao.updatePxh(systemUsedlink);
            systemUsedLinkDao.updatePxh(moveUsedlink);
            return JsonResultUtil.ok();
        }
    }
}
