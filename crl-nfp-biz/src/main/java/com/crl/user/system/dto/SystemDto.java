package com.crl.user.system.dto;

import com.cars.util.bean.BaseModel;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 业务系统数据交互对象
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel("业务系统数据交互对象")
public class SystemDto extends BaseModel {
    /**
     * id
     */
    @ApiModelProperty("id")
    @NotNull(groups = {UpdateValidGroup.class, DeleteValidGroup.class})
    private Integer id;
    /**
     * 系统大类id
     */
    @ApiModelProperty("系统大类id")
    @NotNull(groups = {AddValidGroup.class, UpdateValidGroup.class})
    private Integer cateId;
    /**
     * 系统名称
     */
    @ApiModelProperty("系统名称")
    @NotBlank(groups = {AddValidGroup.class, UpdateValidGroup.class})
    private String systemName;
    /**
     * 系统地址
     */
    @ApiModelProperty("系统地址")
    @NotBlank(groups = {AddValidGroup.class, UpdateValidGroup.class})
    private String systemPath;
    /**
     * 绑定地址
     */
    @ApiModelProperty("绑定地址")
    private String bindPath;
    /**
     * 解绑地址
     */
    @ApiModelProperty("解绑地址")
    private String unbindPath;
    /**
     * 系统地址
     */
    @ApiModelProperty("回调地址")
    private String callbackPath;
    /**
     * 字典推送地址
     */
    @ApiModelProperty("字典推送地址")
    private String dicPushPath;
    /**
     * 联系人
     */
    @ApiModelProperty("联系人")
    private String contacts;
    /**
     * 联系电话
     */
    @ApiModelProperty("联系电话")
    private String mobile;
    /**
     * 大类名称
     */
    @ApiModelProperty("大类名称")
    private String cateName;
    /**
     * 排序号
     */
    @ApiModelProperty("排序号")
    private Integer displayOrder;

    /**
     * 路局代码
     */
    @ApiModelProperty("路局代码")
    private String ljdm;
    /**
     * 系统可见范围
     */
    private List<String> ljdms;
    /**
     * 是否分配
     */
    private Boolean flag;
    /**
     * 是否统一登陆接入（0，否；1，是）
     */
    private String sftydljr;
    /**
     * 是否统一字典接入（0，否；1，是）
     */
    private String sftyzdjr;
    /**
     * 系统appid
     */
    private String appid;
}
