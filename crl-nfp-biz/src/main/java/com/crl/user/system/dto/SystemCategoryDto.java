package com.crl.user.system.dto;

import com.cars.util.bean.BaseModel;
import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel("系统大类传输实体")
public class SystemCategoryDto extends BaseModel {
    /**
     * id
     */
    @ApiModelProperty("id")
    @NotNull(groups = {DeleteValidGroup.class, UpdateValidGroup.class})
    private Integer id;
    /**
     * 系统名称
     */
    @ApiModelProperty("分类名称")
    @NotBlank(groups = {AddValidGroup.class, UpdateValidGroup.class})
    private String cateName;
    /**
     * 系统架构类型0B/S 1C/S
     */
    @ApiModelProperty("系统架构类型0 B/S 1 C/S")
    @NotNull(groups = {AddValidGroup.class, UpdateValidGroup.class})
    private Integer framework;
    /**
     * 系统图标
     */
    @ApiModelProperty("系统图标")
    private String cateIcon;
    /**
     * 排序号
     */
    @ApiModelProperty("排序号")
    private Integer displayOrder;
}
