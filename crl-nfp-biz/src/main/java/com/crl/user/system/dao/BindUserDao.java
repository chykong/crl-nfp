package com.crl.user.system.dao;


import com.crl.user.system.model.HgpLogin;
import com.crl.dao.BaseDao;
import org.springframework.stereotype.Repository;

/**
 * @Description:用户绑定
 * @Date:2020/5/8 17:28
 * @Author:苗凯旋
 */
@Repository
public class BindUserDao extends BaseDao<HgpLogin,HgpLogin> {
    /**
     * 新增
     * @param hgpLogin
     * @return
     */
    public int insert(HgpLogin hgpLogin) {
        String sql = "insert into CRL_BIND_USER(id,ip,username,hgpuser,ljdm,created_at) values(:id,:ip,:username,:hgpuser,:ljdm,sysdate)";
        return insert(sql,hgpLogin);
    }

    /**
     * 删除
     * @param id
     * @return
     */
    public int delete(String id) {
        String sql = "delete from CRL_BIND_USER where id=?";
        return delete(sql,id);
    }

    /**
     * 根据id获取
     * @param id
     * @return
     */
    public HgpLogin getById(String id) {
        String sql = "select * from CRL_BIND_USER where id = ?";
        return get(sql,id);
    }
}
