package com.crl.user.system.dao;

import com.crl.user.system.model.SystemUsedlink;
import com.crl.user.system.vo.SystemUsedlinkSearchVO;
import com.cars.util.page.PageUtil;
import com.cars.util.string.StringUtil;
import com.crl.dao.BaseDao;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 常用链接dao
 */
@Repository
public class SystemUsedlinkDao extends BaseDao<SystemUsedlink, SystemUsedlinkSearchVO> {
    /**
     * 新增
     */
    public int saveSystemUsedlink(SystemUsedlink systemUsedLink) {
        String sql = "INSERT INTO crl_base_used_link(id,sys_name,url,display_order,created_by,created_at) " +
                "VALUES(seq_crl_base_used_link.nextval,:sysName,:url,:displayOrder,:createdBy,sysdate)";
        return insert(sql, systemUsedLink);
    }

    /**
     * 删除
     */
    public int deleteSystemUsedlink(Integer systemUsedLinkId) {
        String sql = "DELETE FROM crl_base_used_link WHERE id=?";
        return delete(sql, systemUsedLinkId);
    }

    /**
     * 根据id查询
     */
    public SystemUsedlink getById(Integer systemUsedLinkId) {
        String sql = "SELECT id,sys_name,url,display_order,created_by,created_at FROM crl_base_used_link WHERE id=?";
        return get(sql, systemUsedLinkId);
    }

    /**
     * 查询所有
     */
    public List<SystemUsedlink> listAll() {
        String sql = "SELECT id,sys_name,url,display_order,created_by,created_at FROM crl_base_used_link ORDER BY display_order asc";
        return list(sql);
    }

    /**
     * 查询所有
     */
    public List<SystemUsedlink> listByCreatedBy(String CreatedBy) {
        String sql = "SELECT id,sys_name,url,display_order,created_by,created_at,colour,icon FROM crl_base_used_link where created_by = ? ORDER BY display_order asc";
        return list(sql, CreatedBy);
    }

    /**
     * 获取最大排序号
     * @return
     */
    public int getMaxOrder(String createBy) {
        String sql = "SELECT nvl(MAX(display_order),0) as display_order FROM crl_base_used_link where created_by = ?";
        SystemUsedlink sc = get(sql,createBy);
        if(sc == null){
            return 0;
        }
        return sc.getDisplayOrder() == null? 0: sc.getDisplayOrder() + 1;
    }

    /**
     * 我的常用列表
     * @param systemUsedlinkSearchVO
     * @return
     */
    public List<SystemUsedlink> myListSearch(SystemUsedlinkSearchVO systemUsedlinkSearchVO) {
        String sql = "SELECT id,sys_name,url,display_order,created_by,created_at,colour,icon FROM crl_base_used_link where 1=1 ";
        sql += createMySQL(systemUsedlinkSearchVO);
        sql += " ORDER BY  display_order asc";
        sql = PageUtil.createOraclePageSQL(sql, systemUsedlinkSearchVO.getPageIndex(), systemUsedlinkSearchVO.getPageSize());
        return list(sql,systemUsedlinkSearchVO);
    }

    /**
     * 添加查询条件
     * @param systemUsedlinkSearchVO
     * @return
     */
    private String createMySQL(SystemUsedlinkSearchVO systemUsedlinkSearchVO) {
        String sql = "";
        if (systemUsedlinkSearchVO.getCreatedBy()!=null) {
            sql += " and created_by = :createdBy";
        }
        if(StringUtil.isNotNullOrEmpty(systemUsedlinkSearchVO.getSysName())){
            systemUsedlinkSearchVO.setSysName("%"+systemUsedlinkSearchVO.getSysName()+"%");
            sql += " and sys_name like :sysName";
        }
        return sql;
    }

    /**
     * 查询我的链接数量
     * @param systemUsedlinkSearchVO
     * @return
     */
    public int myListCount(SystemUsedlinkSearchVO systemUsedlinkSearchVO) {
        String sql = "SELECT count(1) FROM crl_base_used_link where 1=1 ";
        sql += createMySQL(systemUsedlinkSearchVO);
        return count(sql, systemUsedlinkSearchVO);
    }

    /**
     * 修改我的常用链接的图标 颜色和排序
     * @param systemUsedlink
     * @return
     */
    public int updateMyUsedlink(SystemUsedlink systemUsedlink) {
        String sql = " update crl_base_used_link set sys_name=:sysName, url=:url, display_order=:displayOrder, colour=:colour, icon=:icon where id=:id";
        return update(sql,systemUsedlink);
    }

    /**
     * 根据id获取常用链接
     * @param id
     * @return
     */
    public SystemUsedlink getMyUsedlinkById(Integer id) {
        String sql = "SELECT id,sys_name,url,display_order,created_by,created_at,colour,icon FROM crl_base_used_link where id=? ";
        return get(sql,id);
    }

    public int updateSavePxh(Integer displayOrder, String createdBy) {
        String sql = "update crl_base_used_link set display_order = display_order + 1 where created_by = ? and display_order >= ?";
        return update(sql,createdBy,displayOrder);
    }

    /**
     * 当原排序号比现在排序小时
     * @param oldPxh
     * @param pxh
     */
    public Integer updateDpx(Integer oldPxh, Integer pxh,String userId) {
        String sql = "update crl_base_used_link set display_order = display_order - 1 where display_order > ? and display_order<= ? and created_by = ?";
        return update(sql,oldPxh,pxh,userId);
    }

    /**
     * 当原排序号比现在排序大时
     * @param oldPxh
     * @param pxh
     */
    public Integer updateXpx(Integer oldPxh, Integer pxh,String userId) {
        String sql = "update crl_base_used_link set display_order = display_order + 1 where display_order >= ? and display_order < ? and created_by = ?";
        return update(sql,pxh,oldPxh,userId);
    }

    public SystemUsedlink getMyBindByPxh(Integer pxh, String moveType, String userId) {
        String sql = "select * from crl_base_used_link where created_by = ?";
        if ("1".equals(moveType)) {
            sql += " and display_order<?";
            sql += "ORDER by display_order desc";
        } else {
            sql += " and display_order>?";
            sql += "ORDER by display_order asc";
        }

        List<SystemUsedlink> list = list(sql,userId,pxh);
        if (!CollectionUtils.isEmpty(list)){
            return list.get(0);
        } else {
            return null;
        }

    }

    /**
     * 修改排序号
     * @param systemUsedlink
     * @return
     */
    public int updatePxh(SystemUsedlink systemUsedlink) {
        String sql = "update crl_base_used_link set display_order = :displayOrder where id=:id";
        return update(sql,systemUsedlink);
    }
}
