package com.crl.user.system.model;


import com.cars.util.bean.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 业务系统实体类
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class System extends BaseModel {
    /**
     * id
     */
    private Integer id;
    /**
     * 系统大类id
     */
    private Integer cateId;
    /**
     * 系统名称
     */
    private String systemName;
    /**
     * 系统地址
     */
    private String systemPath;
    /**
     * 排序号
     */
    private Integer displayOrder;
    /**
     * 绑定地址
     */
    private String bindPath;
    /**
     * 解绑地址
     */
    private String unbindPath;
    /**
     * 系统url链接
     */
    private String callbackPath;
    /**
     * 字典推送地址
     */
    private String dicPushPath;
    /**
     * 联系人
     */
    private String contacts;
    /**
     * 联系电话
     */
    private String mobile;
    /**
     * 大类名称
     */
    private String cateName;
    /**
     * 路局代码
     */
    private String ljdm;

    /**
     * 是否分配
     */
    private Boolean flag;
    /**
     * 是否统一登陆接入（0，否；1，是）
     */
    private String sftydljr;
    /**
     * 是否统一字典接入（0，否；1，是）
     */
    private String sftyzdjr;
    /**
     * 系统appid
     */
    private String appid;
}
