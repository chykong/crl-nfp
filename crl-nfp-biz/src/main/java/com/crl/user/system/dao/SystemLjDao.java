package com.crl.user.system.dao;


import com.crl.user.system.model.SystemLj;
import com.crl.dao.BaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Description:系统可见范围dao
 * @Date:2019/12/25 20:20
 * @Author:苗凯旋
 */
@Repository
public class SystemLjDao extends BaseDao<SystemLj,SystemLj> {
    public int save(SystemLj systemLj) {
        String sql = "insert into crl_base_system_lj(system_id,ljdm) values(:systemId,:ljdm)";
        return insert(sql,systemLj);
    }

    /**
     * 获取SystemLj
     * @param id
     * @return
     */
    public List<SystemLj> getBySystemId(Integer id) {
        String sql = "select system_id,ljdm from crl_base_system_lj where system_id=? order by ljdm" ;
        return list(sql,id);
    }

    /**
     * 根据systemid删除
     * @param flag
     * @return
     */
    public int deleteBySystemId(int flag) {
        String sql = "delete from crl_base_system_lj where system_id=?";
        return delete(sql,flag);
    }
}
