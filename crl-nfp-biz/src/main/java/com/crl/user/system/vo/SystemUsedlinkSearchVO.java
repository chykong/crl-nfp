package com.crl.user.system.vo;

import com.cars.util.bean.BasePageSearchVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 常用链接
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("常用链接")
public class SystemUsedlinkSearchVO extends BasePageSearchVO {
    @ApiModelProperty("系统名称")
    private String sysName;
    @ApiModelProperty("创建人")
    private String createdBy;
}
