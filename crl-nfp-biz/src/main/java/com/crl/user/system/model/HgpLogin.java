package com.crl.user.system.model;

import lombok.Data;

/**
 * @Description:
 * @Date:2020/5/8 16:50
 * @Author:苗凯旋
 */
@Data
public class HgpLogin {
    private String hgpuser;
    private String username;
    private String code;
    private String random;
    private String ljdm;
    private String ip;
    private String clientIp;
    private String id;

}
