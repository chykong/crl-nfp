package com.crl.user.system.dto;

import com.cars.util.validation.AddValidGroup;
import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@ApiModel("绑定系统传输对象")
public class SystemBindDto {
    /**
     * 系统id
     */
    @ApiModelProperty("系统id")
    @NotNull(groups = {AddValidGroup.class, UpdateValidGroup.class})
    private Integer systemId;
    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    @NotNull(groups = {AddValidGroup.class, UpdateValidGroup.class, DeleteValidGroup.class})
    private Integer userId;
    /**
     * 绑定系统的用户名
     */
    @ApiModelProperty("绑定系统的用户名")
    private String bindUsername;
    /**
     * 绑定系统的密码
     */
    @ApiModelProperty("绑定系统的密码")
    private String bindPassword;
    /**
     * 绑定时间
     */
    @ApiModelProperty("绑定时间")
    private Date bindedAt;
    /**
     * 状态
     */
    @ApiModelProperty("状态")
    private Integer status;
    /**
     * 系统名称
     */
    @ApiModelProperty("系统名称")
    private String systemName;
    /**
     * 系统地址
     */
    @ApiModelProperty("系统地址")
    private String systemPath;
    /**
     * 系统大类id
     */
    @ApiModelProperty("系统大类id")
    private Integer cateId;
    /**
     * 系统大类名称
     */
    @ApiModelProperty("系统大类名称")
    private String cateName;
    /**
     * 系统大类图标
     */
    @ApiModelProperty("系统大类图标")
    private String cateIcon;
    /**
     * 负责人联系电话
     */
    @ApiModelProperty("负责人联系电话")
    private String mobile;
    /**
     * 系统架构类型0B/S 1C/S
     */
    @ApiModelProperty("系统架构类型0B/S 1C/S")
    private Integer framework;
    /**
     * B/S系统调用随机数
     */
    @ApiModelProperty("B/S系统调用随机数")
    private String random;
    /**
     * cs回调地址
     */
    @ApiModelProperty("cs回调地址")
    private String csCallbackPath;
    /**
     * bs回调地址
     */
    @ApiModelProperty("bs回调地址")
    private String webCallbackPath;
    /**
     * 回调地址
     */
    @ApiModelProperty("回调地址")
    private String callbackPath;
    /**
     * 用户标识
     */
    @ApiModelProperty("用户标识")
    private String id;
    /**
     * 绑定路局代码
     */
    @ApiModelProperty("绑定路局代码")
    private String bindLjdm;
    /**
     * 是否统一登陆接入（0，否；1，是）
     */
    private String sftydljr;
    /**
     * 是否统一字典接入（0，否；1，是）
     */
    private String sftyzdjr;
    /**
     * 排序号
     */
    private Integer pxh;
    /**
     * 颜色
     */
    private String colour;
    /**
     * 图标
     */
    private String icon;
    /**
     * 排序上下移动标记 1上移，0 下移
     */
    private String moveType;

}
