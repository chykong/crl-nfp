package com.crl.user.system.model;

import lombok.Data;

import java.util.Date;

/**
 * 常用链接表
 */
@Data
public class SystemUsedlink {
    /**
     * id
     */
    private Integer id;
    /**
     * 系统名称
     */
    private String sysName;
    /**
     * 系统地址
     */
    private String url;
    /**
     * 排序
     */
    private Integer displayOrder;
    /**
     *创建人
     */
    private String createdBy;
    /**
     *创建时间
     */
    private Date createdAt;
    /**
     * 颜色
     */
    private String colour;
    /**
     * 图标
     */
    private String icon;

}
