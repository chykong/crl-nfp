package com.crl.user.system.vo;

import com.cars.util.bean.BasePageSearchVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel("查询接收实体")
public class SystemBindSearchVO extends BasePageSearchVO {
    /**
     * 系统id
     */
    @ApiModelProperty("系统id")
    private Integer systemId;
    @ApiModelProperty("用户id,当前用户id，前端自动补齐，不需要在查询条件显示")
    @NotNull
    private Integer userId;
    /**
     * 系统名称
     */
    private String systemName;
}
