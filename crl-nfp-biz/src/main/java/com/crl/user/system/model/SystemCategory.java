package com.crl.user.system.model;

import com.cars.util.bean.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 业务系统大类表HGP_BASE_SYSTEM_CATEGORY
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SystemCategory extends BaseModel {
    /**
     * id
     */
    private Integer id;
    /**
     * 系统名称
     */
    private String cateName;
    /**
     * 最大排序号
     */
    private Integer displayOrder;
    /**
     * 系统架构类型0B/S 1C/S
     */
    private Integer framework;
    /**
     * 系统图标
     */
    private String cateIcon;

}
