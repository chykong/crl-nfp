package com.crl.user.system.dao;


import com.crl.user.system.model.SystemCategory;
import com.crl.user.system.vo.SystemCategorySearchVO;
import com.cars.util.page.PageUtil;
import com.cars.util.string.StringUtil;
import com.crl.dao.BaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 业务系统大类表crl_base_SYSTEM_CATEGORY
 */
@Repository
public class SystemCategoryDao extends BaseDao<SystemCategory, SystemCategorySearchVO> {
    /**
     * 新增
     */
    public int saveSystemCategory(SystemCategory systemCategory) {
        String sql = "INSERT INTO crl_base_system_category(id,cate_name,framework,cate_icon,display_order,created_by,created_at) " +
                "VALUES(seq_crl_base_system_category.nextval,:cateName,:framework,:cateIcon,:displayOrder,:createdBy,sysdate)";
        return insert(sql, systemCategory);
    }

    /**
     * 删除
     */
    public int deleteSystemCategory(Integer systemCategoryId) {
        String sql = "DELETE FROM crl_base_system_category WHERE id=?";
        return delete(sql, systemCategoryId);
    }

    /**
     * 修改
     */
    public int updateSystemCategory(SystemCategory systemCategory) {
        String sql = "UPDATE crl_base_system_category SET " +
                "cate_name=:cateName," +
                "framework=:framework," +
                "cate_icon=:cateIcon," +
                "display_order=:displayOrder," +
                "modified_by=:modifiedBy," +
                "modified_at=sysdate " +
                "WHERE id=:id";
        return update(sql, systemCategory);

    }

    /**
     * 按照条件查询
     */
    public List<SystemCategory> listBySearch(SystemCategorySearchVO systemCategorySearchVO) {
        String sql = "SELECT id,cate_name,framework,cate_icon,display_order,created_by,created_at,modified_by,modified_at FROM crl_base_system_category WHERE 1=1 ";
        sql += createSQL(systemCategorySearchVO);
        sql += " ORDER BY display_order asc ";
        sql = PageUtil.createOraclePageSQL(sql, systemCategorySearchVO.getPageIndex(), systemCategorySearchVO.getPageSize());
        return list(sql, systemCategorySearchVO);
    }

    /**
     * 按照条件统计数量
     */
    public int countBySearch(SystemCategorySearchVO systemCategorySearchVO) {
        String sql = "SELECT COUNT(id) FROM crl_base_system_category WHERE 1=1 ";
        sql += createSQL(systemCategorySearchVO);
        sql += " ORDER BY created_by DESC ";
        return count(sql, systemCategorySearchVO);
    }

    /**
     * 拼接查询条件
     */
    public String createSQL(SystemCategorySearchVO systemCategorySearchVO) {
        StringBuffer sql = new StringBuffer(50);
        if (StringUtil.isNotNullOrEmpty(systemCategorySearchVO.getCateName())) {
            systemCategorySearchVO.setCateName("%" + systemCategorySearchVO.getCateName() + "%");
            sql.append(" AND cate_name LIKE :cateName");
        }
        return sql.toString();
    }

    /**
     * 根据id查询
     */
    public SystemCategory getById(Integer systemCategoryId) {
        String sql = "SELECT id,cate_name,framework,cate_icon,display_order,created_by,created_at,modified_by,modified_at FROM crl_base_system_category WHERE id=?";
        return get(sql, systemCategoryId);
    }

    /**
     * 查询所有
     */
    public List<SystemCategory> listAll() {
        String sql = "SELECT id,cate_name,framework,cate_icon,display_order FROM crl_base_system_category ORDER BY display_order asc";
        return list(sql);
    }

    /**
     * 获取最大排序号
     * @return
     */
    public int getMaxOrder() {
        String sql = "SELECT MAX(display_order) as display_order FROM crl_base_system_category ";
        SystemCategory sc = get(sql);
        if(sc == null){
            return 0;
        }
        return sc.getDisplayOrder() == null? 0: sc.getDisplayOrder();
    }

    public List<SystemCategory> listAllByUserId(SystemCategorySearchVO systemCategorySearchVO) {
        String sql = "SELECT DISTINCT c.ID, c.cate_name, c.framework, c.cate_icon, c.display_order FROM  crl_base_system_category c"+
                " LEFT JOIN crl_base_SYSTEM s on c.id=s.CATE_ID"+
                " LEFT JOIN crl_base_SYSTEM_LJ l on s.id = l.system_id"+
                " where 1=1";
        if(StringUtil.isNotNullOrEmpty(systemCategorySearchVO.getLjdm())){
            sql += " and l.ljdm = :ljdm ";
        }
        sql += "ORDER BY c.display_order ASC";
        return list(sql,systemCategorySearchVO);
    }
}
