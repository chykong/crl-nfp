package com.crl.user.system.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class SystemBind {
    /**
     * 唯一主键
     */
    private String id;
    /**
     * 系统id
     */
    private Integer systemId;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 绑定系统的用户名
     */
    private String bindUsername;
    /**
     * 绑定系统的密码
     */
    private String bindPassword;
    /**
     * 绑定时间
     */
    private Date bindedAt;
    /**
     * 盐
     */
    private String salt;
    /**
     * 状态 0正常1不可用
     */
    private Integer status;
    /**
     * 系统名称
     */
    private String systemName;
    /**
     * 系统地址
     */
    private String systemPath;
    /**
     * 系统大类id
     */
    private Integer cateId;
    /**
     * 大类名称
     */
    private String cateName;
    /**
     * 系统大类图标
     */
    @ApiModelProperty("系统大类图标")
    private String cateIcon;
    /**
     * 负责人电话
     */
    private String mobile;
    /**
     * cs回调地址
     */
    private String csCallbackPath;
    /**
     * bs回调地址
     */
    private String webCallbackPath;
    /**
     * 回调地址
     */
    private String callbackPath;
    /**
     * 绑定路局代码
     */
    private String bindLjdm;
    /**
     * 是否统一登陆接入（0，否；1，是）
     */
    private String sftydljr;
    /**
     * 是否统一字典接入（0，否；1，是）
     */
    private String sftyzdjr;
    /**
     * 排序号
     */
    private Integer pxh;
    /**
     * 颜色
     */
    private String colour;
    /**
     * 图标
     */
    private String icon;
    /**
     * 系统架构类型0B/S 1C/S
     */
    private Integer framework;
}
