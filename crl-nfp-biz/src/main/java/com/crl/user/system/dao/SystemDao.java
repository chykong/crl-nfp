package com.crl.user.system.dao;

import com.crl.user.system.model.System;
import com.crl.user.system.vo.SystemSearchVO;
import com.cars.util.page.PageUtil;
import com.cars.util.string.StringUtil;
import com.crl.dao.BaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 业务系统表
 */
@Repository
public class SystemDao extends BaseDao<System, SystemSearchVO> {
    /**
     * 增加
     */
    public int save(System system) {
        String sql = "INSERT INTO crl_base_system(id,cate_id,system_name,system_path,bind_path,unbind_path,callback_path,dic_push_path,created_by,created_at,contacts,mobile,display_order,ljdm, sftydljr, sftyzdjr,appid) " +
                " VALUES(seq_crl_base_system.nextval,:cateId,:systemName,:systemPath,:bindPath,:unbindPath,:callbackPath,:dicPushPath,:createdBy,sysdate,:contacts,:mobile,:displayOrder,:ljdm, :sftydljr, :sftyzdjr,:appid)";
        return insertForId(sql, system,"id");
    }

    /**
     * 修改
     */
    public int update(System system) {
        String sql = "UPDATE crl_base_system SET " +
                "cate_id=:cateId,system_name=:systemName,system_path=:systemPath,bind_path=:bindPath,unbind_path=:unbindPath,callback_path=:callbackPath," +
                "dic_push_path=:dicPushPath,modified_by=:modifiedBy,modified_at=sysdate,display_order=:displayOrder,contacts=:contacts,mobile=:mobile,ljdm=:ljdm,sftydljr=:sftydljr,sftyzdjr=:sftyzdjr,appid=:appid " +
                "WHERE id=:id";
        return update(sql, system);
    }

    /**
     * 根据id获取
     */
    public System getById(Integer systemId) {
        String sql = "SELECT id,cate_id,system_name,system_path,bind_path,unbind_path,callback_path,dic_push_path,created_by,created_at,contacts,mobile,modified_at,modified_by,display_order,ljdm,sftydljr,sftyzdjr,appid FROM crl_base_system WHERE id=?";
        return get(sql, systemId);
    }

    /**
     * 根据条件查询
     */
    public List<System> list(SystemSearchVO systemSearchVO) {
        String sql = "SELECT t.id,t.cate_id,t.system_name,system_path,t.bind_path,t.unbind_path,t.callback_path,t.dic_push_path,t.created_by,t.created_at,t.contacts,t.mobile,t.modified_at,t.modified_by,c.cate_name,t.ljdm,t.sftydljr,t.sftyzdjr,t.appid FROM crl_base_system t left join crl_base_system_category c on t.cate_id = c.id WHERE 1=1 ";
        sql += createSQL(systemSearchVO);
        sql += " ORDER BY c.display_order asc, t.display_order asc";
        sql = PageUtil.createOraclePageSQL(sql, systemSearchVO.getPageIndex(), systemSearchVO.getPageSize());
        return list(sql, systemSearchVO);
    }

    /**
     * 根据条件统计数量
     */
    public int count(SystemSearchVO systemSearchVO) {
        String sql = "SELECT COUNT(t.id) FROM crl_base_system t WHERE 1=1 ";
        sql += createSQL(systemSearchVO);
        return count(sql, systemSearchVO);
    }

    /**
     * 拼接查询条件
     */
    public String createSQL(SystemSearchVO systemSearchVO) {
        StringBuffer sql = new StringBuffer(50);
        if (StringUtil.isNotNullOrEmpty(systemSearchVO.getSystemName())) {
            systemSearchVO.setSystemName("%" + systemSearchVO.getSystemName() + "%");
            sql.append(" AND t.system_name LIKE :systemName ");
        }
        if (systemSearchVO.getCateId() != null) {
            sql.append(" AND t.cate_id =:cateId ");
        }
        return sql.toString();
    }

    /**
     * 查询所有
     */
    public List<System> listAll(Map<String, Object> requestMap) {
        String sql = "SELECT t.id,t.cate_id,t.system_name,t.system_path,t.system_path,t.bind_path,t.unbind_path,t.callback_path,t.dic_push_path,t.created_by,t.created_at,t.contacts,t.mobile,t.modified_at,t.modified_by,c.cate_name,t.ljdm,t.sftydljr,t.sftyzdjr,t.appid FROM crl_base_system t left join crl_base_system_category c on t.cate_id = c.id "+
                " LEFT JOIN crl_base_SYSTEM_LJ l ON l.SYSTEM_ID = t.id where 1 = 1 ";
        SystemSearchVO systemSearchVO = new SystemSearchVO();
        if(requestMap != null){
            Integer cateId = (Integer)requestMap.get("cateId");
            if(cateId != null){
                sql += " and t.cate_id =:cateId";
                systemSearchVO.setCateId(cateId);
            }
            Object ljdm = requestMap.get("ljdm");
            if(ljdm != null){
                sql += " and l.ljdm = :ljdm";
                systemSearchVO.setLjdm(ljdm.toString());
            }
        }
        sql += " ORDER BY c.display_order asc, t.display_order asc";
        return list(sql, systemSearchVO);
    }

    /**
     * 删除
     */
    public int delete(Integer systemId) {
        String sql = "DELETE FROM crl_base_system WHERE id=?";
        return delete(sql, systemId);
    }

    /**
     * 获取最大排序号
     * @param cateId
     * @return
     */
    public Object getMaxOrder(Integer cateId) {
        String sql = "SELECT MAX(display_order) as display_order FROM crl_base_system where cate_id = ?";
        System sys = get(sql, cateId);
        if(sys == null){
            return 0;
        }
        return sys.getDisplayOrder() == null? 0: sys.getDisplayOrder();
    }

    /**
     * 根据字典id 查询对应系统信息
     *
     * @param dicId
     * @return
     */
    public List<System> listSystemByDicId(Integer dicId) {
        String sql = "select t.id,t.cate_id,t.system_name,t.system_path,t.system_path,t.bind_path,t.unbind_path,t.callback_path,t.dic_push_path,t.created_by,t.created_at,t.contacts,t.mobile,t.modified_at,t.modified_by,t.sftydljr,t.sftyzdjr,t.appid," +
                " case when (select count(dic_id) from hgp_dic_system s where  s.system_id =t.id and dic_id=?)> 0 then 1 else 0 end flag  from crl_base_system t where t.sftyzdjr=1" ;
        return list(sql,dicId);

    }

    /**
     * 数量
     * @param
     * @return
     */
    public Integer countByDicId() {
        String sql = "select count(*)  from crl_base_system t where t.sftyzdjr=1 " ;
        return count(sql);
    }
}
