package com.crl.user.system.dto;

import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@ApiModel("常用链接实体")
public class SystemUsedlinkDto {

    @ApiModelProperty("id")
    @NotNull(groups = {DeleteValidGroup.class, UpdateValidGroup.class})
    private Integer id;
    @ApiModelProperty("系统名称")
    private String sysName;
    @ApiModelProperty("系统地址")
    private String url;
    @ApiModelProperty("排序")
    private Integer displayOrder;
    @ApiModelProperty("创建人")
    private String createdBy;
    @ApiModelProperty("创建时间")
    private Date createdAt;
    /**
     * 颜色
     */
    private String colour;
    /**
     * 图标
     */
    private String icon;
    /**
     * 排序上下移动标记 1上移，0 下移
     */
    private String moveType;
}
