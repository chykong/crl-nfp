package com.crl.user.system.dao;


import com.crl.user.system.model.SystemBind;
import com.crl.user.system.vo.SystemBindSearchVO;
import com.cars.util.page.PageUtil;
import com.cars.util.string.StringUtil;
import com.crl.dao.BaseDao;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 系统绑定
 */
@Repository
public class SystemBindDao extends BaseDao<SystemBind, SystemBindSearchVO> {
    /**
     * 绑定
     */
    public int save(SystemBind systemBind) {
        String sql = "INSERT INTO crl_base_user_system(id,system_id,user_id,bind_username,bind_password,binded_at,salt,status,bind_ljdm,pxh,colour,icon) " +
                "VALUES(:id,:systemId,:userId,:bindUsername,:bindPassword,sysdate,:salt,0,:bindLjdm,:pxh,:colour,:icon)";
        return insert(sql, systemBind);
    }

    /**
     * 解绑,根据系统id，用户id，绑定用户名
     */
    public int delete(Integer systemId, Integer userId, String bindUsername) {
        String sql = "DELETE FROM crl_base_user_system WHERE system_id=? AND user_id=? AND bind_username=?";
        return delete(sql, systemId, userId, bindUsername);
    }

    /**
     * 解绑,根据系统id，用户id
     */
    public int delete(Integer systemId, Integer userId) {
        String sql = "DELETE FROM crl_base_user_system WHERE system_id=? AND user_id=? ";
        return delete(sql, systemId, userId);
    }

    /**
     * 解绑,根据系统id，用户id,路局代码
     */
    public int bindDelete(Integer systemId, Integer userId, String bindLjdm){
        String sql = "DELETE FROM crl_base_user_system WHERE system_id=? AND user_id=? and bind_ljdm = ?";
        return delete(sql, systemId, userId, bindLjdm);
    }

    /**
     * 根据用户id查询，按照系统id，绑定时间排序
     */
    public List<SystemBind> listByUserId(Integer userId) {
        String sql = "SELECT t.id,t.system_id,t.user_id,t.bind_username,t.bind_password,t.binded_at,t.salt,t.bind_ljdm,t.status,s.callback_path, s.system_name, s.system_path, s.cate_id, c.cate_name, c.cate_icon, s.sftydljr,s.sftyzdjr,c.framework " +
                " FROM crl_base_user_system t left join crl_base_system s on t.system_id = s.id left join crl_base_system_category c on s.cate_id = c.id " +
                " WHERE t.user_id=? AND t.status=0 ORDER BY t.pxh asc , t.binded_at DESC";
        return list(sql, userId);
    }

    /**
     * 用系统id，用户id，查询可使用的列表
     */
    public SystemBind getBy(Integer systemId, Integer userId) {
        String sql = "SELECT id,system_id,user_id,bind_username,bind_password,binded_at,salt,status,bind_ljdm FROM crl_base_user_system WHERE system_id=? AND user_id=? ";
        return get(sql, systemId, userId);
    }

    /**
     * 用系统id，用户id，查询可使用的列表
     */
    public SystemBind checkExist(Integer systemId, Integer userId, String bindLjdm) {
        String sql = "SELECT id,system_id,user_id,bind_username,bind_password,binded_at,salt,status,bind_ljdm FROM crl_base_user_system WHERE system_id=? AND user_id=? and bind_ljdm = ?";
        return get(sql, systemId, userId, bindLjdm);
    }

    /**
     * 根据id查询
     */
    public SystemBind getByIdAndUserId(String id, Integer userId) {
        String sql = "SELECT id,system_id,user_id,bind_username,bind_password,binded_at,salt,status,bind_ljdm FROM crl_base_user_system WHERE id=? and user_id = ?";
        return get(sql, id, userId);
    }

    /**
     * 用系统id，用户id，绑定用户名查询可使用的列表
     */
    public SystemBind getBy(Integer systemId, Integer userId, String bindUsername) {
        String sql = "SELECT id,system_id,user_id,bind_username,bind_password,binded_at,salt,status,bind_ljdm FROM crl_base_user_system  WHERE system_id=? AND user_id=? AND bind_username=?";
        return get(sql, systemId, userId, bindUsername);
    }

    /**
     * 根据条件查询
     */
    public List<SystemBind> listSearch(SystemBindSearchVO systemBindSearchVO) {
        String sql = "SELECT t.system_id,t.user_id,t.bind_username,t.bind_password,t.binded_at,t.salt,t.status,,bind_ljdm(SELECT system_name FROM crl_base_system WHERE id= t.system_id)  system_name,(SELECT mobile FROM crl_base_system WHERE id= t.system_id)  mobile,(SELECT cate_id FROM crl_base_system WHERE id= t.system_id)  cate_id FROM crl_base_user_system t WHERE 1=1 AND t.user_id=:userId ";
        sql += createSQL(systemBindSearchVO);
        sql += " ORDER BY t.system_id,t.binded_at DESC";
        sql = PageUtil.createOraclePageSQL(sql, systemBindSearchVO.getPageIndex(), systemBindSearchVO.getPageSize());
        return list(sql, systemBindSearchVO);
    }

    /**
     * 拼接查询条件
     */
    public String createSQL(SystemBindSearchVO systemBindSearchVO) {
        StringBuffer sql = new StringBuffer(50);
        if (null != systemBindSearchVO.getSystemId()) {
            sql.append(" AND t.system_id=:systemId ");
        }
        return sql.toString();
    }

    /**
     * 统计查询数量
     */
    public int count(SystemBindSearchVO systemBindSearchVO) {
        String sql = "SELECT COUNT(t.user_id) FROM crl_base_user_system t WHERE 1=1 AND t.user_id=:userId ";
        sql += createSQL(systemBindSearchVO);
        return count(sql,systemBindSearchVO);
    }

    /**
     * 根据系统id，用户id，用户名修改密码
     *
     * @param systemBind
     * @return
     */
    public int updatePwd(SystemBind systemBind) {
        String sql = "UPDATE crl_base_user_system SET bind_password=:bindPassword,bind_username=:bindUsername,salt=:salt,bind_ljdm=:bindLjdm WHERE system_id=:systemId AND user_id=:userId";
        return update(sql, systemBind);
    }

    /**
     * 根据系统id获取数据
     */
    public List<SystemBind> listBySystemId(Integer systemId) {
        String sql = "SELECT system_id,user_id,bind_username,bind_password,binded_at,salt,status,bind_ljdm FROM crl_base_user_system WHERE system_id=? ";
        return list(sql, systemId);
    }

    /**
     * 我的绑定资源列表
     * @param systemBindSearchVO
     * @return
     */
    public List<SystemBind> myListSearch(SystemBindSearchVO systemBindSearchVO) {
        String sql = "SELECT us.id,s.SYSTEM_NAME,us.bind_ljdm,s.SYSTEM_PATH,c.cate_name,us.COLOUR,us.ICON FROM crl_base_USER_SYSTEM us ";
        sql += " LEFT JOIN crl_base_SYSTEM s ON us.SYSTEM_ID = s. ID ";
        sql += " LEFT JOIN crl_base_SYSTEM_CATEGORY c ON s.cate_id = c. ID  where 1=1";
        sql += createMySQL(systemBindSearchVO);
        sql += " ORDER BY us.PXH";
        sql = PageUtil.createOraclePageSQL(sql, systemBindSearchVO.getPageIndex(), systemBindSearchVO.getPageSize());
        return list(sql,systemBindSearchVO);
    }

    /**
     * 添加查询条件
     * @param systemBindSearchVO
     * @return
     */
    private String createMySQL(SystemBindSearchVO systemBindSearchVO) {
        String sql = "";
        if(systemBindSearchVO.getUserId()!=null){
            sql += " and us.user_id = :userId";
        }
        if(StringUtil.isNotNullOrEmpty(systemBindSearchVO.getSystemName())){
            systemBindSearchVO.setSystemName("%"+systemBindSearchVO.getSystemName()+"%");
            sql += " and s.system_name like :systemName";
        }
        return sql;
    }

    /**
     * 查询我的链接数量
     * @param systemBindSearchVO
     * @return
     */
    public int myListCount(SystemBindSearchVO systemBindSearchVO) {
        String sql = "SELECT count(1) FROM crl_base_USER_SYSTEM us ";
        sql += " LEFT JOIN crl_base_SYSTEM s ON us.SYSTEM_ID = s. ID ";
        sql += " LEFT JOIN crl_base_SYSTEM_CATEGORY c ON s.cate_id = c. ID  where 1=1";
        sql += createMySQL(systemBindSearchVO);
        return count(sql, systemBindSearchVO);
    }

    /**
     * 修改我的绑定系统的图标 颜色和排序
     * @param systemBind
     * @return
     */
    public int updateMyBind(SystemBind systemBind) {
        String sql = " update crl_base_USER_SYSTEM set pxh=:pxh, colour=:colour, icon=:icon where id=:id";
        return update(sql,systemBind);
    }

    /**
     * 根据id获取绑定系统
     * @param id
     * @return
     */
    public SystemBind getMyBindById(String id) {
        String sql = "SELECT us.id,us.user_id,s.SYSTEM_NAME,us.pxh,us.bind_ljdm,s.SYSTEM_PATH,c.cate_name,us.COLOUR,us.ICON FROM crl_base_USER_SYSTEM us ";
        sql += " LEFT JOIN crl_base_SYSTEM s ON us.SYSTEM_ID = s. ID ";
        sql += " LEFT JOIN crl_base_SYSTEM_CATEGORY c ON s.cate_id = c. ID  where us.id=?";
        return get(sql,id);
    }

    /**
     * 根据用户id获取最大的排序号
     * @param userId
     * @return
     */
    public Integer getMaxPxhByUserId(Integer userId) {
        String sql = " select nvl(max(pxh),0) from crl_base_USER_SYSTEM where user_id = ?";
        return count(sql,userId);
    }

    /**
     * 当原排序号比现在排序小时
     * @param oldPxh
     * @param pxh
     */
    public Integer updateDpx(Integer oldPxh, Integer pxh,Integer userId) {
        String sql = "update crl_base_USER_SYSTEM set pxh = pxh - 1 where pxh > ? and pxh<= ? and user_id = ?";
        return update(sql,oldPxh,pxh,userId);
    }

    /**
     * 当原排序号比现在排序大时
     * @param oldPxh
     * @param pxh
     */
    public Integer updateXpx(Integer oldPxh, Integer pxh,Integer userId) {
        String sql = "update crl_base_USER_SYSTEM set pxh = pxh + 1 where pxh >= ? and pxh < ? and user_id = ?";
        return update(sql,pxh,oldPxh,userId);
    }

    public SystemBind getMyBindByPxh(Integer pxh, String moveType, Integer userId) {
        String sql = "select * from crl_base_USER_SYSTEM where user_id = ?";
        if ("1".equals(moveType)) {
            sql += " and pxh<?";
            sql += "ORDER by pxh desc";
        } else {
            sql += " and pxh>?";
            sql += "ORDER by pxh asc";
        }

        List<SystemBind> list = list(sql,userId,pxh);
        if (!CollectionUtils.isEmpty(list)){
            return list.get(0);
        } else {
            return null;
        }

    }

    /**
     * 修改排序号
     * @param systemBind
     * @return
     */
    public int updatePxh(SystemBind systemBind) {
        String sql = "update crl_base_USER_SYSTEM set pxh = :pxh where id=:id";
        return update(sql,systemBind);
    }

    public SystemBind getById(String id) {
        String sql = "select * from crl_base_USER_SYSTEM where id = ?";
        return get(sql, id);
    }
}
