package com.crl.user.system.service;

import com.cars.util.bean.BasePage;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.list.ListUtils;
import com.crl.user.system.dao.SystemCategoryDao;
import com.crl.user.system.dao.SystemDao;
import com.crl.user.system.dto.SystemCategoryDto;
import com.crl.user.system.model.SystemCategory;
import com.crl.user.system.model.System;
import com.crl.user.system.vo.SystemCategorySearchVO;
import com.crl.user.system.vo.SystemSearchVO;
import com.crl.user.user.dao.UserDao;
import io.swagger.models.auth.In;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.embedded.netty.NettyWebServer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SystemCategoryService {
    @Autowired
    private SystemCategoryDao systemCategoryDao;
    @Autowired
    private SystemDao systemDao;
    @Autowired
    private UserDao userDao;

    /**
     * 新增
     */
    public JsonResult saveSystemCategory(SystemCategoryDto systemCategoryDto) {
        SystemCategory systemCategory = new SystemCategory();
        BeanUtils.copyProperties(systemCategoryDto, systemCategory);
        int flag = systemCategoryDao.saveSystemCategory(systemCategory);
        if (flag > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("新增系统大类失败");
        }
    }

    /**
     * 删除
     */
    public JsonResult deleteSystemCategory(SystemCategoryDto systemCategoryDto) {
        SystemSearchVO systemSearchVO = new SystemSearchVO();
        systemSearchVO.setPageIndex(1);
        systemSearchVO.setPageSize(100);
        systemSearchVO.setCateId(systemCategoryDto.getId());
        List<System> systems = systemDao.list(systemSearchVO);
        if (systems.size() > 0) {
            return JsonResultUtil.error("系统类别存在使用系统不能删除");
        }
        int flag = systemCategoryDao.deleteSystemCategory(systemCategoryDto.getId());
        if (flag > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("删除系统大类失败");
        }
    }

    /**
     * 修改
     */
    public JsonResult updateSystemCategory(SystemCategoryDto systemCategoryDto) {
        SystemCategory systemCategory = new SystemCategory();
        BeanUtils.copyProperties(systemCategoryDto, systemCategory);
        int flag = systemCategoryDao.updateSystemCategory(systemCategory);
        if (flag > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("修改系统大类失败");
        }
    }

    /**
     * 按照条件查询
     */
    public JsonResult<BasePage<List<SystemCategoryDto>>> listBySearch(SystemCategorySearchVO systemCategorySearchVO) {
        int count = systemCategoryDao.countBySearch(systemCategorySearchVO);
        List<SystemCategory> list = systemCategoryDao.listBySearch(systemCategorySearchVO);
        List<SystemCategoryDto> resultList = ListUtils.copyList(list, SystemCategoryDto.class);
        BasePage basePage = new BasePage();
        basePage.setData(resultList);
        basePage.setTotal(count);
        return JsonResultUtil.ok(basePage);
    }


    /**
     * 根据id查询
     */
    public JsonResult<SystemCategoryDto> getById(SystemCategoryDto systemCategoryDto) {
        SystemCategory systemCategory = systemCategoryDao.getById(systemCategoryDto.getId());
        if (systemCategory == null) {
            return JsonResultUtil.error("id不存在");
        }
        SystemCategoryDto systemCategoryDto1 = new SystemCategoryDto();
        BeanUtils.copyProperties(systemCategory, systemCategoryDto1);
        return JsonResultUtil.ok(systemCategoryDto1);
    }

    /**
     * 查询所有
     */
    public JsonResult<List<SystemCategoryDto>> listAll() {
        List<SystemCategory> list = systemCategoryDao.listAll();
        List<SystemCategoryDto> listResult = ListUtils.copyList(list, SystemCategoryDto.class);
        return JsonResultUtil.ok(listResult);
    }

    /**
     * 获取最大排序号
     * @return
     */
    public JsonResult getMaxOrder() {
        return JsonResultUtil.ok(systemCategoryDao.getMaxOrder());
    }

    /**
     * 根据用户所在路局查询所有本路局可见的分类
     * @param requestMap
     * @return
     */
    public JsonResult<List<SystemCategoryDto>> listAllByUserId(Map<String, String> requestMap) {
        Integer userId = Integer.parseInt(requestMap.get("userId"));
        SystemCategorySearchVO systemCategorySearchVO = new SystemCategorySearchVO();
        String ljdm = userDao.getById(userId).getBureauCode();
        systemCategorySearchVO.setLjdm(ljdm);
        List<SystemCategory> list = systemCategoryDao.listAllByUserId(systemCategorySearchVO);
        List<SystemCategoryDto> listResult = ListUtils.copyList(list, SystemCategoryDto.class);
        return JsonResultUtil.ok(listResult);
    }
}
