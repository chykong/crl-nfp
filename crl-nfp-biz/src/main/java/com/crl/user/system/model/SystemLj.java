package com.crl.user.system.model;

import lombok.Data;

/**
 * @Description:系统可见范围表
 * @Date:2019/12/25 20:17
 * @Author:苗凯旋
 */
@Data
public class SystemLj {
    private String ljdm;
    private Integer systemId;
}
