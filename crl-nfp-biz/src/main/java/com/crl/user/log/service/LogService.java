package com.crl.user.log.service;

import com.cars.util.bean.BasePage;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.list.ListUtils;
import com.crl.user.log.dao.LogDao;
import com.crl.user.log.dto.LogDto;
import com.crl.user.log.model.Log;
import com.crl.user.log.vo.LogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description:
 * @Date:2019/12/13 17:33
 * @Author:苗凯旋
 */
@Service
public class LogService {
    @Autowired
    private LogDao logDao;
    public JsonResult findLog(LogVo logVo) {
        BasePage<List<LogDto>> basePage = new BasePage();
        int count = logDao.count(logVo);
        basePage.setTotal(count);
        List<Log> list = logDao.findLog(logVo);
        List<LogDto> listResult = ListUtils.copyList(list, LogDto.class);
        basePage.setData(listResult);
        return JsonResultUtil.ok(basePage);
    }
}
