package com.crl.user.log.vo;

import com.cars.util.bean.BasePageSearchVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:操作日志查询用
 * @Date:2019/12/13 17:36
 * @Author:苗凯旋
 */
@Data
public class LogVo extends BasePageSearchVO {
    @ApiModelProperty("操作账号")
    private String username;
}
