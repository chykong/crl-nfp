package com.crl.user.log.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Description:操作日志
 * @Date:2019/12/13 17:35
 * @Author:苗凯旋
 */
@Data
public class LogDto {
    @ApiModelProperty("请求地址")
    private String url;
    @ApiModelProperty("操作账号")
    private String username;
    @ApiModelProperty("操作ip")
    private String ip;
    @ApiModelProperty("请求参数")
    private String parameters;
    @ApiModelProperty("响应结果")
    private String result;
    @ApiModelProperty("响应码")
    private String resultCode;
    @ApiModelProperty("操作时间")
    private Date createdAt;
}
