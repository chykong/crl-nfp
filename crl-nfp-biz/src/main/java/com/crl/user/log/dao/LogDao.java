package com.crl.user.log.dao;


import com.crl.user.log.model.Log;
import com.crl.user.log.vo.LogVo;
import com.cars.util.string.StringUtil;
import com.crl.dao.BaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Description:操作日志
 * @Date:2019/12/13 17:42
 * @Author:苗凯旋
 */
@Repository
public class LogDao extends BaseDao<Log, LogVo> {
    /**
     * 根据条件统计总数
     */
    public int count(LogVo logVo) {
        String sql = "SELECT COUNT(*) FROM crl_log_operation WHERE 1=1 ";
        sql += createSQL(logVo);
        return count(sql, logVo);
    }

    private String createSQL(LogVo logVo) {
        String sql = "";
        if(StringUtil.isNotNullOrEmpty(logVo.getUsername())){
            sql += " and username = :username";
        }
        return sql;
    }

    /**
     * 根据条件查询日志列表
     * @param logVo
     * @return
     */
    public List<Log> findLog(LogVo logVo){
        String sql = "select url,username,ip,parameters,result,result_code,created_at from crl_log_operation where 1=1";
        sql += createSQL(logVo);
        return list(sql,logVo);
    }
}
