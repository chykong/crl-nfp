package com.crl.user.opinion.dao;


import com.crl.user.opinion.model.Opinion;
import com.crl.user.opinion.vo.OpinionSearchVO;
import com.cars.util.page.PageUtil;
import com.cars.util.string.StringUtil;
import com.crl.dao.BaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 意见反馈操作dao
 */
@Repository
public class OpinionDao extends BaseDao<Opinion, OpinionSearchVO> {
    /**
     * 增加问题
     */
    public int saveOpinion(Opinion Opinion) {
        String sql = "INSERT INTO crl_base_opinion (id,title,content,type,contact,is_feedback,created_by,created_at)" +
                "VALUES(seq_crl_base_opinion.nextval,:title,:content,:type,:contact,0,:createdBy,sysdate)";
        return insertForId(sql, Opinion, "id");
    }

    /**
     * 根据意见反馈id删除
     */
    public int deleteOpinion(Integer OpinionId) {
        String sql = "DELETE FROM crl_base_opinion WHERE id=?";
        return update(sql, OpinionId);
    }

    /**
     * 根据意见反馈id修改
     */
    public int updateOpinion(Opinion Opinion) {
        String sql = "UPDATE crl_base_opinion SET title=:title,content=:content,type=:type,contact=:contact," +
                "created_by=:createdBy,modified_at=sysdate WHERE id=:id ";
        return update(sql, Opinion);
    }

    /**
     * 提交意见反馈
     */
    public int opinionBackFeed(Opinion Opinion){
        String sql = "UPDATE crl_base_opinion SET is_feedback=:isFeedback,fb_content=:fbContent,fb_by=:fbBy,fb_at=sysdate WHERE id=:id ";
        return update(sql, Opinion);
    }

    /**
     * 查询全部，用于选择意见反馈
     */
    public List<Opinion> listAllOpinion() {
        String sql = "SELECT id,title,content,type,contact,is_feedback,fb_content,fb_by,fb_at,created_at,created_by,modified_at FROM crl_base_opinion order by created_at";
        return list(sql);
    }

    /**
     * 根据id查询
     */
    public Opinion getById(Integer OpinionId) {
        String sql = "SELECT id,title,content,type,contact,is_feedback,fb_content,fb_by,fb_at,created_at,created_by,modified_at FROM crl_base_opinion WHERE id=?";
        return get(sql, OpinionId);
    }

    /**
     * 按条件查询
     */
    public List<Opinion> listOpinion(OpinionSearchVO OpinionSearchVO) {
        String sql = "SELECT t.id,t.title,t.content,t.type,t.contact,t.is_feedback,t.fb_content,t.fb_at,t.created_at,u.real_name created_by,t.modified_at," +
                "ou.real_name fb_by FROM crl_base_opinion t" +
                " left join crl_base_user u on t.created_by = u.id left join crl_base_user ou on t.fb_by = ou.id WHERE 1=1 ";
        sql += createSQL(OpinionSearchVO);
        sql += " ORDER BY t.created_at DESC";
        sql = PageUtil.createOraclePageSQL(sql, OpinionSearchVO.getPageIndex(), OpinionSearchVO.getPageSize());
        return list(sql, OpinionSearchVO);
    }

    /**
     * 统计查询数据条数
     */
    public int count(OpinionSearchVO OpinionSearchVO) {
        String sql = "SELECT COUNT(id) FROM crl_base_opinion t WHERE 1=1 ";
        sql += createSQL(OpinionSearchVO);
        return count(sql, OpinionSearchVO);
    }

    /**
     * 拼接查询条件
     */
    private String createSQL(OpinionSearchVO OpinionSearchVO) {
        String sql = "";
        if (StringUtil.isNotNullOrEmpty(OpinionSearchVO.getTitle())) {
            OpinionSearchVO.setTitle(OpinionSearchVO.getTitle() + "%");
            sql += " AND t.title LIKE :title ";
        }
        if (StringUtil.isNotNullOrEmpty(OpinionSearchVO.getType())) {
            sql += " AND t.type = :type ";
        }
        if (null != OpinionSearchVO.getCreatedBy()) {
            sql += " AND t.created_by = :createdBy ";
        }
        return sql;
    }

    /**
     * 根据用户id获取用户拥有意见反馈列表
     */
    public List<Opinion> getOpinionByUserId(Integer userId) {
        String sql = "SELECT id,title,content,type,contact,is_feedback,fb_content,fb_by,fb_at,created_at,created_by,modified_at FROM crl_base_opinion WHERE created_at = ?";
        return list(sql, userId);
    }
}
