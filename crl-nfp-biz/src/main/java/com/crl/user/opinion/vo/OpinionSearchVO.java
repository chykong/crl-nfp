package com.crl.user.opinion.vo;

import com.cars.util.bean.BasePageSearchVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 意见反馈查询
 */
@Data
public class OpinionSearchVO extends BasePageSearchVO {
    @ApiModelProperty("问题类型")
    private String type;
    @ApiModelProperty("问题标题")
    private String title;
    @ApiModelProperty("创建人")
    private Integer createdBy;
}
