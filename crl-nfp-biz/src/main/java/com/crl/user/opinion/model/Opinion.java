package com.crl.user.opinion.model;

import lombok.Data;

import java.util.Date;

/**
 * 意见反馈
 */
@Data
public class Opinion {
   /**
    * 意见反馈id
    */
   private Integer id;
   /**
    * 问题标题
    */
   private String title;
   /**
    * 问题内容
    */
   private String content;
   /**
    *创建人
    */
   private String createdBy;
   /**
    *创建时间
    */
   private Date createdAt;
   /**
    * 问题类型
    */
   private String type;
   /**
    * 联系方式
    */
   private String contact;
   /**
    * 是否反馈
    */
   private String isFeedback;
   /**
    * 反馈内容
    */
   private String fbContent;
   /**
    * 反馈人
    */
   private String fbBy;
   /**
    * 反馈时间
    */
   private String fbAt;
   /**
    *修改人
    */
   private String modifiedBy;
   /**
    *修改时间
    */
   private Date modifiedAt;
}
