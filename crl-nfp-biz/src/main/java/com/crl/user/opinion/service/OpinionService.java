package com.crl.user.opinion.service;

import cn.hutool.core.bean.BeanUtil;
import com.cars.util.bean.BasePage;
import com.cars.util.json.JsonResult;
import com.cars.util.json.JsonResultUtil;
import com.cars.util.list.ListUtils;
import com.crl.user.opinion.dao.OpinionDao;
import com.crl.user.opinion.dto.OpinionDto;
import com.crl.user.opinion.model.Opinion;
import com.crl.user.opinion.vo.OpinionSearchVO;
import com.crl.user.user.dao.ResourceDao;
import com.crl.user.user.dao.RoleDao;
import com.crl.user.user.model.Role;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 意见
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class OpinionService {
    @Autowired
    private OpinionDao opinionDao;
    @Autowired
    private ResourceDao resourceDao;
    @Autowired
    private RoleDao roleDao;

    /**
     * 增加
     */
    public JsonResult saveOpinion(OpinionDto opinionDto) {
        Opinion opinion = new Opinion();
        BeanUtil.copyProperties(opinionDto, opinion);
        int flag = opinionDao.saveOpinion(opinion);
        if (flag > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("意见新增失败");
        }
    }

    /**
     * 删除
     */
    public JsonResult deleteOpinion(OpinionDto opinionDto) {
        int flag = opinionDao.deleteOpinion(opinionDto.getId());
        if (flag == 0) {
            return JsonResultUtil.error();
        } else {
            return JsonResultUtil.ok();
        }
    }

    /**
     * 修改
     */
    public JsonResult updateOpinion(OpinionDto opinionDto) {
        Opinion opinion = new Opinion();
        BeanUtil.copyProperties(opinionDto, opinion);
        int flag = opinionDao.updateOpinion(opinion);
        if (flag > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("意见修改失败");
        }
    }

    /**
     * 意见反馈
     */
    public JsonResult fbOpinion(OpinionDto opinionDto){
        Opinion opinion = new Opinion();
        BeanUtil.copyProperties(opinionDto, opinion);
        int flag = opinionDao.opinionBackFeed(opinion);
        if (flag > 0) {
            return JsonResultUtil.ok();
        } else {
            return JsonResultUtil.error("意见反馈失败");
        }
    }

    /**
     * 获取所有意见
     */
    public JsonResult<List<OpinionDto>> listAll() {
        List<Opinion> list = opinionDao.listAllOpinion();
        List<OpinionDto> opinionDtoList = ListUtils.copyList(list, OpinionDto.class);
        return JsonResultUtil.ok(opinionDtoList);
    }

    /**
     * 根据id获取数据
     */
    public JsonResult<OpinionDto> getById(OpinionDto opinionDto) {
        Opinion opinion = opinionDao.getById(opinionDto.getId());
        OpinionDto dto = new OpinionDto();
        BeanUtil.copyProperties(opinion, dto);
        return JsonResultUtil.ok(dto);
    }

    /**
     * 获取条件查询意见
     */
    public JsonResult<BasePage<List<OpinionDto>>> listSearch(OpinionSearchVO opinionSearchVO) {
        if(null != opinionSearchVO.getCreatedBy()){
            List<Role> roleList = roleDao.getRoleByUserId(opinionSearchVO.getCreatedBy());
            for(Role role: roleList){
                // 用户角色为管理员角色
                if("0".equals(role.getRoleType())){
                    opinionSearchVO.setCreatedBy(null);
                    break;
                }
            }
        }
        List<Opinion> opinionList = opinionDao.listOpinion(opinionSearchVO);
        int count = opinionDao.count(opinionSearchVO);
        BasePage<List<OpinionDto>> basePage  = new BasePage<>();
        List<OpinionDto> opinionDtoList = ListUtils.copyList(opinionList,OpinionDto.class);
        basePage.setTotal(count);
        basePage.setData(opinionDtoList);
        return JsonResultUtil.ok(basePage);
    }
}
