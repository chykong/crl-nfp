package com.crl.user.opinion.dto;

import com.cars.util.validation.DeleteValidGroup;
import com.cars.util.validation.UpdateValidGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 意见反馈
 */
@Data
public class OpinionDto {
    /**
     *意见反馈id
     */
    @ApiModelProperty("意见反馈id")
    @NotNull(groups = {UpdateValidGroup.class, DeleteValidGroup.class})
    private Integer id;
    /**
     *意见标题
     */
    @ApiModelProperty("意见标题")
    private String title;
    /**
     *意见内容
     */
    @ApiModelProperty("意见内容")
    private String content;
    /**
     *创建人
     */
    @ApiModelProperty("创建人")
    private String createdBy;
    /**
     *创建时间
     */
    @ApiModelProperty("创建时间")
    private Date createdAt;
    /**
     * 问题类型
     */
    @ApiModelProperty("问题类型")
    private String type;
    /**
     * 联系方式
     */
    @ApiModelProperty("联系方式")
    private String contact;
    /**
     * 是否反馈
     */
    @ApiModelProperty("是否反馈")
    private String isFeedback;
    /**
     * 反馈内容
     */
    @ApiModelProperty("反馈内容")
    private String fbContent;
    /**
     * 反馈人
     */
    @ApiModelProperty("反馈人")
    private String fbBy;
    /**
     * 反馈时间
     */
    @ApiModelProperty("反馈时间")
    private String fbAt;
    /**
     *修改人
     */
    @ApiModelProperty("修改人")
    private String modifiedBy;
    /**
     *修改时间
     */
    @ApiModelProperty("修改时间")
    private Date modifiedAt;
}
